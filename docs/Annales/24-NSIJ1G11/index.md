---
title: Centres étrangers, Groupe 1, J1
keywords: baccalauréat, 24-NSIJ1G11, 2024
---

# BACCALAURÉAT GÉNÉRAL

Le sujet est disponible [ici](./24-NSIJ1G11.pdf)

**ÉPREUVE D'ENSEIGNEMENT DE SPÉCIALITÉ - SESSION 2024 - NUMÉRIQUE ET SCIENCES INFORMATIQUES**

**JOUR 1**

Durée de l'épreuve : **3 heures 30**

*L'usage de la calculatrice n'est pas autorisé.*

**Le sujet est composé de trois exercices indépendants.**

**Le candidat traite les trois exercices.**


Durée de l'épreuve : *3 heures 30*

*L'usage de la calculatrice n'est pas autorisé.*

**Le sujet est composé de trois exercices indépendants.**

**Le candidat traite les trois exercices.**

## EXERCICE 1 (6 points)

*Cet exercice porte sur la programmation Python, la programmation dynamique, les
graphes et les réseaux.*

On cherche à lutter contre un virus informatique qui essaie de contourner les protocoles de sécurité en migrant régulièrement vers un autre ordinateur, en choisissant à chaque fois au hasard sa nouvelle cible parmi les ordinateurs accessibles.
On cherche à savoir quels ordinateurs protéger afin de lutter de manière la plus efficace possible avec des ressources limitées.
On considère le réseau informatique suivant, composé de 5 ordinateurs numérotés : 0, 1, 2, 3 et 4.

<figure>
	<img src="24-NSIJ1G11_1.png">
	<figcaption>Figure 1. Réseau informatique</figcaption>
</figure>


!!! note "Question"
	On représente ce réseau informatique par un graphe que l'on stocke sous forme de listes de voisins. 
	Compléter la définition de la variable `voisins`.
	```python linenums="1"
	voisins = [[1, 2, 3, 4],
		[0, 2, 3],
		[0, 1],
		[...],
		[...]]
	```

??? success "Réponse"
	```python linenums="1"
	voisins = [[1, 2, 3, 4],
		[0, 2, 3],
		[0, 1],
		[0, 1],
		[0]]
	```	

On ajoute au réseau actuel un sixième ordinateur, numéroté 5. Cet ordinateur n'est accessible que des ordinateurs numérotés 0 et 2.

??? note "Dessiner le nouveau graphe."
    ```mermaid
    flowchart LR
      0 --- 1 & 2 & 3 & 4 & 5
      1 --- 2 & 3
      2 --- 5
    ```

??? note  "Donner la nouvelle définition de la variable voisins."
	```python linenums="1"
	voisins = [[1, 2, 3, 4, 5],
		[0, 2, 3],
		[0, 1, 5],
		[0, 1],
		[0],
        [0, 2]]
	```	
!!! note "Question"
	Compléter la fonction voisin alea qui prend en paramètre un graphe `voisins` sous forme de listes de voisins et un entier `s` représentant un sommet et qui renvoie un entier représentant un voisin de `s` choisi aléatoirement.
	On pourra utiliser la fonction `random.randrange(n)` qui renvoie un nombre aléatoire entre $0$ inclus et `n` exclus.
	```python
	def voisin alea(voisins, s):
		...
	```

??? success "Réponse"
	```python
	def voisin alea(voisins, s):
		return voisins[s][random.randrange(len(voisins[s]))]
	```

On donne la fonction `marche_alea` suivante :

```python linenums="1"
def marche_alea(voisins, i, n):
	if n == 0:
		return i
	return marche alea(voisins, voisin alea(voisins, i), n-1)
```

??? note "Justifier que la fonction `marche_alea` est une fonction récursive."
	À la ligne 4, on observe que `marche_alea` fait appel à elle même.
	C'est donc une fonction récursive.

??? note "Décrire ce que modélise cette fonction, en rapport avec le contexte de l'exercice."
	Cette fonction parcourt le graphe aléatoirement en faisant `n` pas (en parcourant `n` arêtes) à partir du noeud `i`, puis renvoie le numéro du sommet d'arrivée. Il choisit donc une machine accessible en `n` pas depuis `i` aléatoirement.
	
!!! note "Question"
	Compléter la fonction simule qui simule `n_tests` fois le déplacement d'un virus pendant `n_pas` étapes, démarrant au sommet `i`, et qui renvoie une liste contenant en position `j` le nombre de fois que le virus a terminé son parcours au sommet `j`, divisé par `n` tests.

	```python linenums="1"
	def simule(voisins, i, n_tests, n_pas):
		results = [0] * len(voisins)
		...
	```

??? success "Réponse"
	```python linenums="1"
	def simule(voisins, i, n_tests, n_pas):
		results = [0] * len(voisins)
		for _ in range(n_tests):
        	atteint = marche_alea(voisins, i, n_pas)
			results[atteint] += 1
		return [ results[j]/n_tests for j in range(len(voisins)) ]
	```

!!! note "Question"
	L'appel `simule(voisins, 4, 1000, 1000)` renvoie la valeur suivante : `[0.328, 0.195, 0.18, 0.12, 0.059, 0.118]`. 
	Déduire de ce résultat l'ordinateur du réseau qu'il est le plus rentable de protéger.

??? success "Réponse"
	L'ordinateur où on passe le plus souvent (qui est donc la cible la plus probable) est l'ordinateur numéro 0 (32,8% sur 1000 essais) il faut donc le protéger en priorité.

Au début, on suppose que le virus n'est présent que sur un ordinateur.
À chaque étape, il contamine tous ses voisins non déjà contaminés.
On cherche à savoir combien de temps prend ce virus pour se propager à tout le réseau.

!!! note "Question"
	Un graphe `voisins` représente un réseau, et `s` représente un sommet de départ. 
	Proposer un algorithme pour déterminer le temps, en étape, que met un virus à se propager dans l'intégralité d'un réseau.

??? success "Réponse"
	L'objectif ici est de trouver le temps que mettra un virus à se propager depuis un sommet d'origine jusqu'à **tout les autres sommets**.
	Cela correspond au temps qu'il mettra pour atteindre le sommet **le plus éloigné** si à chaque étape il contamine tout les voisins directement accessibles.
	On peut donc utiliser un algorithme similaire à celui de Dijkstra pour calculer la distance entre le sommet de départ et tout les autres, puis on déterminera quelle est la distance maximale.
	```text
	distances <- [+Infini]*taille(voisins) # la distance entre s et les autres sommets
	distances[0] <- 0
	déjà_vus <- [s]
	a_traiter <- File vide # la file des prochains sommets à contaminer.
	a_traiter.enfiler(s) # au départ seul s est à traiter.
	tant que a_traiter n'est pas vide et que déjà_vus ne contient pas tout les sommets.
		s1 <- a_traiter.défiler()
		Pour chaque sommet v voisin de s1 dans le graphe:
			distances[v] = min(distances[s1]+1,distances[v])
			si v n'est pas dans déjà_vus:
				ajouter v à déjà_vus
				a_traiter.enfiler(v)
	renvoyer le maximum de la liste distances #Avec un parcours par valeur linéaire.
	```
	Une autre possibilité: De façon beaucoup plus simple mais moins sûre, on pourrait utiliser la fonction simule précédente en mettant un nombre de tests élevé (par exemple 1000) et en augmentant progressivement le nombre de pas en partant de 1. Dés
que toutes les cases du tableau renvoyé sont non-nulles, c’est que tout le réseau a pu être parcouru et que donc le nombre de pas est égal au temps en étape de propagation du virus.

## EXERCICE 2 (6 points)

*Cet exercice porte sur les réseaux et les protocoles de routage.*

!!! tip "Rappels :"
	Une adresse IPv4 est composée de 4 octets, soit 32 bits.
	
	Elle est notée $a.b.c.d$, où $a$, $b$, $c$ et $d$ sont les valeurs décimales des 4 octets et nommée «notation décimale pointée ».
	La notation $a.b.c.d/n$, appelée notation CIDR (**C**lassless **I**nter **D**omain **R**outing), signifie que les $n$ premiers bits à gauche de l'adresse IP représentent la partie « réseau », les bits à droite qui suivent représentent la partie « machine ».
	
	L'adresse IPv4 dont tous les bits de la partie « machine » sont à 0 est appelée « adresse du réseau ».
	
	L'adresse IPv4 dont tous les bits de la partie « machine » sont à 1 est appelée « adresse de diffusion ».

On considère le réseau représenté ci-dessous :

<figure>
	<img src="24-NSIJ1G11_2.png">
	<figcaption>Figure 2. Réseau vu par Filius</figcaption>
</figure>

### Partie A : Adresses IP

Les machines du réseau local L1 indiquent un masque de sous réseau sur 24 bits en notation CIDR, soit 255.255.255.0 en notation décimale pointée.

??? note "Donner le masque de sous réseau en notation décimale pointée des machines du réseau L2 (masque de sous réseau de 16 bits)."
	Le masque est sur 16 bits, on a donc: 255.255.0.0 (deux premiers octets à 11111111).

Concernant le réseau local L2 :

??? note "Donner l'adresse du réseau."
	L'adresse de réseau est celle où on met les 16 derniers bits à 0 (car le masque est 16): 172.16.0.0 .
??? note "Donner l'adresse de diffusion."
	L'adresse de diffusion est celle où on met les 16 derniers bits à 1 (car le masque est 16): 172.16.255.255 .

??? note "Donner le nombre maximum de machines pouvant être connectées à ce réseau."
	On commence par calculer le nombre de machines maximales dans le réseau: $2^{32-16} = 2^{16}$.
	On doit ensuite enlever les machines déjà affectées (deux) ainsi que les adresses de réseau et de diffusion soit: $2^{16}-4=65536-4=65532$

### Partie B : Protocoles de routage

On donne ci-dessous des extraits des tables de routage des routeurs :


| Routeur | Réseau destinataire | Passerelle   | Interface      |
|---------|---------------------|--------------|----------------|
| A       | L2                  | 53.10.10.2   | 53.10.10.1     |
| B       | L2                  | 193.55.24.6  | 193.55.24.5    |
| C       | L2                  | 193.55.24.10 | 193.55.24.9    |
| D       | L2                  | Connecté     | 172.16.255.254 |
| E       | L2                  | 42.4.10.14   | 42.4.10.13     |
| F       | L2                  | 42.4.10.10   | 42.4.10.9      |
| G       | L2                  | 53.10.10.13  | 53.10.10.14    |
| H       | L2                  | 53.10.10.6   | 53.10.10.5     |

??? note "À l'aide des extraits des tables de routage ci-dessus, donner un chemin (c'est-à-dire nommer les routeurs traversés) suivi par un message envoyé du réseau L1 vers le réseau L2."
	Le routeur A est la passerelle du réseau L1 : il sera donc le premier routeur traversé par le message.
	La table du routeur A indique que pour joindre L2 il faut passer par 53.10.10.2, donc le routeur H.
	Pour celui-ci, c’est l’adresse 53.10.10.6 soit le routeur D.
	Enfin le routeur d est directement relié à L2, c’est donc le dernier routeur traversé.
	Le chemin suivi est donc **L1 → A → H → D → L2**.

La liaison entre les routeurs H et D est rompue :

??? note "Sachant que le protocole de routage RIP est utilisé (distance en nombre de sauts), donner les nouveaux chemins que pourra suivre un message allant de L1 vers L2."

!!! note "Choisir un des chemins de la question précédente."
	Donner les routeurs dont la règle de routage à destination de L2 est obligatoirement modifiée. 
	Après avoir examiné tous les routeurs, écrire sur votre copie les règles de routage modifiées en conséquence.

??? success "Réponse"

La liaison entre les routeurs H et D est rétablie.

Pour tenir compte du débit des liaisons, on décide d'utiliser le protocole OSPF (distance liée au coût des liaisons) pour effectuer le routage.
Le coût d'une liaison est donné ici par la formule : $coût=\frac{10^9}{BP}$ où BP est la bande passante de la connexion en bit par seconde.

Les valeurs des bandes passantes de chaque liaison entre les routeurs sont données ci-dessous :

| Liaison | Bande passante |
|---------|----------------|
| A-B     | 1 Gbit/s       |
| A-H     | 1 Gbit/s       |
| A-G     | 1 Gbit/s       |
| B-C     | 1 Gbit/s       |
| C-H     | 100 Mbit/s     |
| C-D     | 1 Gbit/s       |

| Liaison | Bande passante |
|---------|----------------|
| D-H     | 100 Mbit/s     |
| D-E     | 10 Gbit/s      |
| E-F     | 10 Gbit/s      |
| F-H     | 1 Gbit/s       |
| F-G     | 10 Gbit/s      |
| G-H     | 1 Gbit/s       |

??? note "Calculer le coût des liaisons pour les 3 valeurs de bande passante qui apparaissent dans le tableau ci-dessus."

??? note "Déterminer alors le chemin que suivra un message allant de L1 vers L2 et donner son coût."

La liaison entre les routeurs G et F est rompue.

??? note "Déterminer le nouveau chemin suivi par un message allant de L1 vers L2 et donner son coût."

## EXERCICE 3 (8 points)

*Cet exercice porte sur la programmation Python, la programmation orientée objet, les bases de données relationnelles et les requêtes SQL.*

L'objectif est de faciliter la gestion du système d'information d'un camping municipal.
Les informations nécessaires sont stockées dans une base de données relationnelle composée de trois relations. On pourra utiliser les mots-clés SQL suivants : `AND,
FROM, INSERT, INTO, JOIN, ON, SELECT, SET, UPDATE, VALUES, WHERE`.

Voici le schéma des deux premières relations :

`Client ( PK:id_client , nom, prenom, adresse, ville, pays, telephone)`

`Reservation ( PK:id_reservation , #id_client, #id_emplacement, nombre_personne, date_arrivee, date_depart)`

Dans ce schéma :

* la clé primaire de chaque relation est définie par son attribut préfixé par `PK:`
* les attributs précédés de `#` sont les clés étrangères.

La troisième relation est appelée `Emplacement` et elle contient tous les emplacements du camping.
Le tableau ci-dessous en donne un extrait.

<table>
	<thead>
		<tr>
			<th scope="col" colspan="4">Emplacement
	</thead>
	<tbody>
		<tr>
			<th scope="col">id emplacement
			<th>nom
			<th>localisation
			<th>tarif journalier
		<tr>
			<td>1
			<td>myrtille
			<td>A4
			<td>25
		<tr>
			<td>2
			<td>mirabelle
			<td>D1
			<td>35
		<tr>
			<td>3
			<td>mangue
			<td>B2
			<td>29.90
		<tr>
			<td>4
			<td>mandarine
			<td>B1
			<td>25
		<tr>
			<td>5
			<td>mûre
			<td>C3
			<td>29.90
		<tr>
			<td>6
			<td>melon
			<td>A2
			<td>25
	</tbody>
</table>

### Partie A

??? note "Citer deux avantages à utiliser une base de données relationnelle plutôt qu'un fichier texte ou un fichier tableur."

??? note "Quelle doit être la caractéristique d'un attribut pour pouvoir être utilisé en tant que clé primaire ?"

??? note "Dans la relation `Reservation`, quel est le rôle des clés étrangères `id_client` et `id_emplacement `?"

??? note "Donner le schéma relationnel de la relation `Emplacement` en précisant la clé primaire et le type de chacun des attributs."

!!! note "À partir de l'extrait du contenu de la relation `Emplacement` donner le résultat de la requête ci-dessous :"
	```sql
	SELECT id_emplacement, nom, localisation
	FROM Emplacement
	WHERE tarif_journalier = 25;
	```

??? note "Écrire une requête permettant de donner le nom et le prenom de tous les clients habitant à 'Strasbourg'."

!!! note "Écrire une requête permettant d'ajouter un nouveau client :"
	* `id_client` 42 ;
	* `nom` 'CODD' ;
	* `prénom` 'Edgar' ;
	* `adresse` '28 rue des Capucines' ;
	* `ville` 'Lyon' ;
	* `pays` 'France' ;
	* `numéro_de_téléphone` '0555555555'.

!!! note "Écrire une requête SQL permettant de récupérer les informations ci-dessous concernant la réservation dont l'identifiant `id_reservation` est 18 :"
	* `Client.nom`
	* `Client.prenom`
    * `Reservation.nombre_personne`
	* `Reservation.date_arrivee`
	* `Reservation.date_depart`
	* `Emplacement.tarif_journalier`

### Partie B

Dans cette partie, on souhaite éditer une facture correspondant au séjour d'un client.
Pour cela, on dispose d'une fonction Python qui récupère auprès de la base de données, à la manière de la question 8, les informations concernant la réservation voulue et renvoie le résultat sous forme d'un tuple contenant trois objets respectivement des classes `Client`, `Reservation` et `Emplacement`.

```python linenums="1"
from datetime import datetime
class Client
	def __init__ (self, nom, prenom, adresse, ville, pays,telephone):
		self.nom = nom
		self.prenom = prenom
		self.adresse = adresse
		self.ville = ville
		self.pays = pays
		self.telephone = telephone

class Reservation:
	def __init__ (self, id reservation, nombre personne, date arrivee, date depart):
		self.id reservation = id reservation
		self.nombre personne = nombre personne
		self.date arrivee = date arrivee
		self.date depart = date depart
	def nb_jours(self):
		""" renvoie, à l'aide de l'attribut days de la classe 
		timedelta, un entier correspondant
		au nombre de jours passés au camping."""
		return (self.date depart - self.date arrivee).days

class Emplacement:
	def __init__(self, nom, tarif journalier):
		self.nom = nom
		self.tarif_journalier = tarif_journalier
```

??? note "Expliquer pourquoi le terme self est utilisé comme paramètre pour les méthodes des classes `Client`, `Reservation` et `Emplacement`."

!!! note "Instancier une variable `client01` de la classe `Client` représentant un client se nommant CODD Edgar habitant au 28 rue des Capucines à Lyon, France, ayant pour numéro de téléphone le 0555555555.

On considère un tuple constitué de trois objets, respectivement dans cet ordre, des classes `Client`, `Reservation` et `Emplacement`. 
On souhaite écrire une fonction qui renvoie le montant dû par ce client pour cet emplacement et pour cette durée de séjour.

Sachant qu'au tarif journalier de location de l'emplacement il faut ajouter une taxe de séjour de 2,20 € par jour et par personne.
Exemple de calcul du montant à régler pour un client ayant réservé pour 4 personnes pendant 12 jours un emplacement à 30 € la journée :

```pycon
>>> 30 * 12 + 4 * 2.20 * 12
465.6
```

!!! note "Compléter la ligne 5 de la fonction `montant_a_regler`."
	```python linenums="1"
	def montant_a_regler(triplet):
		""" renvoie le montant en euros
		à régler pour cette réservation """
		client, reservation, emplacement = triplet
		return ..............................
	```
??? success "Réponse"

Chaque facture doit posséder ce que l'on appelle communément un numéro de facture unique.
En réalité il s'agit d'une chaîne de caractères.
Pour ses factures, depuis 2018, le camping a adopté le format 'AAAA-MMM-xxx' composé des trois chaînes de caractères ci-dessous :
* 'AAAA' une année comprise entre 2018 et 2024 ;
* 'MMM' les trois premières lettres du mois en anglais ;
* 'xxx' désigne trois chiffres.

On décide d'écrire une fonction facture est valide pour tester si une chaîne de caractères représente un numéro de facture valide ou non. 
Voici quelques exemples du comportement attendu de la fonction facture est valide.

```pycon
>>> facture est valide('2024-MAY-230')
True
>>> facture est valide('2012-MAY-230')
False
>>> facture est valide('2024-MAI-230')
False
>>> facture est valide('2024-JUN-23')
False
```

On considère le programme suivant :

```python linenums="1"
calendrier = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN',
	'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']

def separe(chaine):
	"""renvoie une liste constituée de chaînes qui étaient
	séparées par le caractère - """
	return chaine.split('-')

def que_des_chiffres(chaine):
	"""renvoie vrai si chaine n'est constituée que
	des caractères de 0 à 9 faux sinon"""
	for car in chaine:
		if not(car in "012345789"):
			return False
	return True

def facture est valide(chaine):
	"""renvoie vrai si chaine est une chaîne de
	caractères conforme au modèle de facture"""
	partie = separe(chaine)
	if not(len(partie) == 3):
		return False
	annee, mois, numero = partie[0], partie[1], partie[2]
	if not(que des chiffres(annee)):
		return False
	if not(len(annee) == 4) or not(2018 <= annee <= 2024):
		return False
	# Reste à faire vérifier les mois MMM
	...
	# Reste à faire vérifier le numéro xxx *
	...
	return True
```

On rappelle que la fonction split en Python divise une chaîne de caractères en une liste de sous-chaînes en fonction d'un séparateur spécifié.
Par exemple;

```python
texte = 'Bonjour-le-monde'
separateur = '--'
resultat = texte.split(separateur)
```

donne comme résultat `['Bonjour', 'le', 'monde']`

??? note "Expliquer pourquoi une erreur se produit à l'exécution de la fonction facture est valide donnée ci-dessus."
??? note "Proposer une correction du code pour que cette erreur ne se produise plus."
!!! note "Question"
	Compléter le code afin de vérifier les mois (ligne 28) et le numéro (ligne 30) dans la fonction facture est valide. 
	Pour chaque vérification, il est possible d'insérer une ou plusieurs lignes.
