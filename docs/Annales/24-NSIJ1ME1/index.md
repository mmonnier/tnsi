# BACCALAURÉAT GÉNÉRAL

Le sujet est disponible [ici](./24-NSIJ1ME1.pdf)

**ÉPREUVE D’ENSEIGNEMENT DE SPÉCIALITÉ**

**SESSION 2024 - NUMÉRIQUE ET SCIENCES INFORMATIQUES - ÉPREUVE DU MERCREDI 19 JUIN 2024**

Durée de l’épreuve : *3 heures 30*

L’usage de la calculatrice n’est pas autorisé.

Dès que ce sujet vous est remis, assurez vous qu’il est complet.

Le sujet est composé de trois exercices indépendants.

Le candidat traite les trois exercices.

## Exercice 1 (6 points)

*Cet exercice porte sur la programmation objet en Python et les graphes.*


Nous avons représenté sous la forme d’un graphe les liens entre cinq différents sites Web :

<figure markdown=1>
![](./24-NSIJ1ME1-000.png)
<figcaption>Figure 1. Graphe avec 5 sites</figcaption>
</figure>

La valeur de chaque arête représente le nombre de citations (de liens hypertextes) d’un site vers un autre. 
Ainsi, le site **site4** contient 6 liens hypertextes qui renvoient vers le site **site5**.

Les sites sont représentés par des objets de la classe `Site` dont le code est partiellement donné ci-dessous.
La complétion de la méthode `calculPopularite` fera l’objet d’une question ultérieure

```python linenums="1"
class Site:
	def __init__(self, nom):
		self.nom = nom
		self.predecesseurs = []
		self.successeurs = []
		self.popularite = 0
		self.couleur = 'blanche'

	def calculPopularite(self):
		...
```

Le graphe précédent peut alors être représenté ainsi :

```python linenums="1"
# Description du graphe
s1, s2, s3, s4, s5 = Site('site1'), Site('site2'),Site('site3'), Site('site4'), Site('site5')
s1.successeurs = [(s3,3), (s4,1), (s5,3)]
s2.successeurs = [(s1,4), (s3,5), (s4,2)]
s3.successeurs = [(s5, 3)]
s4.successeurs = [(s1,2), (s5,6)]
s5.successeurs = [(s3,4)]
s1.predecesseurs = [(s2,4), (s4,2)]
s2.predecesseurs = []
s3.predecesseurs = [(s1,3), (s2,5), (s5,4)]
s4.predecesseurs = ...
s5.predecesseurs = ...
```

??? question "Expliquer la ligne 9 de ce code."
	Le **site2** n'a pas de prédécesseurs, on affecte donc une liste vide à son champ `predecesseurs`.

??? question "Les lignes 11 et 12 de cette description du graphe ne sont pas complètes. Recopier et compléter le code des lignes 11 et 12."
	```python linenums="11"
	s4.predecesseurs = [(s1, 1), (s2, 2)]
	s5.predecesseurs = [(s4, 6), (s3, 3), (s1, 3)]
	```
	
??? question "Donner et expliquer la valeur de l’expression suivante : `s2.successeurs[1][1]`"
	Cette expression permet de récupérer le nombre de fois où `s2` cite son second successeur ici `s3`, dans ce cas c'est la valeur 5 qui est donnée par cette expression.

Pour mesurer la pertinence d’un site, on commence par lui attribuer un nombre appelé valeur de popularité qui correspond au nombre de fois qu’il est cité dans les autres sites, c’est-à-dire le nombre de liens hypertextes qui renvoient sur lui.

Par exemple, la valeur de popularité du site **site4** est 3.

??? question "Donner selon cette définition, la valeur de popularité du site **site1**"
	La valeur de popularité de **site1** est la somme des arêtes entrantes soit: $4+2=6$.
	
??? question "Écrire sur votre copie le code de la méthode `calculPopularite` de la classe `Site` qui affecte à l’attribut `popularite` la valeur de popularité correspondante et renvoie cet attribut."
	```python linenums="10"
	def calculPopularite(self):
		for site,citations in self.predecesseurs:
			self.popularite += citations
		return self.popularite
	```



Afin de calculer cette valeur de popularité pour chacun des sites, nous allons faire un parcours dans le graphe de façon à exécuter la méthode `calculPopularite` pour chacun des objets.


Voici le code de la fonction qui permet le parcours du graphe :

```python linenums="1"
def parcoursGraphe(sommetDepart):
	parcours = []
	sommetDepart.couleur = 'noire'
	listeS = []
	listeS.append(sommetDepart)
	while len(listeS) != 0:
		site = listeS.pop(0)
		site.calculPopularite()
		parcours.append(site)
		for successeur in site.successeurs:
			if successeur[0].couleur == 'blanche':
				successeur[0].couleur = 'noire'
				listeS.append( successeur[0] )
	return parcours
```

On rappelle les points suivants :

- la méthode `append` ajoute un élément à une liste Python ; par exemple, `tab.append(el)` permet d’ajouter l’élément `el` à la liste Python `tab` ;
- la méthode `pop` enlève de la liste l'élément situé à la position indiquée et le renvoie en valeur de retour; par exemple, `tab.pop(2)` enlève l'élément à l'indice 2 et le renvoie.

Dans ce parcours, les sites non encore traités sont de couleur 'blanche' (valeur par défaut à la création de l’objet) et ceux qui sont traités de couleur 'noire'.


??? question "Dans ce parcours, on manipule la liste Python nommée `listeS` uniquement à l’aide d’appels de la forme `listeS.append(sommet)` et `listeS.pop(0)`. Donner la structure de données correspondant à ces manipulations."
	Ici on ajoute nos éléments en fin de liste (`append`) et on récupère en début de liste (`pop(0)`).
	Notre liste est donc utilisée comme une **File**.
	
??? question "Donner le nom de ce parcours de graphe"
	On parcourt un sommet puis tout ses voisins à l'aide d'une file, ceci correspond à un parcours en **largeur** du graphe.

??? question "La fonction `parcoursGraphe` renvoie une liste `parcours`. Indiquer la valeur renvoyée par l’appel de fonction : `parcoursGraphe(s1)`"
	La liste renvoyée ici est: `[s1, s3, s4, s5]`.
	Le sommet `s2` n'a aucun lien entrant et n'est donc pas parcouru.

On cherche maintenant le site le plus populaire, celui dont la valeur de popularité est la plus grande.

Voici le code de la fonction qui renvoie le site le plus populaire, elle prend comme argument une liste non vide contenant des instances de la classe Site.

```python linenums="1"
def lePlusPopulaire(listeSites):
	maxPopularite = 0
	siteLePlusPopulaire=listeSites[0]
	for site in listeSites:
		if site.popularite > maxPopularite:
			...
			...
	return siteLePlusPopulaire
```

??? question "Copier et compléter les lignes 6 et 7 de cette fonction"
	```python linenums="1"
	def lePlusPopulaire(listeSites):
		maxPopularite = 0
		siteLePlusPopulaire=listeSites[0]
		for site in listeSites:
			if site.popularite > maxPopularite:
				maxPopularite = site.popularite
				siteLePlusPopulaire = site
	    return siteLePlusPopulaire
	```
??? question "Donner ce que renvoie la ligne de code suivante : `lePlusPopulaire(parcoursGraphe(s1)).nom`"
	Cette ligne renvoie le **nom** du site **le plus populaire** accessible depuis le **site 1**.
??? question " On envisage d’utiliser l’ensemble des fonctions proposées ci-dessus pour rechercher le site le plus populaire parmi un très grand nombre de sites (quelques milliers de sites). Expliquer si ce code est adapté à une telle quantité de sites à traiter. Justifier votre réponse."
	La fonction `lePlusPopulaire` est linéaire en le nombre de sommets du graphe elle est donc utilisable sur plusieurs milliers de sites.
	Cependant le calcul de la popularité demande de parcourir tout les sommets et pour chaque sommet tout ses voisins.
	Dans le cas d'un graphe complet ceci correspond à une complexité en $O(n^2)$ avec $n$ le nombre de sommets du graphe, ce qui est lent mais encore utilisable.
	
## Exercice 2 (6 points)


*Cet exercice traite de protocoles de routage, de sécurité des communications et de base de données relationnelle.*

Une agence de voyage propose des croisières en bateau. 
Chaque croisière a un nom unique et passe par quatre escales correspondant à des villes qui ont elles aussi des noms différents.

Pour gérer les réservations de ses clients, l’agence utilise une base de données. 
Voici la description des quatres relations de cette base dont les clés primaires ont été soulignées et les clés étrangères indiquées par un # :

<figure markdown=1>
![](./24-NSIJ1ME1-002.png)
</figure>

Remarque : l’énoncé de cet exercice utilise tout ou une partie des mots suivants du langage SQL : `#!sql SELECT, FROM, WHERE, JOIN ON, INSERT INTO, VALUES, UPDATE SET, OR, AND`.


### Partie A

L’agence de voyage possède deux bureaux distincts.

Elle passe par un prestataire de service qui héberge sa base de données et utilise un système de gestion de base de données relationnelle.

Vous trouverez ci-après un schéma du réseau entre les deux bureaux de l’agence de voyage et le prestataire.

On peut y voir les différents routeurs (nommés de A à I) ainsi que le coût des liaisons entre eux.


<figure markdown=1>
![](./24-NSIJ1ME1-004.png)
<figcaption>Figure 1. Topologie du réseau</figcaption>
</figure>

??? question "Donner deux services rendus par un système de gestion de bases de données relationnelles."
	Un système de gestion de base de données relationnelles permet de garantir la **cohérence** des données ainsi que leur **intégrité**.
    Il permettra aussi d'éviter la **redondance**.

Le protocole RIP (Routing Information Protocol) est un protocole de routage qui minimise le nombre de routeurs par lesquels les paquets transitent.

Le protocole OSPF (Open Shortest Path First) est un protocole de routage qui minimise le coût du transit des paquets.

??? question " Donner la route suivie par une requête issue du bureau numéro 1 jusqu’au prestataire si on utilise le protocole RIP."
	La route la plus courte en terme de nombre de routeurs est: `BEA` depuis le bureau 1 jusqu'au prestataire.
	
??? question "Donner les deux routes que pourrait suivre une requête issue du bureau numéro 2 jusqu’au prestataire si on utilise le protocole OSPF. Donner le coût de chaque route."
    En utilisant le protocole OSPF on pourrait suivre les routes `CIGFDA` ou bien `CIHFDA` qui ont toutes les deux un coût de $2,3$.
	
### Partie B

??? question "Expliquer pourquoi l’attribut `id_client` a été choisi comme clé primaire dans la relation `clients`."
	L'attribut `id_client` a été choisi car deux personnes peuvent être homonymes et avoir le même nom et prénom.
	De même la date de naissance ou le pays ne permettent pas d'identifier une personne.
	On choisit donc un attribut `id_client` numérique pour identifier de manière unique chaque personne.
	
??? question "Définir ce qu’est une clé étrangère. Donner la ou les clés étrangères de chaque relation qui en a en précisant la clé primaire qu’elles référencent."
	Une clé étrangère est un attribut d'une table référençant un attribut d'une autre table.
	Cet autre attribut doit avoir des valeurs uniques mais n'est pas obligatoirement une clé primaire de la table.
	Ici, les clés étrangères sont:

	* `reservations.id_client` référençant `client.id_client`,
	* `reservations.nom_croisiere` référençant `croisieres.nom`
	* `croisieres.escale_1`,`croisieres.escale_2`, `croisieres.escale_3`, `croisieres.escale_4` référençant `villes.nom`


L’agence a obtenu l’autorisation de faire escale dans quatre nouvelles villes : *Puerto saibo*, *Puerto kifecho*, *Puerto kifebo* et *Puerto repo*.
Elle souhaite créer une nouvelle croisière qui passera par ces quatre villes. 
Un stagiaire de l’agence demande de l’aide à une Intelligence Artificielle (IA) :

<figure markdown=1 style="text-align: left">

!!! tip "Pour ajouter la nouvelle croisière nommée ‘Croisière Puerto’ avec ses escales correspondantes, vous pouvez utiliser la requête suivante :"
	```sql
	INSERT INTO croisieres (nom, escale_1, escale_2, escale_3,escale_4)
	VALUES ('Croisière Puerto',
		'Puerto sebo',
		'Puerto kifecho',
		'Puerto kifebo',
		'Puerto repo');
	```


<figcaption>Figure 2 - Réponse de l’IA</ficaption>
</figure>

Il tape alors la requête proposée mais obtient le message d’erreur suivant du SGBD (Systèmes de Gestion de Bases de Données) : `FOREIGN KEY constraint failed`.

??? question "Expliquer l'erreur commise et proposer une solution"
	On a une erreur *FOREIGN KEY constraint failed* qui indique qu'une clé étrangère utilisé ne référence pas une valeur de la clé primaire.
	Cela signifie que l'une des escales du voyage n'existe pas (les escales sont les clés étrangères de la table `croisieres`.
	
	Pour corriger l'erreur, on peut soit ortographier `saibo` correctement en supposant que l'escale *Puerto Saibo* existe.
	Sinon on peut créer les escales manquantes dans la table `villes` avec une requête `#!sql INSERT INTO` avant d'exécuter la requête donnée.

### Partie C

!!! question "Question"
	Jean Barc, un allemand né le 29 juin 1972, demande un geste commercial en raison de sa fidélité à l’agence.
	Expliquer les requêtes SQL suivantes saisies par le gestionnaire :
	
	```sql
	SELECT id_client FROM clients
    WHERE nom = 'Barc' AND prenom = 'Jean' AND date_naissance ='1972/06/29' AND pays = 'Allemagne';
	
	SELECT id_reservation FROM reservations
    WHERE id_client = 1243;
	```

??? success "Réponse"
	Le première requête permet de récupérer l'identifiant client de Jean Barc dans la table `clients` grâce à toutes ses informations personnelles.

	La deuxième requête permet de connaître toutes les réservations associées à l'identifiant client 1243, qui doit être celui de Jean Barc d'après le contexte.
	
??? question "Écrire les deux requêtes précédentes sous la forme d’une requête unique."
	```sql
	SELECT id_reservation
	FROM reservations
	JOIN clients ON reservations.id_client = clients.id_client
	WHERE nom = 'Barc' AND prenom = 'Jean' AND date_naissance = '1972/06/29' AND pays = 'Allemagne';
	```
	
??? question "Un client souhaite modifier sa réservation d’identifiant 20456. Il souhaite remplacer la croisière de cette réservation par la toute dernière offre de l’agence : la *Croisière Puerto*. Écrire une requête SQL qui permet de mettre à jour la base de données pour lui donner satisfaction."
	```sql
	UPDATE reservations
	SET nom_croisiere = 'Croisière Puerto'
	WHERE id_reservation = 20456;
	```
	
??? question "Donner une requête SQL permettant d’obtenir les noms, prénoms et dates de naissance des clients ayant choisi la croisière nommée *Croisière Piano* ou celle nommée *Croisière Puerto*."
	```sql
	SELECT nom, prenom, date_naissance
	FROM clients
	NATURAL JOIN reservations
	WHERE nom_croisiere = 'Croisière Piano' OR nom_croisiere = 'Croisière Puerto';
	```
	
## Exercice 3 (8 points)

*Cet exercice porte sur la programmation orientée objet, sur les arbres binaires de recherche et la récursivité.*

Chaque année, plusieurs courses de chiens de traîneaux sont organisées sur les terrains enneigés.
L’une d’elle, *La Traversée Blanche*, est une course se déroulant en 9 étapes.
L’organisateur de cette course est chargé de créer un programme Python pour aider à la bonne gestion de l’événement.


### Partie A : la classe Chien

Afin de caractériser un chien, l’organisateur décide de créer une classe `Chien` avec les attributs suivants :

- `id_chien`, un nombre entier correspondant au numéro attribué au chien lors de son inscription à la course ;
- `nom`, une chaîne de caractères correspondant au nom du chien ;
- `role`, une chaîne de caractères correspondant au poste occupé par le chien : en fonction de sa place dans l’attelage, un chien a un rôle bien défini et peut être 'leader', 'swing dog', 'wheel dog' ou 'team dog'.
- `id_proprietaire`, un nombre entier correspondant au numéro de l’équipe.

Le code Python incomplet de la classe `Chien` est donné ci-dessous.

```python linenums="1"
class Chien:
	def __init__(self, id_chien, nom, role, id_prop):
		self.id_chien = id_chien
		self.nom = nom
		self.role = role
		self.id_proprietaire = id_prop
		
	def changer_role(self, nouveau_role):
		"""Change le rôle du chien avec la valeur passée en
			paramètre."""
		...
```

Voici un extrait des informations dont on dispose sur les chiens inscrits à la course.

<table>
<thead><tr><th>Chiens inscrits à la course
<tbody><tr><th>id_chien<th> nom role<th> id_proprietaire
<tr><td>40<td> Duke<td> wheel dog<td> 10
<tr><td>41<td> Sadie<td> team dog<td> 10
<tr><td>42<td> Zeus <td>swing dog<td> 11
<tr><td>43<td> Roxie<td> swing dog<td> 11
<tr><td>44<td> Scout<td> team dog<td> 11
<tr><td>45 <td>Ginger <td>team dog <td>11
<tr><td>46<td> Helka<td> team dog<td> 11
</tbody>
</table>


Suite aux inscriptions, l’organisateur procède à la création de tous les objets de type `Chien` et les stocke dans des variables en choisissant un nom explicite.
Ainsi, l’objet dont l’attribut `id_chien` a pour valeur 40 est stocké dans la variable `chien40`.

??? question "Écrire l’instruction permettant d’instancier l’objet `chien40` caractérisant le chien ayant le numéro d’inscription `40`."
	```python
	chien40 = Chien(40,'Duke', 'wheel dog', 10)
	```

??? question "Selon l’état de fatigue de ses chiens ou du profil de l’étape, le musher (nom donné à la personne qui conduit le traîneau) peut décider de changer le rôle des chiens dans l’attelage. Recopier et compléter la méthode changer_role de la classe `Chien`."
	```python linenums="7"
	def changer_role(self, nouveau_role):
		"""Change le rôle du chien avec la valeur passée en
		   paramètre."""
	   if nouveau_role in ['leader', 'swing dog', 'wheel dog', 'team dog']:
		   self.role = nouveau_role
	```
	
??? question "Le propriétaire de Duke décide de lui attribuer le rôle de `'leader'`. Écrire l’instruction permettant d’effectuer cette modification."
	```python
	chien40.changer_role('leader')
	```
### Partie B : la classe Equipe

On souhaite à présent créer une classe `Equipe` ayant les attributs suivants :

- `num_dossard`, un nombre entier correspondant au numéro inscrit sur le dossard du musher ;
- `nom_equipe`, une chaîne de caractères correspondant au nom de l’équipe ;
- `liste_chiens`, une liste d’objets de type `Chien` dont chaque élément correspond à un chien au départ de l’étape du jour ;
- `temps_etape`, une chaîne de caractères (par exemple `'2h34'`) représentant le temps mis par l’équipe pour parcourir l’étape du jour ;
- `liste_temps`, une liste de chaînes de caractères permettant de stocker les temps de l’équipe pour chacune des 9 étapes. Cet attribut peut, par exemple, contenir la liste : `['4h36', '3h57', '3h09', '5h49', '4h45', '3h26','4h57', '5h52', '4h31']`.


On donne le code Python suivant de la classe Equipe.

```python linenums="1"
class Equipe:
	def __init__(self, num_dossard, nom_equipe):
		self.num_dossard = num_dossard
		self.nom_equipe = nom_equipe
		self.liste_chiens = []
		self.temps_etape = ''
		self.liste_temps = []

	def ajouter_chien(self, chien):
		self.liste_chiens.append(chien)

	def retirer_chien(self, numero):
		...

	def ajouter_temps_etape(self, temps):
		self.liste_temps.append(temps)
```

Pour la première étape, le musher de l’équipe numéro 11, représentée en Python par l’objet `eq11`, décide de constituer une équipe avec les quatre chiens identifiés par les numéros 42, 44, 45 et 46.
On donne ci-dessous les instructions Python permettant de créer l’équipe eq11 et l’attelage constitué des 4 chiens précédents.

```python linenums="1"
eq11 = Equipe(11, 'Malamutes Endurants')
eq11.ajouter_chien(chien42)
eq11.ajouter_chien(chien44)
eq11.ajouter_chien(chien45)
eq11.ajouter_chien(chien46)
```

Malheureusement, le musher s’aperçoit que sa chienne Helka, chien numéro 46, n’est pas au mieux de sa forme et il décide de la retirer de l’attelage.


??? question " Recopier et compléter la méthode `retirer_chien` ayant pour paramètre `numero`, un entier correspondant au numéro attribué au chien lors de l’inscription, et permettant de mettre à jour l’attribut `liste_chiens` après retrait du chien dont la valeur de l’attribut `id_chien` est `numero`."
	```python linenums='12'
	def retirer_chien(self, numero):
		indice_chien = -1
		for i in range(len(self.liste_chiens)
			if self.liste_chiens[i].id_chien == numero:
				indice_chien = i
	    if indice_chien != -1:
			self.liste_chiens.pop(i)
			# ou del self.liste_chiens[i]
	```

??? question "En vous aidant de la fonction précédente, écrire l’instruction qui permet de retirer Helka de l’attelage de l’équipe `eq11`."
	```python
	eq11.retirer_chien(46)
	```
On donne à présent le code Python d’une fonction `convert` prenant pour paramètre `chaine`, une chaîne de caractères représentant une durée, donnée en heure et minute.
On supposera que cette durée est toujours strictement inférieure à 10 heures, temps maximal fixé par le règlement pour terminer une étape.

```python linenums="1"
def convert(chaine):
	heure_dec = int(chaine[0]) + int(chaine[2] + chaine[3])/ 60
	return heure_dec
```

??? question "Indiquer le résultat renvoyé par l’appel `convert('4h36')`."
	La fonction commencer par convertir le premier caractère en entier, ici: 4.
	Ensuite elle fait la concaténation des deux derniers caractères ici: `'36'`.
	Puis, elle convertit cette valeur en entier (36) et la divise par 60 (ce qui donne 0,6.
	Enfin on ajoute les deux valeurs soit: **4,6**.
	Ceci correspond à l'heure représentée sous forme décimale.
	

??? question "Écrire une fonction `temps_course` qui prend pour paramètre `equipe` de type `Equipe` et qui renvoie un nombre flottant correspondant au cumul des temps de l’équipe equipe à l’issue des 9 étapes de la course. On rappelle que la classe `Equipe` dispose d’un attribut `liste_temps`."
	```python
	def temps_course(equipe):
		total = 0
		for t in equipe.liste_temps:
			total += convert(t)
		return total
	```

	Ou bien en une seule ligne:
	```python
	def temps_course(equipe):
		return sum(convert(t) for t in equipe.liste_temps)
	```
	
### Partie C : classement à l’issue d’une étape

Chaque jour, à la fin de l’étape, on décide de construire un Arbre Binaire de Recherche (ABR) afin d’établir le classement des équipes.
Chaque nœud de cet arbre est un objet de type Equipe.

Dans cet arbre binaire de recherche, en tout nœud :

- toutes les équipes du sous-arbre gauche sont strictement plus rapides que ce nœud ;
- toutes les équipes du sous-arbre droit sont moins rapides ou sont à égalité avec ce nœud.

Voici les temps, en heure et minute, relevés à l’issue de la première étape :

<table>
<thead><tr><th>Temps à l’arrivée de la première étape</thead>
<tbody>
<tr><th scope="row">Equipe <td>eq1<td> eq2<td> eq3<td> eq 4<td>eq5<td> eq6<td> eq7<td> eq8<td> eq9<td> eq10<td> eq</tr>
<tr><th scope="row">Temps <td>4h3 <td>3h57<td> 3h09<td> 5h49<td> 4h45<td> 3h26<td> 4h51<td> 5h52<td> 4h31<td> 3h44<td> 4h
</tbody>
</table>


Dans l’arbre binaire de recherche initialement vide, on ajoute successivement, dans cet ordre, les équipes `eq1, eq2, eq3, ..., eq11`, 11 objets de la classe `Equipe` tous construits sur le même modèle que l’objet `eq11` précédent.

	
!!! question "Question"
	Dans l’arbre binaire de recherche ci-dessous, les nœuds `eq1` et `eq2` ont été insérés.
	Recopier et compléter cet arbre en insérant les 9 nœuds manquants.

??? success "Réponse"
	```mermaid
	flowchart TD
		eq1 --- eq2 & eq4
		eq2 --- eq3 & eq9
		eq4 --- eq5 & eq8
		eq5 --- N1:::hidden & eq7
		eq3 --- N2:::hidden & eq6
		eq6 --- N3:::hidden & eq10
		eq9 --- eq11 & N4:::hidden
		
		classDef hidden display: none;
	```

<figure markdown=1>
![](./24-NSIJ1ME1-008.png)
<figcaption>Figure 1. Premiers éléments de l’ABR</figcaption>
</figure>

??? question "Indiquer quel parcours d’arbre permet d’obtenir la liste des équipes classées de la plus rapide à la plus lente."
	Dans un ABR, c'est le parcours **infixe** qui permet d'obtenir la liste des équipes de la plus petite à la plus lente.

On donne ci-dessous la classe `Noeud`, permettant de définir les arbres binaires :

```python linenums="1"
class Noeud:
	def __init__(self, equipe, gauche = None, droit = None):
		self.racine = equipe
		self.gauche = gauche
		self.droit = droit
```

On donne ci-dessous le code d’une fonction `construction_arbre` qui, à partir d’une liste d’éléments de type `Equipe` permet d’insérer successivement chaque nœud à sa place dans l’ABR.

```python linenums="1"
def construction_arbre(liste):
	a = Noeud(liste[0])
	for i in range(1,len(liste)):
		inserer(a, liste[i])
	return a
```

La fonction `construction_arbre` fait appel à la fonction `inserer` qui prend pour paramètre `arb`, de type `Noeud`, et `eq`, de type `Equipe`.
Cette fonction construit le nœud à partir de `eq` et l’insère à sa place dans l’ABR.

```python linenums="1"
def inserer(arb, eq):
	""" Insertion d'une équipe à sa place dans un ABR contenant
	au moins un noeud."""
	if convert(eq.temps_etape) < convert(arb.racine.temps_etape):
		if arb.gauche is None:
			arb.gauche = ...
		else:
			inserer(..., eq)
	else:
		if arb.droit is None:
			arb.droit = Noeud(eq)
		else:
			...
```

??? question "Expliquer en quoi la fonction `inserer` est une fonction récursive."
	La fonction `inserer` fait appel à elle même à la ligne 8, elle est donc récursive.


??? question "Recopier et compléter les lignes 6, 8 et 13 de la fonction `inserer`."
	```python hl_lines="6 8 13"
	def inserer(arb, eq):
		""" Insertion d'une équipe à sa place dans un ABR contenant
			au moins un noeud."""
		if convert(eq.temps_etape) < convert(arb.racine.temps_etape):
			if arb.gauche is None:
				arb.gauche = Noeud(eq)
			else:
				inserer(arb.gauche, eq)
	    else:
			if arb.droit is None:
				arb.droit = Noeud(eq)
			else:
				inserer(arb.droit,eq)
	```


!!! question "Recopier et compléter les lignes 3 et 5 de la fonction est_gagnante ci-dessous qui prend en paramètre un ABR arbre, de type `Noeud`, et qui renvoie le nom de l’équipe ayant gagné l’étape."
	```python linenums="1"
	def est_gagnante(arbre):
		if arbre.gauche == None:
			return ...
		else:
			return ...
	```

??? success "Réponse"
	```python hl_lines="3 5"
	def est_gagnante(arbre):
		if arbre.gauche == None:
			return arbre.racine.nom_equipe
		else:
			return est_gagnante(arbre.gauche)
	```
	
### Partie D : classement général

On décide d’établir un classement général obtenu à partir du cumul des temps mis par chaque équipe pour parcourir l’ensemble des 9 étapes.

Sur le même principe que l’arbre de la partie précédente, on construit l’ABR ci-dessous qui permet, grâce au parcours d’arbre approprié, d’établir ce classement général des équipes.

<figure markdown=1>
![](./24-NSIJ1ME1-010.png)
<figcaption>Figure 2. ABR du classement général</figcaption>
</figure>

Le règlement prévoit la disqualification d’une équipe en cas de non-respect de celui-ci.
Il s’avère que l’équipe 2 et l’équipe 5 doivent être disqualifiées pour manquement au règlement. 
Les nœuds `eq2` et `eq5` doivent donc être supprimés de l’ABR précédent.

Pour supprimer un nœud $N$ dans un ABR, trois possibilités se présentent :

- le nœud $N$ à supprimer est une feuille : il suffit de le retirer de l’arbre ;
- le nœud $N$ à supprimer n’a qu’un seul fils : on relie le fils de $N$ au père de $N$ et on supprime le nœud $N$ ;
- le nœud $N$ à supprimer possède deux fils : on le remplace par son successeur (l’équipe qui a le temps immédiatement supérieur) qui est toujours le minimum de ses descendants droits.


??? question "Dessiner le nouvel arbre de recherche `a_final` obtenu après suppression des équipes `eq2` et `eq5` dans l’ABR correspondant au classement général."
	```mermaid
	flowchart TD
		eq1 --- eq3 & eq9
		eq3 --- eq11 & eq4
		eq9 --- eq8 & eq6
		eq8 --- eq10 & N1:::hidden
		eq6 --- N2:::hidden & eq7

	classDef hidden display: none;
	```
	
L’organisateur souhaite disposer d’une fonction rechercher permettant de savoir si une équipe a été disqualifiée ou non.
On donne les spécifications de la fonction `rechercher`, prenant en paramètre `arbre` et `equipe`.

```python linenums="1"
def rechercher(arbre, equipe):
	"""
	Paramètres
	---------
		arbre : un ABR, non vide, de type Noeud, représentant le 
		classement général.
		equipe : un élément, de type Equipe, dont on veut déterminer
		l'appartenance ou non à l'ABR arbre.
	Résultat
	---------
		Cette fonction renvoie True si equipe est un nœud de arbre,
		False sinon.
	"""
	...
```

Pour cette fonction (`a_final` désigne l’arbre obtenu à la question précédente, après suppression des équipes 2 et 5) :

- l’appel `rechercher(a_final, eq1)` renvoie `True` ;
- l’appel `rechercher(a_final, eq2)` renvoie `False`.
??? question "Écrire le code de la fonction `rechercher`."
	```python linenums="15"
	if arbre.racine == equipe:
		return True
	elif convert(arbre.racine.temps_etape) < convert(equipe.temps_etape):
		return arbre.droit is not None and rechercher(arbre.droit,equipe)
	else:
		return arbre.gauche is not None and rechercher(arbre.gauche,equipe)
	```
