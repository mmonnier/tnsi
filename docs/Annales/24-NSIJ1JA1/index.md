---
title: Centres étrangers, Groupe 2, Jour 1
keywords: baccalaureat, 24-NSIJ1JA1, 2024
---

# BACCALAURÉAT GÉNÉRAL

Le sujet est disponible [ici](./24-NSIJ1JA1.pdf)

**ÉPREUVE D'ENSEIGNEMENT DE SPÉCIALITÉ - SESSION 2024 - NUMÉRIQUE ET SCIENCES INFORMATIQUES**

**JOUR 1**

Durée de l'épreuve : *3 heures 30*

L'usage de la calculatrice n'est pas autorisé.

Le sujet est composé de trois exercices indépendants.
Le candidat traite les trois exercices.

## EXERCICE 1 (6 points)

*Cet exercice porte sur la programmation Python, la programmation orientée objet et l'algorithmique.*

Une entreprise doit placer des antennes relais le long d'une rue rectiligne.
Une antenne relais de portée (ou rayon) $pp$ couvre toutes les maisons qui sont à une distance inférieure ou égale à $pp$ de l'antenne.

Connaissant les positions des maisons dans la rue, l'objectif est de placer les antennes le long de la rue, pour que toutes les maisons soient couvertes, tout en en minimisant le nombre d'antennes utilisées.
La rue est représentée par un axe, et les maisons sont représentées des points sur cet
axe :

<figure markdown=1>
![](./24-NSIJ1JA1-000.png)
<figcaption>Figure 1. Deux maisons sur une rue, repérée par leur abscisse : 1 et 3,5</figcaption>
</figure>

Les entités manipulées sont modélisées en utilisant la programmation orientée objet.

```python
class Maison:
	def __init__(self, position):
		self._position = position
	def get_pos_maison(self):
		return self._position
class Antenne:
	def __init__(self, position, rayon):
		self._position = position
		self._rayon = rayon
	def get_pos_antenne(self):
		return self._position
	def get_rayon(self):
		return self._rayon
```

??? question  "Donner le code qui crée et initialise deux variables `m1` et `m2` avec des instances de la classe `Maison` situées aux abscisses 1 et 3,5 (Figure 1)."
	```python
	m1 = Maison(1)
	m2 = Maison(3.5)
	```

On ajoute à présent une antenne ayant un rayon d'action de 1 à la position 2,5 :

<figure markdown=1>
![](24-NSIJ1JA1-002.png)
<figcaption>Figure 2. L'antenne placée en 2,5 et de rayon d'action 1 couvre la maison en 3,5 mais pas celle en 1</figcaption>
</figure>

??? question "Donner le code qui crée la variable a correspondant à l'antenne à la position 2,5 avec le rayon d'action 1."
	```python
	a = Antenne(2.5,1)
	```

On souhaite modéliser une rue par une liste d'objets de type Maison. 
Cette liste sera construite à partir d'une autre liste contenant des nombres correspondant aux positions des maisons. 
La fonction `creation_rue` réalise ce travail.
Elle prend en paramètre une liste de positions et renvoie une liste d'objets de type `Maison`.

!!! question "Recopier le schéma ci-dessous et le compléter pour donner une représentation graphique de la situation créée par : `creation_rue([0, 2, 3, 4, 5, 7, 9, 10.5, 11.5])`"
	<figure markdown=1>
	![](24-NSIJ1JA1-004.png)
	<figcaption>Figure 3. Axe pour représenter le problème avec 9 maisons</figcaption>
	</figure>

!!! question "Compléter le code donné ci-dessous de la fonction `creation_rue`."
	```python linenums="1"
	def creation_rue(pos):
		pos.sort()
		maisons = []
		for p in pos:
			m = Maison(p)
			maisons.append(...)
		return ...
	```
	Pour rappel : la commande `tab.sort()` trie la liste `tab`.	
??? success "Réponse"
	```python linenums="1" hl_lines="6 7"
	def creation_rue(pos):
		pos.sort()
		maisons = []
		for p in pos:
			m = Maison(p)
			maisons.append(m)
		return maisons
	```



La méthode `couvre` de la classe `Antenne` prend en paramètre un objet de type `Maison` et indique par un booléen si l'antenne couvre la maison en question ou non. 
La méthode peut être utilisée ainsi (en supposant que les objets précédents `m1`, `m2` et `a` existent).

```pycon
>>> a.couvre(m1)
False
>>> a.couvre(m2)
True
```
!!! question "Compléter la fonction `couvre`, ci-dessous, en veillant à ne pas accéder directement aux attributs d'une maison depuis la classe `Antenne` (on pourra utiliser les méthodes `get_pos_maison` , `get_pos_antenne` et `get_rayon`)."
	```python linenums="1"
	# Méthode à ajouter dans la classe Antenne
	def couvre(self, maison):
		# Code à compléter (éventuellement plusieurs lignes)
		...
	```
	Pour rappel : la fonction valeur absolue se nomme `abs()` en Python.
	
??? success "Réponse"
	```python linenums="1" hl_lines="2-4"
	def couvre(self, maison):
		pos_m = maison.get_pos_maison()
		pos_a,ray = self.get_pos_antenne(),self.get_rayon()
		return pos_a-ray <= pos_m <= pos_a+ray
	```
La fonction `strategie_1` est donnée ci-dessous. 
L'objectif est de placer des antennes dans une rue. 
Elle est fournie à la société qui place les antennes.
La fonction prend en paramètre une liste d'objets de type `Maison` (qu'on supposera triée par abscisse croissante) et le rayon d'action des antennes (float). 
Cette fonction renvoie une liste d'objets de type `Antenne` ayant ce rayon d'action et couvrant toutes les maisons de la rue.

```python linenums="1"
def strategie_1(maisons, rayon):
	''' Prend en paramètre une liste de maisons et le rayon
d'action des antennes et renvoie une liste d'antennes
	'''
	antennes = [Antenne(maisons[0].get_pos_maison(), rayon)]
	for m in maisons[1:]:
		if not antennes[-1].couvre(m):
			antennes.append(Antenne(m.get_pos_maison(), rayon))
	return antennes
```

Pour rappel :

* `tab[1:]` correspond aux éléments de tab à partir de l'indice 1 jusqu'à la fin de la liste ;
* `tab[-1]` correspond au dernier élément de la liste tab.

!!! question "Indiquer ce que renvoie cette suite d'instructions après exécution."
	```pycon
	>>> maisons = creation_rue([0, 2, 3, 4, 5, 7, 9, 10.5, 11.5])
	>>> antennes = strategie_1(maisons, 2)
	>>> print([a.get_pos_antenne() for a in antennes])
	```
??? success "Réponse"
	Cette suite d'instructions commence par créer une rue avec 9 maisons, aux abscisses données.
	On applique ensuite la stratégie 1 qui consiste à placer une antenne sur chaque maison, sauf si la dernière antenne placée couvre déjà la maison.
	
	On affiche ensuite les antennes placées ce qui donne ici (car les antennes ont un rayon de 2):
	`[0,3,7,10.5]`
	
Une amélioration est possible et la société qui pose les antennes souhaite implémenter l'algorithme suivant :

* considérer les maisons dans l'ordre des abscisses croissantes ;
* dès qu'une maison n'est pas couverte, placer une antenne à la plus grande abscisse telle qu'elle couvre cette maison. 

Par exemple, si la maison d'abscisse 5 est la première maison non couverte, alors, on placera l'antenne en 5 + $rr$ si $rr$ est le rayon d'action de l'antenne.

!!! question "Question"
	On considère la rue composée des maisons situées aux abscisses `[0, 2, 3, 4, 5, 7, 9, 10.5, 11.5]`. 
	Recopier et compléter le schéma ci-dessous en indiquant l'emplacement des antennes selon cette nouvelle stratégie.
	On suppose que le rayon d'action est toujours 2.
	<figure markdown=1>
	![](./24-NSIJ1JA1-006.png)
	<figcaption>Figure 4. Axe pour représenter le résultat de la nouvelle stratégie</figcaption>
	</figure>

??? question "Cet algorithme étant a priori plus économe en antennes, proposer une fonction `strategie_2`, sur le modèle de `strategie_1` qui implémente cette nouvelle stratégie."
	```
	Entrées: 
		La liste ms  des maisons
		Le rayon rr des antennes
	Sortie
		La liste des antennes
		
	Trier la liste ms par ordre croissant des abscisses
	as <- Liste vide
	Ajouter à as l'antenne à l'abscisse ms[0] + rr
	
	Pour chaque maison m de ms sauf la première:
		Si la dernière antenne ajoutée ne couvre pas m:
			Soit mx l'abscisse de m
			Ajouter à as l'antenne à l'abscisse mx + rr
	Renvoyer as
	```

??? question "Comparer le coût en nombre d'opérations des deux stratégies en fonction du nombre $n$ de maisons dans la rue. On admet que le coût de la fonction `append` est constant."
	Dans les deux algorithmes on effectue $n-1$ conditions et une opération par condition au maximum.
	Le pire cas est donc celui où la condition est remplie à chaque tour.
	
	Ce cas se présente pour la stratégie 1 si toutes les maisons sont distantes d'un nombre strictement supérieure au rayon des antennes.
	On effectue donc dans ce cas $n-1$ opérations `append`, on a donc une complexité linéaire.
	
	Pour la stratégie 2, ce cas se présente si toutes les maisons sont distantes d'un nombre strictement supérieur à deux fois le rayon des antennes (car on place les antennes entre les maisons et pas dessus).
	On effectue donc dans ce cas $n-1$ opérations `append` soit la même complexité que la stratégie 1.

## EXERCICE 2 (6 points)
*Cet exercice porte sur les graphes, la programmation, la structure de pile et l'algorithmique des graphes.*

On s'intéresse à la fabrication de pain. 
La recette est fournie sous la forme de tâches à réaliser. 
Cette recette est réalisée par une personne seule.


	(a) Préparer 500g de farine.
	(b) Préparer 1/3 de litre d'eau (33cl).
	(c) Préparer 1 c. à café de sel.
	(d) Préparer 20g de levure de boulanger.
	(e) Faire tiédir l'eau dans une casserole.
	(f) Délayer la levure dans l'eau tiède.
	(g) Laisser reposer la levure 5 minutes.
	(h) Préparer un grand saladier.
	(i) Verser la farine dans le saladier.
	(j) Verser le sel dans le saladier.
	(k) Mélanger la farine et le sel puis creuser un puits.
	(l) Verser l'eau mélangée à la levure dans le puits.
	(m) Pétrir jusqu'à obtenir une pâte homogène.
	(n) Couvrir à l'aide d'un linge humide et laisser fermenter au moins 1h30.
	(o) Disposer dans le fond du four un petit récipient contenant de l'eau.
	(p) Préchauffer un four à 200 degrés Celsius.
	(q) Fariner un plan de travail.
	(r) Verser la pâte à pain sur le plan de travail.
	(s) Pétrir rapidement la pâte à pain.
	(t) Disposer la pâte dans un moule à cake.
	(u) Mettre au four pour 15 à 20 minutes, arrêter le four et sortir le pain.


La figure 1 représente les différentes tâches et les dépendances entre ces tâches sous la forme d'un graphe. 
Chaque sommet du graphe représente une tâche à réaliser.
Les dépendances entre les tâches sont représentées par les arcs entre les sommets.
Par exemple, il y a une flèche sur l'arc qui part du sommet d'étiquette (l) et qui atteint le sommet d'étiquette (m) car il faut avoir réalisé la tâche “`Verser l'eau mélangée à la levure dans le puits.`” (l) avant de pouvoir réaliser la tâche “`Pétrir jusqu'à obtenir une pâte homogène.`” (m).

<figure markdown=1>
![](./24-NSIJ1JA1-008.png)
<figcaption> Figure 1. Recette du pain : tâches à effectuer avec leurs dépendances </figcaption>
</figure>


??? question "Dire, sans justifier, s'il s'agit d'un graphe orienté ou non orienté."
	Il s'agit d'un graphe orienté (les arêtes sont des flèches).


!!! question "D'après le graphe, dire s'il est possible d'effectuer les réalisations dans chacun des ordres suivants :"
	- réaliser la tâche (f) puis la tâche (g)
	- réaliser la tâche (g) puis la tâche (f);
	- réaliser la tâche (i) puis la tâche (j);
	- réaliser la tâche (j) puis la tâche (i);

??? success "Réponse"
	- Le graphe indique que (f) doit être réalisée avant (g), on peut donc réaliser (f) puis (g);
	- pour la raison précédente, on ne peut pas réaliser (g) puis (f);
	- les tâches (i) et (j) n'ont pas de dépendances entre elles nous pouvons donc les exécuter dans n'importe quel ordre (i) puis (j) ou (j) puis (i).


??? question "Donner toutes les tâches qu'il faut nécessairement avoir réalisées depuis le début pour pouvoir réaliser la tâche (k). Ne donner que les tâches nécessaires."
	Pour réaliser la tache (k) on doit nécessairement avoir effectué (i) et (j).
	Pour réaliser (i) on doit nécessairement avoir effectué (a) et (h).
	Pour réaliser (j) on doit nécessairement avoir effectué (c) et (h).
	
	Donc pour réaliser (k) on doit avoir effectué (i), (j), (a), (c) et (h).

??? question "Indiquer, sans justifier, si le graphe de la Figure 1 contient un cycle."
	Ce graphe ne contient aucun cycle, en effet aucune flèche ne va d'une lettre vers une lettre inférieure dans l'alphabet.
	
### Graphe de tâches

On s'intéresse désormais de manière plus générale à un graphe de tâches avec des dépendances.
Les sommets sont nommés par des indices.
Comme précédemment, un arc orienté d'un sommet d'indice `i` à un sommet d'indice `j` signifie que la tâche représentée par le sommet d'indice `i` doit être réalisée avant la tâche représentée par le sommet d'indice `j`.

<figure markdown=1>
![](./24-NSIJ1JA1-010.png)
<figcaption>Figure 2. Exemple de graphe de dépendances entre 6 tâches</figcaption>
</figure>


??? question "Déterminer un ordre permettant de réaliser toutes les tâches représentées dans le graphe de la Figure 2 en respectant les dépendances entre les tâches."
	Pour effectuer toutes les taches on peut suivre l'ordre: `0,2,1,3,5,4`.

Voici une matrice d'adjacence d'un graphe écrite en langage Python et telle que si `M[i][j] = 1` alors il existe un arc qui va du sommet d'indice $i$ au sommet d'indice $j$.
Par exemple, `M[0][1] = 1` alors il existe un arc qui va du sommet d'indice 0 au sommet d'indice 1.

```python
M = [ [0, 1, 0, 0, 0],
	  [0, 0, 1, 0, 0],
	  [0, 0, 0, 1, 0],
	  [0, 1, 0, 0, 1],
	  [0, 0, 0, 0, 0] ]
```

??? question "Représenter le graphe associé à cette matrice d'adjacence. Les noms des sommets seront leurs indices."
	```mermaid
	flowchart LR
		0 --> 1
		1 --> 2
		2 --> 3
		3 --> 1 & 4
	```

??? question "Déterminer s'il est possible de trouver un ordre permettant de réaliser les tâches représentées par le graphe de la question précédente en respectant leurs dépendances. Si oui, donner l'ordre. Si non, expliquer pourquoi."
	On ne peut pas trouver un ordre permettant de réaliser les taches ci-dessus car:
	
	- pour effectuer la tache 1 il faut effectuer la tache 3;
	- pour effectuer la tache 3 il faut effectuer la tache 2;
	- pour effectuer la tache 2 il faut effectuer la tache 1.
	
	Nous avons donc une dépendance cyclique entre 1,2 et 3 aucun ordre n'est donc valable.

Voici le code Python d'une fonction `mystere`.

```python linenums="1"
def mystere(graphe, s, n, ouverts, fermes, resultat):
	""" Paramètres :
		graphe    un graphe représenté par une matrice d'adjacence
		s         l'indice d'un sommet du graphe
		n         le nombre de sommets du graphe
		ouverts   une liste de booléens permettant de savoir
			      si le traitement d'un sommet a été commencé
		fermes    une liste de booléens permettant de savoir
			      si le traitement d'un sommet a été terminé
		Retour :  False s'il y a eu un "problème", True sinon.
		Le paramètre resultat sera modifié ultérieurement.
		"""
		if ouverts[s]:
			return False
		if not fermes[s]:
			ouverts[s] = True
			for i in range(n):
				if graphe[s][i] == 1:
					val = mystere(graphe, i, n, ouverts, fermes,resultat)
					if not val:
						return False
	        ouverts[s] = False
			fermes[s] = True
			# ...
	    return True
```

!!! question "En utilisant la matrice `M` donnée précédemment, déterminer si la variable `ok` vaut `True` ou `False` à l'issue des instructions suivantes :"
	```python linenums="1"
	n = len(M)
	ouverts = [ False for i in range(n) ]
	fermes = [ False for i in range(n) ]
	ok = mystere(M, 1, n, ouverts, fermes, None)
	```
	Décrire précisément les appels effectués à la fonction `mystere` et les valeurs des tableaux `ouverts` et `fermes` lors de chaque appel. 
	On pourra recopier et compléter le tableau ci-dessous.
	<table>
		<thead>
			<tr><th scope="col">Appel `mystere`
				<th scope="col">variable `ouverts`
				<th scope="col">variable `fermes`
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Avant l'appel mystere
				<td>`[F,F,F,F,F]`
				<td>`[F,F,F,F,F]`
			<tr>
				<td>`mystere(M,1,5,[F,F,F,F,F],[F,F,F,F,F],None)`
				<td>`[F,T,F,F,F]`
				<td>`[F,F,F,F,F]`
			<tr>
				<td>`mystere(M,2,5,[F,T,F,F,F],[F,F,F,F,F],None)`
				<td>`[F,T,T,F,F]`
				<td>`[F,F,F,F,F]`
			<tr>
				<td>...
	    </tbody>
	</table>

??? success "Réponse"

??? question "De manière générale, expliquer dans quel cas cette fonction `mystere` renvoie `False`."

L'objectif est d'utiliser la fonction `mystere` pour écrire une fonction `ordre_realisation` qui, lorsque c'est possible, détermine l'ordre de réalisation des tâches d'un graphe donné par sa matrice d'adjacence en respectant les dépendances entre les tâches.
Une structure de données de pile est représentée par une classe `Pile` qui possède les méthodes suivantes :

* la méthode `estVide` qui renvoie `True` si la pile représentée par l'objet est vide, `False` sinon ;
* la méthode `empiler` qui prend en paramètre un élément et l'ajoute au sommet de la pile ;
* la méthode `depiler` qui renvoie la valeur du sommet de la pile et enlève cet élément.

!!! question "Déterminer la valeur associée à la variable `elt` après l'exécution des instructions suivantes :"
	```pycon
	>>> essai = Pile()
	>>> essai.empiler(3)
	>>> essai.empiler(2)
	>>> essai.empiler(10)
	>>> elt = essai.depiler()
	>>> elt = essai.depiler()
	```

??? success "Réponse"
	| 
Lorsqu'il en existe un, un ordre de réalisation des tâches sera représenté par un objet de classe `Pile` contenant tous les sommets du graphe de manière à ce que les tâches qu'il faut réaliser en premier se retrouvent au sommet de la pile.
La fonction `ordre_realisation` est écrite de la manière suivante :
```python linenums="1"
def ordre_realisation(graphe):
	n = len(graphe)
	ouverts = [ False for i in range(n) ]
	fermes = [ False for i in range(n) ]
	ordre = Pile()
	ok = True
	s = 0
	while (ok and s < n):
		ok = mystere(graphe, s, n, ouverts, fermes, ordre)
		s = s + 1
	if ok :
		return ordre
	return None
```
??? question "Sachant que dans la fonction `mystere`, la ligne 24 peut être remplacée par une ou plusieurs instructions, donner ce qu'il faut écrire pour que, lorsque c'est possible, `ordre_realisation` renvoie effectivement un ordre de réalisation des tâches du graphe."

## EXERCICE 3 (8 points)
*Cet exercice porte sur la programmation Python, la programmation orientée objet, les bases de données relationnelles et les requêtes SQL.*

### Partie A

Une entreprise, présente sur différents sites en France, attribue à chacun de ses employés un numéro de badge unique.
Dans le tableau ci-dessous, on donne le numéro de badge, le nom, le prénom et les années de naissance et d'entrée dans l'entreprise de quelques salariés. 

| numéro badge | nom     | prénom   | année de naissance | année d'entrée |
|--------------|---------|----------|--------------------|----------------|
| 112          | LESIEUR | Isabelle | 1982               | 2005           |
| 2122         | VASSEUR | Adrien   | 1962               | 1980           |
| 135          | HADJI   | Hakim    | 1992               | 2015           |

Pour chaque personne, on souhaite stocker les informations dans un objet de la classe `Personne` définie ci-dessous :

```python linenums="1"
class Personne():
	def __init__(self, num, n , p , a_naiss, a_entree):
		self.num_badge = num
		self.nom = n
		self.prenom = p
		self.annee_naissance = a_naiss
		self.annee_entree = a_entree
```
??? question "Écrire à l'aide du tableau précédent, l'instruction permettant de créer l'objet `personneA` de la première personne du tableau : LESIEUR Isabelle."

??? question "Donner l'instruction permettant d'obtenir le numéro de badge de l'objet `personneA` instancié à la question précédente."

On souhaite ajouter une méthode `annee_anciennete` à la classe Personne qui donne le nombre d'années d'ancienneté d'une personne au sein de l'entreprise. 
Par exemple: Madame LESIEUR Isabelle a une ancienneté dans l'entreprise de 19 ans en considérant que nous sommes en 2024.

!!! question "Recopier et compléter le code suivant de la méthode `annee_anciennete` :"
	```python linenums="1"
	def annee_anciennete(self):
		return ...
	```
	
??? success "Réponse"

On considère la classe `Personnel` qui modélise la liste du personnel d'une entreprise et dont le début de l'implémentation est la suivante :

```python linenums="1"
class Personnel:
	def __init__(self):
		self.liste = []
```

??? question "Écrire la méthode ajouter permettant d'ajouter un objet de type `Personne` à la liste du personnel de l'entreprise de la classe `Personnel`."

??? question "Écrire la méthode `effectif` de la classe `Personnel`. Cette méthode devra renvoyer le nombre de personnes présentes dans l'entreprise."

!!! question "Recopier et compléter la méthode `donne_nom` de la classe `Personnel`." 
	Cette méthode prend en paramètre le numéro de badge d'une personne et renvoie le nom de la personne correspondant à ce badge si elle existe, ou `None` sinon.
	```python linenums="1"
	def donne_nom(..., num):
		for elt in self.liste:
			if ... == num:
				return ...
	return ...
	```

??? success "Réponse"

Lors de la célèbre cérémonie des vœux, l'entreprise souhaite mettre à l'honneur les personnes ayant exactement 10 ans d'ancienneté dans l'entreprise. 

??? question "Écrire une méthode de la classe `Personnel` `nb_personne_honneur` qui prend en paramètre l'année de la cérémonie et qui retourne le nombre de personne(s) à mettre à l'honneur."

??? question "Écrire une méthode `plus_anciens` de la classe `Personnel` qui retourne la liste des numéros de badge des personnes ayant la plus grande ancienneté dans l'entreprise."

### Partie B

On utilise maintenant une base de données relationnelle. 
La table `Personnel` dont un extrait est donné ci-dessous contient toutes les données importantes sur le personnel de l'entreprise. 
L'attribut `num_centre` désigne le numéro du centre dans lequel travaille une personne.

<table>
<thead>
	<tr><th>Personnel
<tbody>
	<tr>
		<th>num_badge
		<th>nom
		<th>prenom
		<th>num_centre
		<th>annee_naiss
		<th>annee_debut
	<tr>
		<td>112
		<td>LESIEUR
		<td>Isabelle
		<td>1
		<td>1982
		<td>2005
	<tr>
		<td>2122
		<td>VASSEUR
		<td>Adrien
		<td>2
		<td>1962
		<td>1980
	<tr>
		<td>135
		<td>HADJI
		<td>Hakim
		<td>1
		<td>1992
		<td>2015
	</tbody>
</table>

L'attribut `num_badge` est la clé primaire pour la table `Personnel`.

!!! question "Décrire par une phrase en français le résultat de la requête SQL suivante :"
	```sql
	SELECT nom, prenom
	FROM Personnel
	WHERE num_centre = 2;
	```

??? question "Monsieur HADJI Hakim vient d'obtenir une mutation pour le centre numéro 3.Donner la requête permettant de modifier son numéro de centre sachant que son numéro de badge est 135."

On souhaite proposer plus d'informations sur les différents centres de l'entreprise.
Pour cela, on crée une deuxième table Centre avec les attributs suivants :

* `num` de type INT ;
* `nom` de type TEXT ;
* `num_tel` de type TEXT ;
* `ville` de type TEXT.
<table>
	<thead>
		<tr><th> Table Centre
	</thead>
	<tbody>
		<tr>
			<th>num
			<th>nom
			<th>num_tel
			<th>ville
		<tr>
			<td>1
			<td>Normandie
			<td>0450646859
			<td>Caen
		<tr>
			<td>2
			<td>PACA
			<td>0450646859
			<td>Marseille
    </tbody>
</table>

??? question "Expliquer l'intérêt d'utiliser deux tables (`Personnel` et `Centre`) au lieu de regrouper toutes les informations dans une seule table."
??? question "Expliquer comment les tables `Centre` et `Personnel` sont mises en relation."
??? question "Écrire une requête permettant d'avoir les noms des personnes travaillant dans le centre de Lille et ayant été embauchées entre 2015 (inclus) et 2020 (inclus)."

Le centre de Normandie vient d'être fermé, mais les personnes de ce centre n'ont pas encore été affectées dans leur nouveau centre. 
On souhaite mettre à jour la table `Centre` en premier à l'aide de la requête suivante.

```sql
DELETE *
FROM Centre
WHERE nom = 'Normandie';
```
??? question "Expliquer pourquoi cette requête a renvoyé une erreur."
