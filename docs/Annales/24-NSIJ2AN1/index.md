---
title: Sujet Amérique du Nord, Session 2024
keywords: baccalauréat, 2024, 24-NSIJ2AN1
---

Le sujet scanné est disponible [ici](./24-NSIJ2AN1.pdf).

# BACCALAURÉAT GÉNÉRAL

**ÉPREUVE D’ENSEIGNEMENT DE SPÉCIALITÉ - SESSION 2024 - NUMÉRIQUE ET SCIENCES INFORMATIQUES**

**JOUR 2**

Durée de l’épreuve : **3 heures 30**

*L’usage de la calculatrice n’est pas autorisé.*

**Le sujet est composé de trois exercices indépendants.**

**Le candidat traite les trois exercices.**

## Exercice 1 (6 points)

Cet exercice porte sur les algorithmes de tri et la programmation en Python.
On se propose dans cet exercice de se pencher sur un algorithme pour trier un tableau appelé le tri de Stooge.
Pour trier les éléments situés entre les indices $i$ et $j$, où $i < j$, dans un tableau t par ce tri, on procède ainsi :

* si les éléments d’indice $i$ et $j$ sont mal placés, on les échange ;
* si il y a au moins trois éléments entre les indices $i$ et $j$ :
  * on trie les deux premiers tiers du tableau avec cette méthode ;
* on trie les deux derniers tiers du tableau avec cette méthode ;
  * on trie à nouveau les deux premiers tiers du tableau avec cette méthode.

Pour réaliser ce découpage en tiers, on considère l’entier $k$ défini par l’expression `(j – i + 1) // 3`, et on considère les indices intermédiaires $i+k$ et $j-k$.

<figure>
	<img src="schema-stooge.svg" alt="Le tableau est divisé en deux parties selon les indices (i,j-k) pour les deux premiers tiers et (i+k,j) pour les deux derniers tiers" width="100%">
	<figcaption>Le schéma de division du tri de Stooge</figcaption>
</figure>

Voici le code partiel de l’algorithme du tri de Stooge en Python qui trie donc les éléments d’un tableau par ordre croissant.

```python linenums="1"
def triStooge(tab, i, j):
	if tab[i] > tab[j]:
		echange(tab, i, j)
	if (j - i) > 1:
		k = (j - i + 1)//3
		triStooge(...)
		triStooge(...)
		triStooge(...)
```

??? question "Écrire la fonction `echange(tab, i, j)` qui prend en arguments une liste Python `tab` et deux indices `i, j`, et réalise sur place l’échange des valeurs dans `tab` à ces indices. La fonction ne renvoie rien."
	```python
	def echange(tab, i, j):
		temp = tab[i]
		tab[i] = tab[j]
		tab[j] = temp
	```

??? question "Finaliser le programme précédent en complétant les lignes 6, 7 et 8 sur votre copie."
	```python linenums="1" hl_lines="6-8"
	def triStooge(tab, i, j):
		if tab[i] > tab[j]:
			echange(tab, i, j)
		if (j - i) > 1:
			k = (j - i + 1)//3
			triStooge(tab,i,j-k)
			triStooge(tab,i+k,j)
			triStooge(tab,i,j-k)
	```
??? question "Indiquer en le justifiant si cet algorithme est itératif ou récursif."
	La fonction `triStooge` fait appel à elle même aux lignes 6 à 8, elle est donc récursive.

Soit l’appel `triStooge(A, 0, 5)` avec `A = [5, 6, 4, 2, 3, 1]`.

??? question "Déterminer la valeur numérique prise par $k$ lors de ce premier appel. Une justification est attendue."
	Au premier appel, $j-i = 5 - 0 = 5 \geq 3$, donc $k$ est calculé comme: $\frac{(j-i+1)}{3} = \frac{5+1}{3} = 2$.
	Donc $k = 2$.

La figure ci-dessous présente l’arbre (incomplet) des appels récursifs effectués depuis l’appel `triStooge(A, 0, 5)`.

<figure>
	<img src="appel-stooge.png" alt="L'arbre d'appel du tri de stooge avec trois zones à compléter." width="100%">
	<figcaption>Figure 1. Arbre des appels récursifs pour `triStooge(A, 0, 5)`</figcaption>
</figure>

??? question "Dénombrer le nombre d’appels récursifs effectués lors du tri sans compter l’appel initial."

??? question "Déterminer les appels effectués dans les cases 1, 2 et 3 de cet arbre des appels récursifs."

Dans cette question, `A = [5, 6, 4, 2]`.

!!! question "Recopier le tableau ci-dessous et le compléter (remplacer les ??)."

| Appel              | Valeur de A avant l’appel | Valeur de A après l’appel |
| ---                | ---                       | --                        |
| `triStooge(A,0,3)` | `[5, 6, 4, 2]`            | ??                        |
| ??                 | `[2, 6, 4, 5]`            | ??                        |
| `triStooge(A,1,3)` | `[2, 4, 6, 5]`            | ??                        |
| ??                 | `[2, 4, 5, 6]`            | `[2, 4, 5, 6]`            |

??? success "Réponse"
	| Appel              | Valeur de A avant l’appel | Valeur de A après l’appel |
	|--------------------|---------------------------|---------------------------|
	| `triStooge(A,0,3)` | `[5, 6, 4, 2]`            | `[2, 4, 5, 6]`            |
	| `triStooge(A,0,2)` | `[2, 6, 4, 5]`            | `[2, 4, 6, 5]`                        |
	| `triStooge(A,1,3)` | `[2, 4, 6, 5]`            | `[2, 4, 5, 6]`                        |
	| `triStooge(A,0,2)` | `[2, 4, 5, 6]`            | `[2, 4, 5, 6]`            |

On montre que le coût en temps dans le pire des cas de l’algorithme de Stooge est de l’ordre de $n^e$ , avec $e$ environ égal à $\frac{8}{3}$.

??? question "Donner un algorithme de tri dont le coût est strictement meilleur."
	Tout les algorithmes de tris étudiés en Première sont meilleurs car $2 \leq \frac{8}{3}$.
	Les algorithmes du tri par **sélection** et **insertion** sont donc meilleurs.
	De même, l'algorithme du tri **fusion** ou du tri **rapide** le sont aussi par transitivité.

## Exercice 2 (6 points)

Cet exercice porte sur le langage SQL et les bases de données.

Un pharmacien nouvellement installé décide de créer son propre système de gestion des médicaments qu’il délivre à ses clients.
Pour sa base de données relationnelle, il a déjà élaboré la première relation à l’aide des données indiquées sur les cartes vitales de ses deux premiers clients : `client (id_client : INT, nom_client : VARCHAR(30),prenom_client : VARCHAR(30), num_secu_sociale : VARCHAR(15))`

<table>
	<thead><tr><th>client</th></tr></thead>
	<tbody>
	<tr>
		<th>id_client</th>
		<th>nom_client</th>
		<th>prenom_client</th>
		<th>num_secu_social</th>
	</tr>
	<tr>
		<td>1</td>
		<td>Martin</td>
		<td>Sophie</td>
		<td>202103812326129</td>
	</tr>
	<tr>
	<td>2</td>
		<td>	Dufour</td>
		<td>Marc</td>
		<td>10507381700995</td></tr>
	</tbody>
</table>

!!! question "Écrire le résultat de l’exécution de la requête SQL suivante :"
	```sql
	SELECT nom_client, prenom_client
	FROM client
	ORDER BY nom_client;
	```

??? success "Réponse"
	Cette requête permet de récupérer le nom et le prénom de tout les clients classés selon leur nom.
	On obtient donc la table:
	
	| nom_client | prenom_client |
	|------------|---------------|
	| Dufour     | Marc          |
	| Martin     | Sophie        |

Pour écrire la relation `medicament`, il doit utiliser les informations fournies par la notice des médicaments.
En voici une ci-dessous :

<figure>
	```
	Paracétamol 1 Gramme CP
	
	Qu'est-ce que Paracétamol 1 gramme CP et dans quel cas est-il utilisé ?
	Paracétamol 1 gramme CP est un antalgique (calme la douleur)
	Que contient un comprimé de Paracétamol 1 gramme CP ?
	La substance active est le paracétamol: 1 gramme pour un comprimé.
	Sous quelle forme se présente Paracétamol 1 gramme CP ?
	Ce médicament se présente sous la forme de comprimé. Chaque boite contient 8 comprimés.
	```
	
	<figcaption>Figure 1. Informations extraites de la notice du médicament Paracétamol 1 gramme CP.</figcaption>
</figure>


La relation medicament suivante a été obtenue à l’aide de ces notices : 
`medicament (id_medic : INT, nom_medic : VARCHAR(30),categorie : VARCHAR(20), conditionnement : INT,quantite : INT, prix : FLOAT)`

La table des médicaments de son officine est présentée ci-dessous.

<table>
	<thead><tr><th>		medicament</th></tr></thead>
	<tbody>
	<tr>
		<th>id_medic</th>
		<th>nom_medic</th>
		<th>categorie</th>
		<th>conditionnement</th>
		<th>quantite</th>
		<th>prix</th>
	</tr>
	<tr><td>1</td>
		<td>Paracétamol 1 gramme CP</td>
		<td>antalgique</td>
		<td>8</td>
		<td>50</td>
		<td>3,50</td>
	</tr>
	<tr><td>2</td>
		<td>Acide acétylsalicylique</td>
		<td>antalgique</td>
		<td>8</td>
		<td>20</td>
		<td>2,30</td>
	</tr>
	<tr><td>3</td>
		<td>		Gel hydroalcoolique 100 ml</td>
		<td>désinfectant</td>
		<td>1</td>
		<td>300</td>
		<td>2,30</td>
	</tr>
	<tr><td>4</td>
		<td>Acide ascorbique</td>
		<td>vitamine</td>
		<td>10</td>
		<td>450</td>
		<td>5,50</td>
	</tr>
	</tbody>
</table>

??? question "Écrire une requête SQL permettant d’afficher les noms de tous les médicaments dont le prix est strictement inférieur à 3 euros."
	```sql
	SELECT nom_medic
	FROM medicament
	WHERE prix < 3
	```
	
Madame Martin présente au pharmacien une nouvelle ordonnance :

<figure>
	<img src="ordonnance.png"w/>
	<figcaption>Figure 2. Ordonnance de Madame Sophie Martin.</figcaption>
</figure>

Il saisit les informations de cette ordonnance dans la relation `ordonnance`, chaque médicament prescrit correspondant à un enregistrement dans la table ci-dessous.


<table>
	<thead>
	<tr><th>ordonnance	</th></tr>
	</thead>
	<tbody>
	<tr>
	<th>id_ordo</th>
	<th>id_client</th>
	<th>date_ordo</th>
	<th>id_medic</th>
	<th>nb_boites</th>
	</tr>
	<tr>
		<td>	6</td>
		<td>	2</td>
		<td>	2023-11-29</td>
		<td>	2</td>
		<td>	2</td>
	</tr>
	<tr><td>	7</td>
	<td>		1</td>
		<td>	2023-12-13</td>
		<td>	1</td>
		<td>	...</td>
	</tr>
	<tr><td>		8</td>
	<td>		1</td>
	<td>		2023-12-13</td>
	<td> 4 </td>
		<td> ...</td>
	</tr>
	</tbody>
</table>

!!! question "Écrire une requête SQL permettant d’ajouter les informations de la carte vitale de sa troisième cliente présentée ci-dessous :"
	<figure>
		<figcaption>Figure 3. Image de la carte vitale extraite de la page Wikipédia<br/>Source : d’après https://fr.wikipedia.org/wiki/Carte_Vitale (wikipedia.org)</figcaption>
	</figure>
	
??? success "Réponse"
	```sql
	INSERT INTO client VALUES(3,"Durand","Nathalie","2690549588815780")
	```
			
??? question "Donner les attributs qui doivent être déclarés comme clés étrangères de la relation `ordonnance` et en préciser l’utilité."
	Dans la table `ordonnance` les clés étrangères sont:
	
	* `id_client` qui référence la clé primaire de `client` et indique donc le client à qui elle est adressée;
	* `id_medic` qui référence la clé primaire de `médicament` et indique donc un médicament sur l'ordonnance.
   
??? question "Indiquer, pour les lignes 7 et 8 de la table `ordonnance`, le nombre de boites prescrites."
	Pour le Paracétamol elle doit prendre au maximum trois comprimés par jour pendant deux jours, soit six comprimés.
	Les boites en contiennent huit donc une boite suffit. (ligne 7)
	
	Pour l'acide ascorbique, elle doit prendre un comprimé par jour pendant quatres semaines, soit sept comprimés par semaine soit 28 comprimés.
	Une boite en contient 10, il faut donc trois boites avec un reste de deux comprimés. (ligne 8)
	
??? question "Écrire la requête SQL mettant à jour la quantité du médicament Acide ascorbique en stock dans l’officine du pharmacien suite au passage de Madame Martin."
	```sql
	UPDATE medicament SET quantite = quantite - 3
	WHERE id_medic = 4
	```
??? question "Calculer le coût total des médicaments fournis à Madame Martin (on ne demande pas d’écrire une requête ici, mais de calculer le coût total en justifiant le calcul)."
	Madame Martin doit acheter trois boites d'acide ascorbique à 5,50 l'unité et une boite de Paracétamol à 3,50 l'unité.
	Elle doit donc payer $5,5 \times 3 + 3,5 = 16,5 + 3,5 = 20$.
	
??? question "Écrire la requête SQL permettant d’afficher le nom du médicament pour l’ordonnance ayant l’`id_ordo` numéro 6."
	```sql
	SELECT nom_medic
	FROM ordonnance NATURAL JOIN medicament
	WHERE id_ordo = 6
	```
	On peut utiliser un `NATURAL JOIN` car la colonne est `id_medic` dans les deux tables.

## Exercice 3 (8 points)

Cet exercice porte sur la programmation objet, les structures de données, les réseaux et l’architecture matérielle.

On considère un réseau local constitué des trois machines de Alice, Bob et Charlie dont les adresses IP sont les suivantes :

* la machine d’Alice a pour adresse 192.168.1.1 ;
* la machine de Bob a pour adresse 192.168.1.2.

On rappelle que l’adresse 192.168.1.255 est l’adresse de diffusion qui sert à communiquer avec toutes les machines du réseau local et le masque de ce réseau local est 255.255.255.0.
Cette adresse de diffusion est réservée et ne peut être attribuée à une machine.

### Partie A


??? question "Donner une adresse IP possible pour la machine de Charlie afin qu’elle puisse communiquer avec celles d’Alice et Bob dans le réseau local. Justifier votre réponse en donnant toutes les conditions à respecter dans le choix de cette adresse IP"
	On doit choisir une IP dans le même réseau que celle d'Alice et Bob.
	Pour cela on calcule la partie réseau grâce au masque : 192.168.1.1 `ET` 255.255.255.0 donne **192.168.1.0**.
	L'adresse doit donc être de la forme 192.168.1.X avec $0 < X < 255$ et les trois valeurs $1,2,255$ sont déjà attribuées.
	Une IP possible est donc $192.168.1.42$.

Ce réseau est utilisé pour effectuer des transactions financières en monnaie numérique nsicoin entre les trois utilisateurs.
Pour cela, on crée la classe `Transaction` ci-dessous :

```python linenums="1"
class Transaction:
	def __init__(self, expediteur, destinataire, montant):
		self.expediteur = expediteur
		self.destinataire = destinataire
		self.montant = montant
```

Toutes les dix minutes, les transactions réalisées pendant cet intervalle de temps sont regroupées par ordre d’apparition dans une liste Python.
Dans un intervalle de dix minutes, Alice envoie dix nsicoin à Charlie puis Bob envoie cinq nsicoin à Alice.

??? question "Écrire la liste Python correspondante à ces transactions."
	```python
	liste_transactions = [
		Transaction("Alice","Charlie",10),
		Transaction("Bob","Alice",5)
	]
	```

Pour garder une trace de toutes les transactions effectuées, on utilise une liste chaînée de blocs (ou blockchain) dont le code Python est fourni ci-dessous.
Toutes les dix minutes un nouveau bloc contenant les nouvelles transactions est créé et ajouté à la blockchain.

```python linenums="1"
class Bloc:
	def __init__(self, liste_transactions, bloc_precedent):
		self.liste_transactions = liste_transactions
		self.bloc_precedent = bloc_precedent # de type Bloc

class Blockchain:
	def __init__(self):
		self.tete = self.creer_bloc_0()

	def self.creer_bloc_0(self):
		"""
		Crée le premier bloc qui distribue 100 nsicoin à tous les utilisateurs
		(un pseudo-utilisateur Genesis est utilisé comme expéditeur)
		"""
		liste_transactions = [
			Transaction("Genesis", "Alice", 100),
			Transaction("Genesis", "Bob", 100),
			Transaction("Genesis", "Charlie", 100)
		]
		return Bloc(liste_transactions, None)
```

La figure 1 représente les trois premiers blocs d’une Blockchain.

!!! question "Expliquer pourquoi la valeur de l’attribut `bloc_precedent` du `bloc0` est `None`."
	Le bloc 0 est la tête de la liste des transactions, il n'a donc aucun prédécesseur.
	Comme le prédécesseur doit être typable comme un objet, on utilise `None` au lieu de `0`.
	
<figure>
	<img src="transactions.png" alt="la liste des transactions ci-dessous représentées comme une liste chaînée">
	```python
	# tête pointe vers bloc0
	
	# Pour bloc2 on a:
	liste_transactions = [
		Transaction('Bob', 'Charlie', 20),
		Transaction('Bob', 'Charlie', 20),
		Transaction('Charlie', 'Alice', 30)
	]
	
	# bloc2 pointe vers bloc1
	# Pour bloc1 on a:
	
	liste_transactions = [
		Transaction('Alice', 'Charlie', 50),
		Transaction('Charlie', 'Bob', 30)
	]
	
	# bloc1 pointe vers bloc0 
	# Pour bloc0 on a:
	liste_transactions = [
		Transaction('Genesis', 'Alice', 100),
		Transaction('Genesis', 'Bob', 100),
		Transaction('Genesis', 'Charlie', 100)
	]
	```
	<figcaption>Figure 1. Blockchain</figcaption>
</figure>

??? question "Donner la valeur de l’attribut `bloc_precedent` du `bloc1` afin que celui-ci soit lié au `bloc0`."
	Si le bloc0 est contenu dans la variable `bloc0`, alors il faut que `bloc1.bloc_precedent` soit égal à `bloc0`.
	Si la blockchain vient d'être créée dans la variable `bc` et qu'elle ne contient que le bloc 0, alors on peut utiliser `bc.tete`.
	
??? question "À l’aide des classes `Bloc` et `Blockchain`, écrire le code Python permettant de créer un objet `ma_blockchain` de type `Blockchain` représenté par la figure 1."
	```python
	bc = Blockchain()
	bloc0 = bc.tete
	
	liste_transactions = [
		Transaction('Alice', 'Charlie', 50),
		Transaction('Charlie', 'Bob', 30)
	]

	bloc1 = Bloc(liste_transactions,bloc0)
	
	liste_transactions = [
		Transaction('Bob', 'Charlie', 20),
		Transaction('Bob', 'Charlie', 20),
		Transaction('Charlie', 'Alice', 30)
	]
	
	bloc2 = Bloc(liste_transactions,bloc1)
	
	bc.tete = bloc2
	```
	
??? question "Donner le solde en nsicoin de Bob à l’issue du `bloc2`."
	À l'issue du `bloc2` Bob a reçu et envoyé les transferts suivants: $+100,+30,-20,-20$ il a donc un solde de 90 (addition de tout les transferts).

On souhaite doter la classe Blockchain d’une méthode `ajouter_bloc` qui prend en paramètre la liste des transactions des dix dernières minutes et l’ajoute dans un nouveau bloc.

!!! question "Écrire le code Python de cette méthode ci-dessous."
	```python linenums="1"
	def ajouter_bloc(self, liste_transactions):
		# A compléter
	```
		
??? success "Réponse"
	```python
	def ajouter_bloc(self,liste_transactions):
		nouveau_bloc = Bloc(liste_transactions,self.tete) # On crée un bloc dont le précédent est la tête.
		self.tete = nouveau_bloc #la nouvelle tête est le nouveau bloc.
	```
	
Lorsqu’un utilisateur ajoute un nouveau bloc à la Blockchain, il l’envoie aux autres membres.
Ainsi chaque utilisateur dispose sur sa propre machine d’une copie identique de la Blockchain.

??? question "Donner le nom et la valeur de l’adresse IP à utiliser pour effectuer cet envoi."
	On peut utiliser l'adresse de diffusion (192.168.1.255) qui permet d'envoyer un message à toutes les machines du réseau d'après l'énoncé.

On souhaite doter la classe Bloc d’une nouvelle méthode `calculer_solde` permettant de renvoyer le solde à l’issue de ce bloc.

!!! question "Recopier et compléter sur votre copie le code Python de cette méthode :"
	```python linenums="1"
	def calculer_solde(self, utilisateur):
		if self.precedent is None: # cas de base
			solde = 0
		else:
			solde = ... # appel récursif : calcul du solde au bloc précédent
		for transaction in bloc.liste_transactions
			if ... == utilisateur:
				solde = solde - ....
			elif ... :
				...
	    return solde
	```

??? success "Réponse"
	```python linenums="1"
	def calculer_solde(self, utilisateur):
		if self.precedent is None: # cas de base
			solde = 0
		else:
			solde = self.precedent.calculer_solde(utilisateur) 
			# appel récursif : calcul du solde au bloc précédent
			
		for transaction in bloc.liste_transactions
			if transaction.expediteur == utilisateur:
				solde = solde - transaction.montant
			elif transaction.destinataire == utilisateur :
				solde = solde + transaction.montant
	    return solde
	```


??? question "Écrire l’appel à la fonction `calculer_solde` permettant de calculer le solde actuel de Alice"
	On conserve la convention que la blockchain est dans la variable `bc`.
	```python
	bc.tete.calculer_solde("Alice")
	```

### Partie B

Dans cette partie, on va améliorer la sécurité de la blockchain.
Pour cela on enrichit la classe `Bloc` comme indiqué ci-dessous :

```python linenums="1"
class Bloc:
	def __init__(self, liste_transactions, bloc_precedent):
		self.liste_transactions = liste_transactions
		self.bloc_precedent = bloc_precedent
		# Définition de trois nouveaux attributs
		self.hash_bloc_precedent = self.donner_hash_precedent()
		self.nonce = 0 # Fixé arbitrairement et temporairement à 0 avant le minage du bloc
		self.hash = self.calculer_hash()
	
	# Définition de trois nouvelles méthodes
	def donner_hash_precedent(self):
		if self.bloc_precedent is not None:
			return self.bloc_precedent.hash
		else :
			return "0"
	
	def calculer_hash(self):
	"""calcule et renvoie le hash du bloc courant"""
		# Le code python n'est pas étudié dans cet exercice
		
	def minage_bloc(self):
	"""modifie le nonce d'un bloc pour que son hash commence par '00'"""
		# A compléter
```

La fonction `calculer_hash` produit une chaîne de caractères appelée hash qui possède les propriétés suivantes :

* Le hash d’un bloc dépend de toutes les données contenues dans le bloc et uniquement de ces données ;
* Le calcul du hash d’un bloc est rapide et facile à calculer par une machine ;
* La moindre modification dans le bloc produit un hash complètement différent ;
* Il est impossible de déduire le bloc à partir de son hash.
* Si deux blocs ont le même hash, c’est qu’ils sont parfaitement identiques.

L’attribut `nonce` est de type entier.
Miner un bloc signifie trouver une valeur de nonce de telle façon que l’attribut hash du bloc commence par les deux caractères “00”.
Compte tenu des propriétés précédentes, la seule façon de trouver cette valeur est de procéder à une recherche exhaustive.

??? question "Expliquer en quoi consiste le fait de trouver une valeur par recherche exhaustive."
	Trouver une valeur par recherche exhaustive consiste à générer et tester toutes les valeurs possibles jusqu'à trouver celle qui correspond.
	Ici cela signifie générer tout les nonces possibles jusqu'à ce que le hash du bloc commence par "00".

Dans la suite de l’exercice, on considère que tous les utilisateurs cherchent à miner le nouveau bloc.
Le premier qui réussit, l’ajoute à la blockchain et gagne une récompense en nsicoin.

??? question "En justifiant votre réponse, donner la valeur de l’attribut `hash_bloc_precedent` du `bloc0`."
	Le `bloc0` n'a aucun bloc précédent (voir partie A), son attribut `bloc_precedent` est donc à `None`.
	Donc d'après la définition de la fonction, l'attribut `hash_bloc_precedent` sera à "0".

??? question "Sachant que le hash est écrit sur 256 bits, donner le calcul permettant d’obtenir le nombre de hash possibles."
    Le hash étant écrit sur 256 bits, si on suppose qu'il n'a aucune structure particulière alors le nombre de hashs possibles est : $2^256$ car chaque bit peut avoir les valeurs 0 ou 1 uniquement.
	Cela ne serait pas le cas si le hash était une chaîne de caractères ASCII par exemple car chaque octet n'aurait alors que 128 valeurs différentes.

!!! question "Recopier et compléter sur votre copie le code python de la méthode `minage_bloc`."
	```python linenums="1"
	def minage_bloc(self):
	"""modifie le nonce d'un bloc pour que son hash commence par '00' en énumérant tous les entiers naturels en partant de 0."""
		self.nonce = 0
		self.hash = self.calculer_hash()
		while ... :
			self.nonce = ...
			self.hash = ...
	```

??? success "Réponse"
	```python linenums="1"
	def minage_bloc(self):
	"""modifie le nonce d'un bloc pour que son hash commence par '00' en énumérant tous les entiers naturels en partant de 0."""
		self.nonce = 0
		self.hash = self.calculer_hash()
		while self.hash[0] != "0" and self.hash[1] != "0" :
			self.nonce = self.nonce + 1
			self.hash = self.calculer_hash()
	```
