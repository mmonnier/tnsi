# BACCALAURÉAT GÉNÉRAL

Le sujet est disponible [ici](./24-NSIJ2JA1.pdf)

**ÉPREUVE D’ENSEIGNEMENT DE SPÉCIALITÉ -SESSION 2024 -NUMÉRIQUE ET SCIENCES INFORMATIQUES**

**JOUR 2**

Durée de l’épreuve : *3 heures 30*

L’usage de la calculatrice n’est pas autorisé.

Le sujet est composé de trois exercices indépendants.

Le candidat traite les trois exercices.


## EXERCICE 1 (6 points)

*Cet exercice porte sur la programmation Python (listes, dictionnaires) et la méthode “diviser pour régner”.*

*Cet exercice est composé de trois parties indépendantes.*

Dans cet exercice, on s’intéresse à des algorithmes pour déterminer, s’il existe, l’élément absolument majoritaire d’une liste.

On dit qu’un élément est absolument majoritaire s’il apparaît dans strictement plus de la moitié des emplacements de la liste.

Par exemple, la liste `[1, 4, 1, 6, 1, 7, 2, 1, 1]` admet 1 comme élément absolument majoritaire, car il apparaît cinq fois sur neuf éléments.
Par ailleurs, la liste `[1, 4, 6, 1, 7, 2, 1, 1]` n’admet pas d’élément absolument majoritaire, car celui qui est le plus fréquent est 1 , mais il n’apparaît que quatre fois sur huit, ce qui ne fait pas plus que la moitié.

??? question "Déterminer les effectifs possibles d’un élément absolument majoritaire dans une liste de taille 10."
	Un élément strictement majoritaire *apparait dans strictement plus de la moitié des emplacements*.
	Donc dans une liste de taille 10, un élément *absolument majoritaire* doit apparaître dans strictement plus de $\frac{10}{2}=5$ emplacements.
	Donc les effectifs possibles sont: 6,7,8,9 et 10.
	
### Partie A : Calcul des effectifs de chaque élément sans dictionnaire

On peut déterminer l’éventuel élément absolument majoritaire d’une liste en calculant l’effectif de chacun de ses éléments.

??? question "Écrire une fonction `effectif` qui prend en paramètres une valeur `val` et une liste `lst` et qui renvoie le nombre d’apparitions de `val` dans `lst`. Il ne faut pas utiliser la méthode `count`."
	```python
	def effectif(val,lst):
		'''Renvoie le nombre d'apparitions de la valeur val dans la liste lst.'''
		eff = 0
		for element in lst:
			if element == val:
				eff += 1
	    return eff
	```
	Ou bien en version *fonctionnelle*:
	```python
	def effectif(val,lst):
		return sum(1 for element in lst if element == val)
	```

??? question "Déterminer le nombre de comparaisons effectuées par l’appel `effectif(1,[1, 4, 1, 6, 1, 7, 2, 1, 1])`."
    Dans la fonction précédente, on effectue un parcours de toutes les valeurs de la liste.
	On compare donc `val` à chaque élément de `lst`.
	Ici `lst` est composée de neuf éléments, on effectue donc neuf comparaisons.
	En général, `effectif` effectue $n$ comparaisons si `lst` est de longueur $n$.
	
??? question "En utilisant la fonction `effectif` précédente, écrire une fonction `majo_abs1` qui prend en paramètre une liste `lst`, et qui renvoie son élément absolument majoritaire s’il existe et renvoie `None` sinon."
	```python
	def majo_abs1(lst):
		'''Renvoie l'élément absolument majoriaitre de lst si il existe, None sinon.'''
		for element in lst:
			if effectif(element) > len(lst)/2:
				return element
	    return None # Non obligatoire car une fonction python renvoie None par défaut.
	```

??? question "Déterminer le nombre de comparaisons effectuées par l’appel à `majo_abs1([1, 4, 1, 6, 1, 7, 2, 1, 1])`."
	Notre liste a une taille de $n=9$.
	On commence par calculer l'effectif de la valeur $1$ en $n$ comparaisons, on obtient $5$.
	On effectue une comparaison de plus pour vérifier que $5 > 9/2$, c'est le cas donc on renvoie 5.
	On effectue donc au total dix comparaisons pour cette liste.
	
### Partie B : Calcul des effectifs de chaque élément dans un dictionnaire

Un autre algorithme consiste à déterminer l’élément absolument majoritaire éventuel d’une liste en calculant l’effectif de tous ses éléments en stockant l’effectif partiel de chaque élément déjà rencontré dans un dictionnaire.

!!! question "Recopier et compléter les lignes 3, 4, 5 et 7 de la fonction `eff_dico` suivante qui prend en paramètre une liste `lst` et qui renvoie un dictionnaire dont les clés sont les éléments de `lst` et les valeurs les effectifs de chacun de ces éléments dans `lst`."
	```python linenums="1"
	def eff_dico(lst):
		dico_sortie = {}
		for ......... :
			if ... in dico_sortie:
				...
			else:
				...
	    return dico_sortie
	```
	
??? success "Réponse"
	```python linenums="1" hl_lines="3 4 5 7"
	def eff_dico(lst):
		dico_sortie = {}
		for element in lst :
			if element in dico_sortie:
				dico_sortie[element] += 1
			else:
				dico_sortie[element] = 0
	    return dico_sortie
	```
??? question "En utilisant la fonction `eff_dico` précédente, écrire une fonction `majo_abs2` qui prend en paramètre une liste `lst`, et qui renvoie son élément absolument majoritaire s’il existe et renvoie `None` sinon."
	```python
	def majo_abs2(lst):
		effectif = eff_dico(lst)
		for element in effectif:
			if effectif[element] > len(lst)/2:
				return element
	    return None
	```

### Partie C : par la méthode “diviser pour régner”

Un dernier algorithme consiste à partager la liste en deux listes.
Ensuite, il s’agit de déterminer les éventuels éléments absolument majoritaires de chacune des deux listes.
Il suffit ensuite de combiner les résultats sur les deux listes afin d’obtenir, s’il existe, l’élément majoritaire de la liste initiale.

Les questions suivantes vont permettre de concevoir précisément l’algorithme.

On considère `lst` une liste de taille $n$.

??? question "Déterminer l’élément absolument majoritaire de `lst` si $n = 1$. C’est le cas de base."
	Si $n=1$ la liste ne contient qu'un seul élément, c'est donc l'élément majoritaire.
	Il est situé en `lst[0]`.

On suppose que l’on a partagé `lst` en deux listes :

- `lst_g = lst[:n//2]` (`lst_g` contient les $\frac{n}{2}$ premiers éléments de `lst`)
- `lst_d = lst[n//2:]` (`lst_d` contient les autres éléments de `lst`)

??? question "Si, ni `lst_g` ni `lst_d` n’admet d’élément absolument majoritaire, expliquer pourquoi `lst` n’admet pas d’élément absolument majoritaire."
	`lst_g` et `lst_d` sont les deux moitiés de la liste.
	Si aucun élément n'est absolument majoritaire dans `lst_g` cela veut dire que tout élément a un effectif plus petit ou égal à `n//4` dans `lst_g`, de même dans `lst_d`.
	Au total un élément quelconque aura donc un effectif plus petit ou égal à `n//4+n//4 = n//2` dans `lst`, ce qui n'est pas **strictement supérieur** à `n//2`.
	Donc `lst` n'admet pas d'élément absolument majoritaire.

??? question "Si `lst_g` admet un élément absolument majoritaire `maj_g`, donner un algorithme pour vérifier si `maj_g` est l’élément absolument majoritaire de `lst`."
	Si `maj_g` est absolument majoritaire dans `lst_g` alors il suffit de calculer son effectif dans la liste entière pour vérifier qu'il l'est dans `lst`.
	L'algorithme consiste donc à calculer l'effectif de `maj_g` dans `lst` et vérifier s'il est supérieur à $n/2$.
	
!!! question "Recopier et compléter les lignes 4, 11, 13, 15 et 17 pour la fonction récursive `majo_abs3` qui implémente l’algorithme précédent. Vous pourrez utiliser la fonction `effectif` de la question 2."
	```python linenums="1"
	def majo_abs3(lst):
		n = len(lst)
		if n == 1:
			return ...
		else:
			lst_g = lst[:n//2]
			lst_d = lst[n//2:]
			maj_g = majo_abs3(lst_g)
			maj_d = majo_abs3(lst_d)
			if maj_g is not None:
				eff = ......
				if eff > n/2:
					return ...
	        if maj_d is not None:
				eff = ......
				if eff > n/2:
					return ...
	```

??? success "Réponse"
	```python linenums="1" hl_lines="4 11 13 15 17"
	def majo_abs3(lst):
		n = len(lst)
		if n == 1:
			return lst[0]
		else:
			lst_g = lst[:n//2]
			lst_d = lst[n//2:]
			maj_g = majo_abs3(lst_g)
			maj_d = majo_abs3(lst_d)
			if maj_g is not None:
				eff = effectif(maj_g,lst)
				if eff > n/2:
					return maj_g
	        if maj_d is not None:
				eff = effectif(maj_d,lst)
				if eff > n/2:
					return maj_d
	```


## EXERCICE 2 (6 points)

*Cet exercice porte sur la programmation, la programmation orientée objet et les structures de données linéaires.*

*Cet exercice est composé de 2 parties indépendantes.*

Dans cet exercice, on appelle parenthèses les couples de caractères `()`, `{}` et `[]`.
Pour chaque couple de parenthèses, la première parenthèse est appelée la parenthèse ouvrante du couple et la seconde est appelée la parenthèse fermante du couple.

On dit qu’une expression (chaine de caractères) est bien parenthésée si

- chaque parenthèse ouvrante correspond à une parenthèse fermante de même type ;
- les expressions comprises entre parenthèses sont des expressions bien parenthésées.

Par exemple, l’expression `'tab[2*(i + 4)] - tab[3]'` est bien parenthésée.

En revanche, l’expression `'tab[2*(i + 4] - tab[3)'` n’est pas bien parenthésée, car la première parenthèse fermante `]` devrait correspondre à la dernière parenthèse ouvrante `(`.

??? question "Déterminer si l’expression `'[2*(i+1)-3) for i in range(3, 10)]'` est bien parenthésée. Justifier votre réponse"
	L'expression n'est pas bien parenthésée car la parenthèse fermante après `-3)` n'est pas associée à une parenthèse ouvrante.

### Partie A

On peut observer que, si les parenthèses vont par couple, une expression bien parenthésée contient autant de parenthèses ouvrantes que de parenthèses fermantes.

On se propose d’écrire une fonction qui vérifie si une chaine de caractères est bien parenthésée.

??? question "Écrire une fonction `compte_ouvrante` qui prend en paramètre une chaine de caractères `txt` et qui renvoie le nombre de parenthèses ouvrantes qu’elle contient."
	```python
	def compte_ouvrante(txt):
		compte = 0
		for symbole in txt:
			if symbole in '([{':
				compte += 1
	    return compte
	```
	
	Version en une ligne mais utilisant la fonction intégrée `sum`:

	```python
	def compte_ouvrante(txt):
		return sum(1 for symbole in txt if symbole in '([{')
	```
??? question "Écrire une fonction `compte_fermante` qui prend en paramètre une chaine de caractères `txt` et qui renvoie le nombre de parenthèses fermantes qu’elle contient."
	Il s'agit exactement du même algorithme, on remplace juste les symboles recherchés par `)]}`
	```python
	def compte_ouvrante(txt):
		compte = 0
		for symbole in txt:
			if symbole in ')]}':
				compte += 1
	    return compte
	```
	
	Version en une ligne mais utilisant la fonction intégrée `sum`:

	```python
	def compte_ouvrante(txt):
		return sum(1 for symbole in txt if symbole in ')]}')
	```
	
??? question "En utilisant les deux fonctions précédentes, écrire une fonction `bon_compte` qui prend en paramètre une chaine de caractères `txt` et qui renvoie `True` si `txt` a autant de parenthèses ouvrantes que parenthèses fermantes et `False` sinon."
	```python
	def bon_compte(txt):
		return compte_ouvrante(txt) == compte_fermante(txt)
	```
??? question "Donner un exemple de chaine de caractères pour laquelle `bon_compte` renvoie True alors qu’elle n’est pas bien parenthésée."
	La chaine `)(` contient bien une parenthèse ouvrante et une parenthèse fermante, mais celles-ci sont inversées.

### Partie B

Comme l’algorithme précédent n’est pas suffisant, on se propose d’implémenter un algorithme utilisant une structure linéaire de pile.

On se propose d’écrire une classe `Pile` qui implémente la structure de pile.

```python linenums="1"
class Pile:
	def __init__(self):
		self.contenu = []

	def est_vide(self):
		return len( self.contenu) == 0

	def empiler(self, elt):
		...
	
	def depiler(self):
		if self.est_vide():
			return "La pile est vide."
		return ...
```

??? question "Compléter les lignes 9 et 14 du code précédent pour que la méthode `empiler` permette d’empiler un élément `elt` dans une pile et que la méthode `depiler` permette de dépiler une pile en renvoyant l’élément dépilé. Vous n’écrirez que le code des deux méthodes."
	```python linenums="8"
	def empiler(self, elt):
		self.contenu.append(elt)
		
	def depiler(self,elt):
		if self.est_vide():
			return "La pile est vide."
		return self.contenu.pop(-1) # On enlève le dernier élément ajouté
		#return self.contenu.pop() #Équivalent car pop(-1) est le comportement par défaut.
	```
	
Un algorithme permettant de vérifier si une expression est bien parenthésée consiste à

* créer une pile vide `p` ;
* parcourir l’expression en testant chaque caractère :
	- si c’est une parenthèse ouvrante, on l’empile dans `p` ;
	- si c’est une parenthèse fermante, on dépile `p` ;
		* si les deux caractères correspondent à un couple de parenthèses, on continue le parcours,
		* sinon l’expression n’est pas bien parenthésée ;
	- sinon on continue le parcours.

Si l’expression a été entièrement parcourue, on teste la pile ; l’expression est bien parenthésée si la pile est vide.

??? question "Déterminer le nombre de comparaisons effectuées si on applique l’algorithme précédent à la chaine de caractères `'tab[2*(i + 4)] - tab[3]'`. En déduire le nombre maximum de comparaisons effectuées si on applique l’algorithme précédent à une chaine de caractères de taille $n$ (attention aux comparaisons de la classe pile)."
	Pour la chaine `'tab[2*(i + 4)] - tab[3]'` on va effectuer:
	
	- Pour chaque caractère deux comparaisons (parenthèse fermante ou ouvrante).
	- Pour chaque parenthèse fermante une comparaison en plus de correspondance.
	
	On a vingt-trois caractères et trois parenthèses fermantes, on va donc effectuer $23 \times 2 + 3 = 49$ comparaisons.
	On effectue de plus autant de comparaisons que de dépilements, donc de parenthèses fermantes, donc encore trois comparaisons de plus.
	On effectue donc en tout cinquante-deux comparaisons.
	
	Pour une chaine de taille $n$ on effectue donc pour chaque caractère:
	
	- Deux comparaisons pour le type de parenthèse.
	- Une comparaison de dépilement
	- Une comparaison de correspondance.
	
	Donc quatres comparaisons de caractères, soit $4 \times n$ comparaisons en pire cas.
	

??? question "Écrire une fonction `est_bien_parenthesee` qui prend en paramètre une chaine de caractères et qui implémente l’algorithme précédent."
	```python
	def est_bien_parenthesee(texte):
		p = Pile()
		for car in texte:
			if car in '({[': p.empiler(car)
			if car in ')}]':
				couple = p.depiler()+car
				if couple not in ['()', '[]', '{}']:
					return False
	    return p.est_vide()
	```
	
## EXERCICE 3 (8 points)

*Cet exercice porte sur les bases de données relationnelles, les requêtes SQL et la programmation en Python.*

L’énoncé de cet exercice utilise des mots-clés du langage SQL suivants : `#!sql SELECT, FROM, WHERE, JOIN... ON, UPDATE... SET, INSERT INTO... VALUES..., COUNT, ORDER BY`.

La clause `#!sql ORDER BY` suivie d’un attribut permet de trier les résultats par ordre croissant de l’attribut précisé. 
`#!sql SELECT COUNT(*)` renvoie le nombre de lignes d’une requête.

Amélie souhaite organiser sa collection de CD.
Elle a commencé par enregistrer toutes les informations sur un fichier CSV mais elle trouve que la recherche d’informations est longue et fastidieuse.
Elle repense à son cours sur les bases de données et elle se dit qu’elle doit pouvoir utiliser une base de données relationnelle pour organiser sa collection.

### Partie A

Dans cette partie on utilise une seule table.

Voici un extrait de la table `Chanson`.

<table>
<thead><tr><th>Chanson
<tbody><tr>
	<th>id
	<th>titre
	<th>album
	<th>groupe
<tr><td>1 <td>Sunburn<td> Showbiz<td> Muse
<tr><td>2 <td>Muscle Museum <td>Showbiz <td>Muse
<tr><td>3 <td>Showbiz<td> Showbiz<td> Muse
<tr><td>4 <td>New Born <td>Origin of Symmetry <td>Muse
<tr><td>5 <td>Sing for Absolution<td> Absolution<td> Muse
<tr><td>6 <td>Hysteria<td> Absolution <td>Muse
<tr><td>7 <td>Welcome too the Jungle<td> Appetite for Destruction <td>Guns N’ Roses
<tr><td>8 <td>Muscle Museum<td> Hullabaloo <td>Muse
<tr><td>9 <td>Showbiz <td>Hullabaloo<td> Muse
</tbody>
</table>

??? question "L’attribut `titre` peut-il être une clé primaire pour la table `Chanson`? Justifier."
	Pour être une clé primaire, il faut que toutes les valeurs de l'attribut soit uniques.
	Ici, l'attribut `titre` est dupliqué aux ids 2 et 8 (`Muscle Museum`), il ne peut donc pas être une clé primaire.

!!! question "Donner le résultat de la requête suivante :"
	```sql
	SELECT titre, album
	FROM Chanson
	WHERE groupe = 'Guns N’Roses';
	```
??? success "Réponse"
	Cette requête permet d'obtenir les titres et les albums faits par le groupe `Guns N'Roses`.
	Dans la table `Chansons` il n'y en a qu'un seul.
	Le résultat est donc: `Welcome too the Jungle Appetite for Destruction`.
	
??? question "Écrire une requête SQL permettant d’obtenir tous les titres des chansons de l’album Showbiz dans l’ordre croissant."
	```sql
	SELECT titre
	FROM Chanson
	WHERE album = 'Showbiz'
	ORDER BY titre ASC
	```
??? question "Écrire une requête SQL permettant d’ajouter la chanson dont le titre est Megalomania de l’album Hullabaloo du groupe Muse."
	On ne précise pas si l'id est automatiquement calculé ou non, les deux requêtes suivantes sont donc acceptables:
	
	```sql
	INSERT INTO Chanson (titre,album,groupe)
	VALUES ('Megalomania','Hullabaloo','Muse')
	```
	
	```sql
	INSERT INTO Chanson 
	VALUES (10,'Megalomania','Hullabaloo','Muse')
	```

Amélie a remarqué une faute de frappe dans la chanson Welcome too the Jungle qui s’écrit normalement Welcome to the Jungle.

??? question "Écrire une requête SQL permettant de corriger cette erreur."
	On peut faire une recherche selon le titre ou l'id:
	```sql
	UPDATE Chanson
	SET titre = "Welcome to the Jungle"
	WHERE titre = "Welcome too the Jungle";
	
	UPDATE Chanson
	SET titre = "Welcome to the Jungle"
	WHERE id = 7;
	```
	
### Partie B

Dans cette partie on utilise trois tables.

Voici des extraits des trois tables `Chanson`, `Album` et `Groupe`.


<table>
<thead><tr><th>Chanson
<tbody><tr>
	<th>id
	<th>titre
	<th>id_album
<tr><td>1 <td>Sunburn<td> 1
<tr><td>2 <td>Muscle Museum <td>1
<tr><td>3 <td>Showbiz<td> 1
<tr><td>4 <td>New Born <td>2
<tr><td>5 <td>Sing for Absolution <td>4
<tr><td>6 <td>Hysteria <td>4
<tr><td>7 <td>Welcome to the Jungle<td> 5
<tr><td>8 <td>Muscle Museum <td>3
<tr><td>9 <td>Showbiz<td> 3
</tbody>
</table>

<table>
<thead><tr><th>Album
<tbody><tr>
	<th>id<th>titre<th> année<th> id_groupe
<tr><td>1 <td>Showbiz <td>1999 <td>1
<tr><td>2<td> Origin of Symmetry<td> 2001<td> 1
<tr><td>3 <td>Hullabaloo <td>2002<td> 1
<tr><td>4 <td>Absolution <td>2003 <td>1
<tr><td>5 <td>Appetite for Destruction <td>1987<td> 2
</tbody>
</table>

<table>
<thead><tr><th>Groupe
<tbody><tr>
	<th>id<th> nom
<tr><td>1<td> Muse
<tr><td>2G <td>Guns N’ Roses
</tbody>
</table>


??? question "Expliquer l’intérêt d’utiliser trois tables `Chanson`, `Album` et `Groupe` au lieu de regrouper toutes les informations dans une seule table."
	Utiliser plusieurs tables permet d'éviter la redondance et de rendre les données plus cohérentes.
	De plus, si on a commis une erreur dans une valeur, un seul enregistrement est à modifier au lieu de plusieurs. (Par exemple pour le nom d'un groupe).
	
??? question "Expliquer le rôle de l’attribut `id_album` de la table `Chanson`." 
	`id_album` est une clé étrangère référençant la clé primaire de la table `Album`.
	Cela permet d'associer une ou plusieurs chansons à un album donné.
	
??? question "Proposer alors un schéma relationnel pour cette version de la base de données. On pensera à bien spécifier les clés primaires en les soulignant et les clés étrangères en les faisant précéder par le symbole `#`."
	Un schéma possible est:
	<pre markdown=1>
	Chanson(^^id^^,titre,#id\_album) FOREIGN KEY(id\_album) REFERENCES (Album.id)
	Album(^^id^^,titre,annee,#id\_groupe) FOREIGN KEY(id\_groupe) REFERENCES (Groupe.id)
	Groupe(^^id^^,nom)
	</pre>

??? question "Écrire une requête SQL permettant d’obtenir tous les noms des albums contenant la chanson Showbiz."
	```sql
	SELECT Album.nom
	FROM Chanson JOIN Album on Chanson.id_album = Album.id
	WHERE Chanson.titre = 'Showbiz';
	```
??? question "Écrire une requête SQL permettant d’obtenir tous les titres avec le nom de l’album des chansons du groupe Muse."
	```sql
	SELECT Chanson.titre, Album.nom
	FROM Chanson JOIN Album on Chanson.id_album = Album.id
	JOIN Groupe ON Album.id_groupe = Groupe.id
	WHERE Groupe.nom = 'Muse';
	```
!!! question "Décrire par une phrase ce qu’effectue la requête SQL suivante :"
	```sql
	SELECT COUNT(*) AS tot
	FROM Album AS a
	JOIN Groupe AS g ON a.id_groupe = g.id
	WHERE g.nom = 'Muse';
	```
??? success "Réponse"
	Cette requête permet de compter le nombre de lignes `COUNT(*) as tot` dans la table qui contient tout les albums où le nom du groupe est `Muse`.
	Elle permet donc de récupérer le nombre d'albums du groupe `Muse`, en supposant qu'aucun album n'est présent plusieurs fois.

### Partie C

Dans cette partie, on utilise Python.

Amélie a remarqué que son professeur ne parle jamais d’ordre alphabétique mais d’ordre lexicographique lorsqu’il fait une requête avec `ORDER BY`.

Elle a compris qu’il s’agissait de l’ordre du dictionnaire mais elle se demande comment elle pourrait elle-même écrire une fonction `ordre_lex(mot1, mot2)` de comparaison entre deux chaînes de caractères en utilisant l’ordre lexicographique.
La fonction `ordre_lex(mot1, mot2)` prend en arguments deux chaînes de caractères et renvoie un booléen.
Une rapide recherche lui permet de trouver le résultat suivant :

Lorsque l’on compare deux chaînes de caractères suivant l’ordre lexicographique, on commence par comparer les deux premiers caractères de chacune des deux chaînes, puis en cas d’égalité on s’intéresse au second, et ainsi de suite.
Le classement est donc le même que celui d’un dictionnaire.
Si lors de ce procédé on dépasse la longueur d’une seule des deux chaînes, elle est considérée plus petite que l’autre.
Lorsqu’on dépasse la longueur des deux chaînes au même moment, elles sont nécessairement égales

Amélie commence par écrire quelques assertions que sa fonction devra vérifier.

!!! question "Compléter les assertions suivantes :"
	```python linenums="1"
    assert ordre_lex("", "a") == True
	assert ordre_lex("b", "a") == ...
	assert ordre_lex("aaa", "aaba") == ...
	```
??? success "Réponse"
	```python linenums="1"
    assert ordre_lex("", "a") == True
	assert ordre_lex("b", "a") == False
	assert ordre_lex("aaa", "aaba") == True
	```
	
On suppose que les chaînes de caractères `mot1` et `mot2` ne sont composées que des lettres de l’alphabet, en minuscule, et la comparaison entre deux lettres peut se faire avec les opérateurs classiques `==` et `<`.
Par exemple :

```pycon linenums="1"
>>> "" < "a"
True
>>> "b" == "a"
False
```

Enfin, le slice `mot1[1:]` renvoie la chaîne de caractère de `mot1` privée de son premier caractère.
Par exemple :
```pycon linenums="1"
>>> mot1 = "abcde"
>>> mot2 = mot1[1:]
>>> mot
"bcde"
```

!!! question "Recopier et compléter la fonction récursive `ordre_lex` ci-dessous qui prend pour paramètre deux chaînes de caractères `mot1` et `mot2` et qui renvoie `True` si `mot1` précède `mot2` dans l’ordre lexicographique.
	```python linenums="1"
	def ordre_lex(mot1, mot2):
		if mot1 == "":
			return True
		elif mot2 == "":
			return False
		else:
			c1 = mot1[0]
			c2 = mot2[0]
			if c1 < c2:
				return ...
			elif c1 > c2:
				return ...
			else:
				return ...
	```

??? success "Réponse"
	```python linenums="1"
	def ordre_lex(mot1, mot2):
		if mot1 == "":
			return True
		elif mot2 == "":
			return False
		else:
			c1 = mot1[0]
			c2 = mot2[0]
			if c1 < c2:
				return True
			elif c1 > c2:
				return False
			else:
				return ordre_lex(mot1[1:],mot2[1:])
	```
	
??? question "Proposer une version itérative de la fonction `ordre_lex`."
	```python
	def ordre_lex_iter(mot1,mot2):
		if len(mot1) > len(mot2):
			return False
		for indice in len(mot1):
			if mot1[indice] > mot2[indice]:
				return False
			elif mot1[indice] < mot2[indice]:
				return True
	    return True
	```

