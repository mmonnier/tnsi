# Cours de terminale NSI

Ce site a vocation à héberger les documents de NSI de M. Monnier, au niveau terminale.

## Matériel nécessaire

Les Travaux Pratiques en Python se font sur [Thonny](https://thonny.org).

Des activités se font sur [Capytale](https://capytale2.ac-paris.fr) accessible via l'ENT, pour que les élèves puissent les travailler à la maison.

## Progression

Une **semaine** comprend **3 séances**.
Une **séance** comprend **2 heures**.

La progression suivie en 2024-2025 est celle de <https://fabricenativel.github.io/>.
Les différents documents utilisés en plus seront ajoutés dans les sections correspondantes *au fil de l'eau*.


### Semaine 1

??? note "Séance 1"
	**Révisions Gameshell et Python**

	A faire pour vendredi, les questions 1 et 2, les 3 et 4 sont optionnelles.
	
	!!! note "Exercice de programmation"
		Pour organiser l'année, il est plus facile de tout exprimer dans la même unité.
		L'organisation suivante: `Python: 1 h, Systèmes: 2 semaines, Algorithmique: 4 séances` n'est pas très claire.
		
		1. Écrivez des fonctions python permettant de convertir les différentes durées entre elles (heure -> semaine, heur -> séances, ...)
		2. Étant donné une liste de taches représentées en tuples `[('Python',1, 'h'),('Systèmes', 2, 's'),...]`, convertissez toutes les durées en heures.
		3. À partir de votre liste estimez le nombre de semaines nécessaires à la réalisation d'une liste de taches quelconque.
		4. Créez une IHM (web avec Flask, ligne de commande, graphique ...) permettant d'ajouter des taches à une liste et affichant le nombre d'heures, de séances et de semaines nécessaires à sa réalisation.

??? note "Séance 2"
	**Révisions Python**

	Pas de devoir pour mardi

??? note "Séance 3"
	**Révisions Python**

	<https://capytale2.ac-paris.fr/web/c/1692-3865560>

	<https://capytale2.ac-paris.fr/web/c/c541-3865556>

	!!! note "A faire pour mercredi"
		1. Lecture du diaporama pour réviser le python (commentaires, boucles, conditions, listes)
		2. Exercices 6 et 7 sur les listes
		
### Semaine 2
??? note "Séance 4"
	**Révisions Python**
	
	- Correction des notebooks liste et dictionnaire
	- Problème de Joséphus [Voir ici](fnat-corr/C0/index.md)

??? note "Séance 5"
	**Début de la récursivité**
	
	Notebook donnée en cours (notions de fonctions)

??? note "Séance 6"
	**Activités récursivité**
	
	- Activités débranchées
	- notebook partie  (notions de fonctions récursives)

### Semaine 3

??? note "Séance 7"
	**Activités récursivité**
	
	- Activités débranchées
	- notebook partie  (notions de fonctions récursives)


??? note "Séance 8"
	**Appels de fonctions et arbres d'appels**

	Cours complet sur la récursivité.

??? note "Séance 9"
	**Tours de hanoi et fin des notebooks"

### Semaine 4

??? note "Séance 10"
	Correction des notebooks et des tours de hanoi.

??? note "Séance 11"
	Exercices et activités de FNativel sur la récursivité.

??? note "Séance 12"
	Exercices et activités de FNativel sur la récursivité.

### Semaine 5

??? note "Séance 13"
	Bases de données, branchées et débranchées.

	**À faire pour la prochaine séance:**
	- Mettre au propre les 4 premiers diaporamas
	- Trouver une BDD intéressante et son schéma ainsi qu'une donnée marquante.


??? note "Séance 14"
	BDD branchées

??? note "Séance 15"
	BDD branchées

### Semaine 6

??? note "Séance 16"
	Bases de données, branchées et débranchées.

	**À faire pour la prochaine séance:**
	- Mettre au propre les 4 premiers diaporamas
	- Trouver une BDD intéressante et son schéma ainsi qu'une donnée marquante.


??? note "Séance 17"
	BDD branchées

??? note "Séance 18"
	BDD branchées


### Semaine 7

??? note "Séance 19"
	Processus, programmes.

	* Activité 1 Fabrice Nativel
	* Schéma d'état des processus

??? note "Séance 17"
	DS BDD.

	Chronogrammes

??? note "Séance 18"
	Chronogramme et Interblocages


### Semaine 8 - Vacances

À faire:

* Exercices sur les chronogrammes
* Lire le cours Diviser pour régner (Chapitre 13 - page 221)
* Exercice simple

### Semaine 9 - Vacances

### Semaine 10 - Diviser pour Régner

### Semaine 11 - POO

### Semaine 12 - Projets POO

### Semaine 13 - Structures linéaires

### Semaine 14 - SL Piles et Files

### Semaine 15 - Arbres

??? note "Séance 33"
	* Arbres, vocabulaire et mise en pratique
	À faire : Exercice 1 et 2 Fabrice nativel

??? note "Séance 34"
	* Arbres Binaires, Implémentation et taille
	
	À faire : Exercice 71 du livre, dénombrement des Arbres Binaires en fonction de la hauteur.
	
??? note "Séance 35"
	* Arbres Binaires, Notebook sur l'implémentation
### Semaine 16 - Arbres

??? note "Séance 36"
	* Arbres Binaires, Parcours d'arbre et POO

??? note "Séance 37"
	* Arbres Binaires, Exercices sur les arbres

??? note "Séance 38"
	* Examen Type Bac

### Semaine 17 - Vacances

### Semaine 18 - Vacances

### Semaine 19 - Schémas de BDD

??? note "Séance 39"
	* Rappels sur le vocabulaire (20 minutes)
	* Activité 1 du C9 (20 minutes jusqu'à la q. 3(d))
	

	À faire: Reprendre le premier diaporama (INSERT/UPDATE/DELETE)


??? note "Séance 40"
	* Reprise de la syntaxe des requêtes de modification
	* Fin Activité 1 et début Activité 2 du C9 jusqu'aux jointures (exclues)
	* Intégrité Référentielle donnée
	
	À faire: Reprendre toutes les définitions en rouge du diaporama dans le cours.
	
??? note "Séance 41"
	* Activité 2 du C9 finie et corrigée (Jointure de tables)
	* Exercices de N. Reveret pour reprendre toutes les BDD.
	* Un exercice de bac au choix à faire par deux et à présenter pour la prochaine séance.
	
### Semaine 20 - Schémas de BDD (pratique) et Algorithmie des arbres
	
??? note "Séance 42"
	TP noté, manipulation de bases de données [ici](https://mmonnier.forge.apps.education.fr/tnsi/bdd/)

??? note "Séance 43"
	Parcours en largeur des arbres
	
	- Partie de cours à noter (sur les trois parcours)
	- Activité 1 [FN C10](https://fabricenativel.github.io/Terminale/algoarbre)

??? note "Séance 44"
	Arbres binaire de recherche

	- Activité 2 FN C10
	- Cours à écrire au tableau et noté.

### Semaine 22

??? note "Séance 45"
	Graphes, définitions et vocabulaire

	- Activité 1 [FN C11](https://mmonnier.forge.apps.education.fr/tnsi/fnat-corr/C11/)

??? note "Séance 46"
	Graphes, représentation

	- TP Capytale [ici](https://capytale2.ac-paris.fr/web/c/9a14-5480924)
	- Exercices à faire :  Jusqu'à [lui](https://mmonnier.forge.apps.education.fr/tnsi/fnat-corr/C11/#parcours-dun-graphe_1)
	- Finir le TP Capytale

??? note "Séance 47"
	Formation JDI NSI

### Semaine 23

??? note "Séance 48"
	Parcours de Graphe
	
	- Mini projets : Chaine de Markov / Réseaux / Jeu de Nim / PageRank / Chemins dans les mots. 

??? note "Séance 49"
	Recherche du plus court chemin

	- Mini projets

??? note "Séance 50"
	Algorithme de Dijkstra (exemple)

	- Mini projets

### Semaine 24

??? note "Séance 51"
	Programmation Dynamique Introduction (<https://info-mounier.fr/terminale_nsi/algorithmique/programmation-dynamique>)

	- Activité capytale Toto le robot

??? note "Séance 52"
	Programmation dynamique

	- Cours mémoisation et programmation dynamique
	- Problème du recouvrement de grille par un carré
	- Notebook d'exercices (C.Poulmaire)

??? note "Séance 53"
	DS Graphes et parcours de graphes (1h)

	- Ré-introduction au réseau (vidéos/filius) (C.Poulmaire)
	- Fin des exos programmation dynamique

### Semaine 25

??? note "Séance 54"
	Protocoles de routage et réseau
	Points de cours IP/Masques/TCP-IP

	- Correction exercices (C.Poulmaire)
	- Exercices 10,11,12

??? note "Séance 55"
	Points de cours sur RIP

	- Exercices à continuer


??? note "Séance 56"
	Points de cours sur OSPF

	- Exercices à continuer

### Semaine 26 - VACANCES

### Semaine 27 - VACANCES

