---
author: Laura Fléron
title: 📖 Ep 04

tags: 
 - KNN
 - Listes Min max Moy Rch
---

#### Issue de : 23-NSI-06

## EXERCICE 1

Programmer la fonction `recherche`, prenant en paramètre un tableau non vide `tab` (type `list`) d'entiers et un entier `n`, et qui renvoie l'indice de la **dernière** occurrence de l'élément cherché.

Si l'élément n'est pas présent, la fonction renvoie `None`.

### Pas à pas

#### Le squelette.

On nous demande ici de programmer une fonction (`recherche`) à deux paramètres (`tab` et `n`).
On peut donc déjà créer le squelette de la fonction avec sa documentation:

```python3
def recherche(tab,n):
	'''
    Renvoie l'indice de la dernière occurence de n dans tab.
	Entrée:
		- tab: liste d'entiers.
		- n: un entier, présent ou non dans tab.
	Sortie:
		- Le dernier indice de n si n est présent.
		- None sinon
	'''
	pass
```

#### Les tests

On peut ensuite rédiger quelques tests pour vérifier que notre futur fonction **est exempt des bugs courants**.
La fonction marche-t-elle:

- sur un tableau avec un `n` ? `assert recherche([10,20,30],20) == 1, "Erreur sur l'élément présent une fois"`;
- sur un tableau vide ? `assert recherche([],123) == None, "Erreur sur le tableau vide"`;
- sur un tableau avec plusieurs `n` ? `assert recherche([10,20,30,20,10],20) == 3, "Erreur sur l'élément présent deux fois"`;
- sur un tableau sans `n` ? `assert recherche([10,20,30],40) == None, "Erreur sur l'élément non présent"`.

On doit ajouter ces tests après la définition de la fonction:

```python
def recherche(tab,n):
	'''
    Renvoie l'indice de la dernière occurence de n dans tab.
	Entrée:
		- tab: liste d'entiers.
		- n: un entier, présent ou non dans tab.
	Sortie:
		- Le dernier indice de n si n est présent.
		- None sinon
	'''
	pass

assert recherche([10,20,30],20) == 1, "Erreur sur l'élément présent une fois"
assert recherche([],123) == None, "Erreur sur le tableau vide"
assert recherche([10,20,30,20,10],20) == 3, "Erreur sur l'élément présent deux fois"
assert recherche([10,20,30],40) == None, "Erreur sur l'élément non présent"
```

#### L'implémentation

On ne nous donne pas ici d'informations sur la complexité ou la façon de programmer.
Nous sommes donc libre d'écrire une fonction efficace ou non.

On se rappelle cependant que la recherche (d'un élément, du maximum, du minimum) est **linéaire**.
Elle peut donc s'écrire avec une simple boucle for qui va faire **une action pour chaque élément** et renvoyer un résultat final.
On cherche **un indice** on doit donc faire un parcours par indice.

```python hl_lines="3 4 5"
def recherche(tab,n):
	...
	for indice in range(len(tab)):
		...
	return ...
```

On cherche à stocker l'indice d'un élément, on doit donc avoir une variable pour stocker cet indice.
Au départ, `n` n'est nul part, on peut donc mettre cette variable à `None`

```python hl_lines="2"
def recherche(tab,n):
	indice_n = None
	for indice in range(len(tab)):
		...
	return ...
```

On doit ensuite vérifier **pour chaque indice** du tableau si **l'élément associé** n'est pas celui recherché.
Si c'est le cas, on stockera l'indice dans notre variable.
On pourra alors renvoyer cet indice.

```python hl_lines="3 4 5"
def recherche(tab,n):
	indice_n = None
	for indice in range(len(tab)):
		if tab[indice] == n:
			indice_n = indice
	return indice_n
```

Tout les tests passent normalement.

#### Les erreurs

??? danger "Utiliser un parcours par élément (`for indice in tab`)"

	À ce moment, `indice_n` ne commence pas à 0 et vaut les éléments du tableau.
	Le test avec `n` présent ne passe pas.

	Pour l'éviter, on ne mettra pas d'éléments que l'on peut confondre avec des indices (0,1,2,3 à  éviter.)

??? danger "Ne pas renvoyer `indice_n`"
	
	Le premier test ne passe pas, on récupère `None` au lieu d'un indice.

??? danger "Renvoyer dans la boucle"

	On récupèrera l'indice de la première occurence au lieu de la deuxième, le test 3 ne passera pas.


## EXERCICE 2

On souhaite programmer une fonction donnant la distance la plus courte entre un point de départ et une liste de points. Les points sont tous à coordonnées entières.

Les points sont donnés sous la forme d'un tuple de deux entiers. 

La liste des points à traiter est donc un tableau de tuples.

On rappelle que la distance entre deux points du plan de coordonnées $(x;y)$ et $(x';y')$ est donnée par la formule :

$$d^2=(x-x')^2+(y-y')^2$$

Compléter le code des fonctions `distance_carre` et `point_le_plus_proche` fournies ci-dessous pour qu’elles répondent à leurs spécifications.

```python linenums='1'
def distance_carre(point1, point2):
    """ Calcule et renvoie la distance au carre entre 
    deux points."""
    return (...)**2 + (...)**2 

def point_le_plus_proche(depart, tab):
    """ Renvoie les coordonnées du premier point du tableau tab se 
    trouvant à la plus courte distance du point depart."""
    min_point = tab[0]
    min_dist = ... 
    for i in range(1, len(tab)):
        if distance_carre(tab[i], depart) < ...: 
            min_point = ... 
            min_dist = ... 
    return min_point
```

### Les tests

On commence par rajouter des tests pour vérifier directement nos fonctions:

#### Pour `distance_carre`:
  * la distance d'un point à lui même est 0 : `assert distance_carre((1,2),(1,2)) == 0, "Un point n'est pas à zéro de lui même"`;
  * deux points à une distance de 1 : `assert distance_carre((1,1),(0,0)) == 1, "Point à une unité mal calculé"`;
  * l'ordre n'a pas d'importance: `assert distance_carre((1,1),(0,0)) == distance_carre((0,0),(1,1)), "Ordre trop important"`.
  
#### Pour `point_le_plus_proche`:
  - les points dans le bon ordre: `assert le_plus_proche((0,0),[(1,1),(2,2),(3,3)]) == (1,1)`;
  - les points dans le mauvais ordre: `assert le_plus_proche((0,0),[(10,10),(5,5),(3,3)]) == (3,3)`.

### Implémentation

Pour `distance_carre` on nous donne la formule.
Ici, la difficulté est de comprendre que `point1` et `point2` sont des tuples donc on a $(x,y) \leftrightarrow \text{point1}$ et $(x',y') \leftrightarrow \text{point2}$

On récupérera donc ici les **abscisses** $x,x'$ (resp. les **ordonnées** $y,y'$) avec `point1[0]` et `point2[0]` (resp. `point1[1]` et `point2[1]`).
On les utilisent ensuite dans la formule:

```python hl_lines="3"
def distance_carre(point1,point2):
	...
	return (point1[0] - point2[0])**2 + (point1[1] - point2[1])**2
```

Pour `point_le_plus_proche` on remplit ligne par ligne.
D'abord, on observe que `min_dist` est initialisée puis mise à jour à chaque tour de boucle.
On en déduit que `min_dist` est la distance minimum actuelle, donc entre `min_point` et `depart`: `min_dist = distance_carre(depart,min_point)` aux lignes `min_dist = ...`

La condition fait intervenir la distance, on doit donc comparer avec une distance: `if distance_carre(tab[i],depart) < min_dist` donc.
Enfin on doit mettre à jour `min_point` avec le nouveau point plus proche.
Comme dans toute recherche il s'agit ici de l'élément courant, soit `tab[i]` donc: `min_point = tab[i]`
