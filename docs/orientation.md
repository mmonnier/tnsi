---
title: Orientation après NSI
author: Marius Monnier
---

# Quelques ressources sur l'orientation

## Générales

* Une vidéo **très** longue sur les différents parcours: <https://www.youtube.com/watch?v=kVFjI3cNn7U> et les transparents associés <https://mermet.users.greyc.fr/orientation2022v2.pdf>
* En 2020: <https://www.infoforall.fr/art/nsi/orientation-avec-la-nsi/>
* Les données ouvertes de parcoursup 2022 (<https://data.enseignementsup-recherche.gouv.fr/pages/parcoursupdata/?disjunctive.fili&sort=tri&refine.contrat_etab=Public&q=informatique&refine.fili=Licence&refine.fili=CPGE>)


## Licences informatiques

* <https://www.univ-paris8.fr/-Licence-Informatique-585->
* <https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/licence-XA/licence-informatique-IAI7UC15.html>
* ...

## Classes préparatoire

Une liste des MP2I en 2024: <https://thotismedia.com/annuaire-parcoursup-cpge-mp2i/>

Une liste des MP2I ouvertes en 2023:

| Lycée                    | Ville            | Département | Places /Ouverture 2023 | Taux |
|--------------------------|------------------|-------------|----------------|--------|
| Valbonne                 | Valbonne         | 6           | 48             | 29 %   |
| Thiers                   | Marseille        | 13          | 48             | 35 %   |
| Carnot                   | Dijon            | 21          | 47             | 51 %   |
| Victor Hugo              | Besançon         | 25          | 45             | 63 %   |
| Pierre de Fermat         | Toulouse         | 31          | 48             | 10 %   |
| Montaignes               | Bordeaux         | 33          | 48             | 19 %   |
| Joffre                   | Montpellier      | 34          | Oui            |
| Descartes                | Tours            | 37          | 48             | 19 %   |
| Champollion              | Grenoble         | 38          | 48             | 24 %   |
| Claude Fauriel           | Saint-Etienne    | 42          | 48             | 57 %   |
| Clémenceau               | Nantes           | 44          | 48             | 15 %   |
| Franklin Roosevelt       | Reims            | 51          | 48             | 60 %   |
| Henri Poincaré           | Nancy            | 54          | 35             | 36 %   |
| Alain René Lesage        | Vannes           | 56          | 48             | 81 %   |
| Henri Wallon             | Valenciennes     | 59          | 24             | 46 %   |
| Alfred Kastler           | Denain           | 59          | Oui            |
| Faidherbe                | Lille            | 59          | 48             | 20 %   |
| Frédéric Ozanam          | Lille            | 59          | Oui            |
| Colbert                  | Tourcoing        | 59          | 24             | 50 %   |
| Guy Mollet               | Arras            | 62          | Oui            |
| La Fayette               | Clermont-Ferrand | 63          | Oui            |
| Kléber                   | Strasbourg       | 67          | 48             | 28 %   |
| Le Parc                  | Lyon             | 69          | 48             | 10 %   |
| Aux Lazaristes           | Lyon             | 69          | 10             | 13 %   |
| La Martinière Monplaisir | Lyon             | 69          | Oui            | 48     |
| Berthollet               | Annecy           | 74          | Oui            | 24     |
| Louis le Grand           | Paris            | 75          | 48             | 7 %    |
| Saint-Louis              | Paris            | 75          | 48             | 8 %    |
| Paul Valéry              | Paris            | 75          | 48             | 31 %   |
| Janson De Sailly         | Paris            | 75          | 48             | 23 %   |
| Fénelon Sainte-Marie     | Paris            | 75          | 40             | 38 %   |
| Pierre Corneille         | Rouen            | 76          | Oui            |
| Hoche                    | Versailles       | 78          | 48             | 13 %   |
| LouisThuillier           | Amiens           | 80          | 48             | 77 %   |
| Camille Guérin           | Poitiers         | 86          | 48             | 78 %   |
| Gay Lussac               | Limoges          | 87          | 24             | 23 %   |
| Charles Coeffin          | Baie-Mahault     | 971         | 24             | 76 %   |
| Leconte de Lisle         | Saint-Denis      | 974         | 24             | 73 %   |

Un exemple de présentation d'une MP2I (Paul Valéry ici): <https://ineskkk.github.io/entrer-en-mp2i-a-pv/>

## Pour les INSA:

Vous pouvez les prendre avec NSI ! (cf <https://www.groupe-insa.fr/preparer/comment-candidater/questions-frequemment-posees#collapse-4>)
