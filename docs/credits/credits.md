---
author: Marius Monnier
title: Crédits
---

Le site est hébergé sur [`#LaForgeEdu`](https://forge.apps.education.fr/), administrée auparavant par [l'AEIF](https://aeif.fr)

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

😀 Un grand merci à  [Mireille COILHAC](https://forge.apps.education.fr/mcoilhac) [Vincent-Xavier Jumel](https://forge.aeif.fr/vincentxavier),[Vincent Bouillot](https://gitlab.com/bouillotvincent)et [Charles Poulmaire](https://forge.aeif.fr/cpoulmaire) qui ont réalisé le modèle de ce site.

L'adresse originale du rendu de ce site modèle avant clonage est : 

[Modèle de projet avec Python](https://modeles-projets.forge.aeif.fr/mkdocs-pyodide-review/)

