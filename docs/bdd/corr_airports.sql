/*
TABLE airport_frequencies {
	id INTEGER [PRIMARY KEY]
	airport_ref INTEGER [ref: > airports.id]
	airport_ident TEXT [ref: > airports.ident]
	type TEXT
	description TEXT
	frequency_mhz REAL
}

TABLE airports {
	id INTEGER [PRIMARY KEY]
	ident TEXT [PRIMARY KEY]
	type TEXT
	name TEXT
	latitude_deg REAL
	longitude_deg REAL
	continent TEXT
	iso_country TEXT [ref: > countries.code]
	iso_region TEXT [ref: > regions.code]
	municipality TEXT
	scheduled_service TEXT
	gps_code TEXT
	iata_code TEXT
	local_code TEXT
	home_link TEXT
	wikipedia_link TEXT
	keywords TEXT
}

Table countries {
  id INTEGER
  code TEXT [primary key]
  name TEXT
  continent TEXT
  wikipedia_link TEXT
  keywords TEXT
}

TABLE navaids {
  id INTEGER [primary key]
	filename TEXT
	ident TEXT
	name TEXT
	type TEXT
	frequency_khz REAL
	latitude_deg REAL
	longitude_deg REAL
	elevation_ft INTEGER
	iso_country TEXT [ref: > countries.code]
	dme_frequency_khz REAL
	dme_channel TEXT
	dme_latitude_deg REAL
	dme_longitude_deg REAL
	dme_elevation_ft INTEGER
	slaved_variation_deg REAL
	magnetic_variation_deg REAL
	usageType TEXT
	power TEXT
	associated_airport TEXT [ref: > airports.ident]
}

TABLE regions {
	id INTEGER 
	code TEXT [primary key]
	local_code TEXT
	name TEXT
	continent TEXT
	iso_country TEXT [ref: > countries.code]
	wikipedia_link TEXT
	keywords TEXT
}

TABLE runways {
	id INTEGER [primary key]
	airport_ref INTEGER [ref: > airports.id]
	airport_ident TEXT [ref: > airports.ident]
	length_ft INTEGER
	width_ft INTEGER
	surface TEXT
	lighted INTEGER
	closed INTEGER
	le_ident TEXT
	le_latitude_deg REAL
	le_longitude_deg REAL
	le_elevation_ft INTEGER
	le_heading_degT REAL
	le_displaced_threshold_ft INTEGER
	he_ident TEXT
	he_latitude_deg REAL
	he_longitude_deg REAL
	he_elevation_ft INTEGER
	he_heading_degT REAL
	he_displaced_threshold_ft INTEGER
}
*/

SELECT name,id from airports where iso_country like 'fr';

INSERT INTO airport_frequencies (airport_ref, type, frequency_mhz) 
VALUES (28927,"A/A",404);

SELECT * from airport_frequencies where frequency_mhz = 404;

UPDATE airport_frequencies 
SET description = 'Fréquence de controLe des éLèves' 
WHERE frequency_mhz = 404 and airport_ref = 28927;

DELETE from airport_frequencies where frequency_mhz BETWEEN 100 and 120;

SELECT name from regions where continent like 'eu' order by name ASC;

SELECT max(frequency_mhz)
FROM airport_frequencies
JOIN airports on airport_frequencies.airport_ref = airports.id
where airports.type like '%smaLL%' and airports.iso_country like 'af';

SELECT length_ft
from runways
join airports on runways.airport_ref = airports.id
where airports.type like '%smaLL%' and airports.iso_country like 'au';