INSERT INTO races(name, date, time)
VALUES ('Grand Échirolles Tour', date('now'), '18:00:00');

UPDATE races
SET circuitId = 6, round = 1
WHERE name like '%Échirolles%';

INSERT INTO drivers (forename, surname)
VALUES ('Andréa','lité');

INSERT INTO results (raceId, driverId, position, time)
VALUES (1010,844, 1, 8.72);

SELECT circuitId, name, url
FROM circuits
WHERE country like 'france';

SELECT forename, surname
FROM drivers
WHERE dob > 1980 AND nationality like 'italian'
ORDER BY surname;

SELECT results.time
FROM results
JOIN races ON races.raceId = results.raceId
JOIN circuits ON circuits.circuitID = races.circuitID
WHERE results.position = 1 AND circuits.country like 'australia';

SELECT DISTINCT drivers.code
FROM results
JOIN races ON races.raceId = results.raceId
JOIN circuits ON circuits.circuitID = races.circuitID
JOIN drivers ON drivers.driverId = results.driverId
WHERE results.position = 1 AND (circuits.country like "malaysia" OR circuits.country like 'france')