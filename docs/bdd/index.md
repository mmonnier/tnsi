# Bases de données et SGBD

Dans le TP suivant vous allez devoir mettre à profit les différents types de requêtes que vous avez étudiés en cours.
Tout au long de ce TP, n'hésitez pas à utiliser le [mémento de N. Reveret](https://nreveret.forge.apps.education.fr/exercices_bdd/memento_sql/) qui vous permettra de retrouver les syntaxes oubliés, ainsi que vos propres notes de cours.

Vous aurez le choix dans la base que vous manipulez.
Ne faites donc que les questions **en rapport avec votre base de données**.

Votre rendu consistera en un document PDF ou Markdown ou bien un fichier SQLite contenant les parties suivantes:

- nom, prénom;
- présentation succincte de la base choisie (deux phrases);
- schéma relationnel de votre base de données (n'hésitez pas à utiliser <https://dbdiagram.io/d> ou <https://app.diagrams.net/> pour une image **ou a l'écrire comme vu en cours**);
- pour chaque consigne demandée, vous donnerez la requête utilisée (sur une ou plusieurs lignes), ainsi que les tables qu'elles fait intervenir et le nombre de résultats que vous obtenez.

## Travail sur une base de données existante

Votre schéma relationnel compte pour 3 points.

Choisissez une base de données parmi les suivantes:

### Des formules 1, simple

Téléchargez [ce fichier](https://mmonnier.forge.apps.education.fr/tnsi/bdd/fm1_4.db).
	
Vos consignes sont:

- Insérez une nouvelle course ayant lieu aujourd'hui à 18h et nommée "Grand Échirolles Tour", pourquoi cette opération ne remet-elle pas en question l'intégrité référentielle ? (/1,5)
- Modifiez la pour qu'elle se déroule sur le circuit d'id 6 en 1 round seulement. (/1)
- Insérez un nouveau conducteur, participant à cette course qui est arrivé premier avec un temps de 8.72. (/2)
- Affichez tout les circuits et leur URL, se trouvant en France. (/1)
- Affichez toutes les conductrices et conducteurs né(e)s après 1980 et parlant Italien, trié(e)s par nom de famille. (/1,5)
- Affichez la moyenne des temps des personnes arrivées en première position dans les courses sur circuit australien. (/2)
- Affichez le code de toutes les personnes arrivées en première position dans les courses sur circuit français ou Malaisien. (/3)

??? success "Une correction"
	![Un schéma de base](https://mmonnier.forge.apps.education.fr/tnsi/bdd/fm1_4_schema.svg)
	
	```sql
	INSERT INTO races(name, date, time)
	VALUES ('Grand Échirolles Tour', date('now'), '18:00:00');

	UPDATE races
	SET circuitId = 6, round = 1
	WHERE name like '%Échirolles%';

	INSERT INTO drivers (forename, surname)
	VALUES ('Andréa','lité');

	INSERT INTO results (raceId, driverId, position, time)
	VALUES (1010,844, 1, 8.72);

	SELECT circuitId, name, url
	FROM circuits
	WHERE country like 'france';

	SELECT forename, surname
	FROM drivers
	WHERE dob > 1980 and nationality like 'italian'
	order by surname;

	SELECT AVG(results.time)
	FROM results
	JOIN races ON races.raceId = results.raceId
	JOIN circuits ON circuits.circuitID = races.circuitID
	where results.position = 1 and circuits.country like 'australia';

	SELECT DISTINCT drivers.code
	FROM results
	JOIN races ON races.raceId = results.raceId
	JOIN circuits ON circuits.circuitID = races.circuitID
	JOIN drivers ON drivers.driverId = results.driverId
	WHERE results.position = 1 AND (circuits.country lIKE "malaysia" or circuits.country lIKE 'france')
	```

### Des aéroports

Téléchargez [ce fichier](https://mmonnier.forge.apps.education.fr/tnsi/bdd/airports.db).

Vos consignes sont:

- Affichez tout les noms et Ids des aéroports français. (/1,5)
- Insérez une nouvelle fréquence de 404 MhZ pour l'aéroport: Aérodrome de Grenoble Le Versoud, de type "A/A". (/1,5)
- Modifiez la description de cette fréquence pour qu'elle soit "Fréquence de contrôle des élèves". (/1,5)
- Supprimez toutes les fréquences ATIS entre 100 et 120 MhZ inclus, pourquoi cette opération ne peut-elle pas lever d'erreurs ? (/2)
- Affichez toutes les régions européennes classées par ordre alphabétique (/1,5)
- Affichez la fréquence maximale utilisée dans les aéroports d'Afghanistan (/2)
- Affichez les longueurs des pistes d'atterrissages des **petits** aéroports australiens. (/2)

??? success "Une correction"
	![Un schéma de base](https://mmonnier.forge.apps.education.fr/tnsi/bdd/airports_schema.svg)

	```sql
	SELECT name,id FROM airports WHERE iso_country lIKE 'fr';

	INSERT INTO airport_frequencies (airport_ref, type, frequency_mhz) 
	VALUES (28927,"A/A",404);
	
	SELECT * FROM airport_frequencies WHERE frequency_mhz = 404;
	
	UPDATE airport_frequencies 
	SET description = 'Fréquence de controLe des éLèves' 
	WHERE frequency_mhz = 404 AND airport_ref = 28927;
	
	DELETE FROM airport_frequencies WHERE frequency_mhz BETWEEN 100 AND 120;
	
	SELECT name FROM regions WHERE continent lIKE 'eu' ORDER BY name ASC;
	
	SELECT max(frequency_mhz)
	FROM airport_frequencies
	JOIN airports ON airport_frequencies.airport_ref = airports.id
	WHERE airports.type lIKE '%smaLL%' AND airports.iso_country lIKE 'af';
	
	SELECT length_ft
	FROM runways
	JOIN airports ON runways.airport_ref = airports.id
	WHERE airports.type lIKE '%smaLL%' AND airports.iso_country lIKE 'au';
	```
### Des musiques, plus complexe

Téléchargez [ce fichier](https://mmonnier.forge.apps.education.fr/tnsi/bdd/chinook.db)

Vos consignes sont:

- Affichez le nom des musiques vendues à plus de 0.99, triées selon leur durée en millisecondes. (/1,5)
- Modifiez la playlist d'id 7 pour qu'elle se nomme "Movies (to delete ???)" (/1,5)
- Supprimez tout les clients n'ayant pas de numéro de Fax, une erreur est-elle levée et si oui pourquoi ? (/1,5)
- Donnez le nombre de clients ayant une adresse finissant par ".com" (/2)
- Affichez les titres des albums de Queen. (/2,5)
- Affichez les titres des albums et le nom du groupe, ainsi que le genre des musiques de la playlist Grunge. (/3)

??? success "Une correction"
	![Un schéma de base](https://mmonnier.forge.apps.education.fr/tnsi/bdd/chinook_schema.svg)

	```sql
	SELECT Name FROM Track WHERE UnitPrice > 0.99 ORDER BY Milliseconds;

	UPDATE Playlist SET Name = 'Movies (to delete ???)' WHERE PlaylistId=7;

	DELETE FROM Customer WHERE Fax is NUll; --erreur d'intégrité référentielle, ces clients sont utilisés.

	SELECT count(*) from Customer where Email like '%.com';

	SELECT Album.Title FROM Album JOIN Artist ON Album.ArtistId = Artist.ArtistId  WHERE Artist.Name = 'Queen';

	SELECT Album.Title, Artist.Name, Genre.Name FROM Playlist 
	JOIN PlaylistTrack ON Playlist.PlaylistId = PlaylistTrack.PlaylistId
	JOIN Track ON PlaylistTrack.TrackId = Track.TrackId
	JOIN Album ON Album.AlbumId = Track.AlbumId
	JOIN Artist ON Artist.ArtistId = Album.ArtistId
	JOIN Genre ON Track.GenreId = Genre.GenreId
	WHERE Playlist.Name like 'Grunge';
	```
	
### Des restaurants, rattrapage

Téléchargez [ce fichier](https://mmonnier.forge.apps.education.fr/tnsi/bdd/restaurants.db)

Dans cette base de données, aucune clé étrangère ni clé primaire n'est répertoriée.

- Donnez une ou des clé primaire pour les tables *Customers*, *Dishes* et *CustomerDishes*, expliquez en une phrase votre choix de clé primaire. (/2)
- Aucune contrainte d'intégrité référentielle n'est implémentée ici, proposez deux contraintes de type clé étrangère pour cette base, en expliquant ce qu'elles représentent au niveau des données de la table. (/2)

On suppose maintenant que des contraintes de clés primaires ont été ajoutées sur tout les attributs de la forme `NomDeTableID` et des contraintes de clés étrangères sur ces mêmes attributs lorsqu'ils sont présents dans d'autre tables.
Par exemple, dans la table `OrderDishes`, les deux attributs `OrderID` et `DishesID` sont des clés étrangères référençant les clés primaires des tables `Orders` et `Dishes`.

- Affichez tout les clients vivant à Springfield; (/1)
- Insérez un nouveau plat (Dishes) nommé "Tiramisu", coûtant 10.5 et ayant pour description "Un bon dessert". (/1,5)
- Supprimez toutes les réservations ayant un nombre d'invités (PartySize) inférieur ou égal à deux. (/1,5)
- Affichez tout les noms et descriptions des plats favoris des clients vivant à Springfield. (/3)
- Affichez le nombre de plats commandés par le client d'email `loverg@wisdompets.com` (On utilisera les tables, *Customers* et *Orders* et une autre au moins). (/4)

??? question "Une correction"
	Pour *Customers* et *Dishes* on peut choisir les attributs **ID**, car on remarque qu'ils ne sont jamais dupliqués dans les tables.
	Pour *CustomerDishes*, on doit choisir la clé primaire constituée des deux colonnes car les deux autres sont répétées plusieurs fois, on pourrait aussi en créer une nouvelle.
	
	```sql
	SELECT * FROM Customers WHERE City like 'Springfield';

	INSERT INTO Dishes(Name,Price,Description) VALUES ('Tiramisu',10.5,'Un bon dessert');

	DELETE FROM Reservations WHERE PartySize <= 2;

	SELECT Dishes.Name, Dishes.Description FROM Customers 
	JOIN Dishes ON Customers.FavoriteDish = Dishes.DishID
	WHERE Customers.City like 'Springfield';

	SELECT count(*)
	FROM OrdersDishes
	JOIN Orders ON OrdersDishes.OrderId = Orders.OrderID
	JOIN Customers on Orders.CustomerID = Customers.CustomerID
	WHERE Customers.Email like 'loverg@wisdompets.com';
	```
