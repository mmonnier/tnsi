---
author: Marius Monnier
title: Sujets de grand oral
---

!!! note "Origine des sujets"
	Les sujets ci-dessous sont extraits de divers sites web ainsi que des sessions vécues.
	Ils ne constituent pas une liste exhaustive et ont été sélectionnés manuellement.


# Le grand oral

Voir [la page eduscol](https://eduscol.education.fr/729/presentation-du-grand-oral).

Ou bien l'explication de Thomas Beline <https://kxs.fr/cours/grand-oral/>

Et bien sûr les modalités résumées:

![Modalités de l'épreuve](https://eduscol.education.fr/sites/default/files/styles/image_used_in_block_media_full_height/public/2022-10/infog_epreuve_orale_terminale_grandoral_v4.jpg)

# Exemples de sujets

Cette liste s'enrichira au cours de l'année et de vos propositions.

## Accessible à partir de la Première

- Licences libres et propriétaires, laquelle choisir ?
- Comment l’art de couper en deux permet-il de résoudre des problèmes en mathématiques et informatique ?
- Comment les caractères de nos claviers sont-ils encodés ?
- Que se cache-t-il derrière une page web ?
- Quelles sont les différentes étapes pour parvenir à un site web complet ?
- Comment l’algèbre booléenne a-t-elle permis le développement de l’informatique ?
- Encoder toutes les écritures du monde avec des 1 et des 0
- Femmes et numérique : quelle histoire ? Et où en sommes nous aujourd’hui?
- Pourquoi existe-t-il tant de langages informatiques ?
- Comment créer un programme qui donne un résultat entier avec des chiffres romains ?
- Ada Lovelace, pionnière du langage informatique.
- Jeux vidéos, développement et compétences attendues.

## Accessible à partir de la Terminale

- Pourquoi chiffrer les communications ?
- Comment le chiffrement RSA sécurise-t-il nos données ?
- Les bugs.
- Intelligence artificielle générative, quel modèle de création ? (Peinture / Lettres / Musique / NSI / Économie / EMC)

## Société

- Quels sont les impacts de l’obsolescence programmée dans la société ?
- Nos données personnelles sont-elles toujours personnelles ?
- Faut-il autoriser les voitures autonomes ?

## Déconseillé (mais si vous êtes motivé•es ...)

- Quels sont les enjeux démocratiques des NFT ?
- Comment l'intelligence artificielle révolutionne-t-elle déjà le monde d’aujourd’hui et de demain ?
