Vous trouverez ci-dessous les corrections des cellules du [notebook suivant](./boyer.ipynb) que vous pouvez ouvrir avec **Capytale** ou bien [notebook.basthon.fr](https://notebook.basthon.fr/?from={{config.site_url}}boyer.ipynb&extensions=admonitions,romd)

# Correction du notebook séquencé

Pour cette correction vous pouvez copier coller les cellules de codes directement dans le notebook.

## Question préliminaire sur les tranches

On vous demande ici d'utiliser les **tranches** et les indices pour récupérer divers mots.

```python
texte = "Python est un langage de programmation."

# Complétez les trous ci-dessous en utilisant les indices et les tranches

# Récupérez le premier mot en utilisant une tranche sur la chaîne texte
trou1 = texte[:6]

# Récupérez le mot langage
trou2 = texte[14:21]

# Récupérez la longueur du texte
trou3 = len(texte)

# Récupérez la lettre h
trou4 = texte[3]

# Assertions pour vérifier les réponses
assert trou1 == "Python"
assert trou2 == "langage"
assert trou3 == 39
assert trou4 == "h"

print("Toutes les assertions ont réussi. Bravo !")
```

Pour réussir cet exercice, il vous est conseillé de réécrire la chaîne de caractères avec les indices:


|0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|...|
|-|-|-|-|-|-|-|-|-|-|- |- |- | -|- |- |- |- |- |- |- |- | - |
|P|y|t|h|o|n| |e|s|t|  |u |n |  |l|a|n|g|a|g|e|  |de programmation.|


Ainsi, on voit que les tranches qui délimitent les portions demandées sont:

- `"Python`: `texte[0:6]` (on exclut le caractère n°6);
- `"langage`: `texte[14:19]` (on commence au `l` et on finit au n°19 exclus)
- `h` en position n°3.

## Manipulation de fichiers et d'indices

Trouver la taille du texte:

```python
print(len(stendhal))
```

La méthode `find` : Premier argument, le texte à rechercher, deuxième argument optionnel, l'indice de début où chercher.


```
print(stendhal.find('Julien trembla'))
```

Votre nom apparaît ?

```python
indice_prenom = stendhal.find('Catherine')
if indice_prenom != -1 :
    print("Je suis dans le roman !")
else:
    print("Le roman n'a aucun intérêt.")
```

### Nombre d'occurrences:

```python
def nbOccurences(texte,motif)
    compteur = 0 #Combien de fois motif apparait-il avant d'avoir lu le texte ?
    i = 0 #À quel indice motif peut-il apparaître en premier ?
    
    while i >= 0 and i < len(texte): # Tant que i est un indice valide (ni négatif, ni plus grand que le texte)
        occurence = texte.find(motif,i) 
        # occurence est l'indice auquel on a trouvé motif, dans texte, à partir de l'indice i, ou -1.
        if occurrence != -1:
            compteur += 1 # On augmente le compteur de 1
            i = occurence+len(motif) # i doit être positionné à la fin du motif débutant à occurence
        else: # on a pas trouvé d'occurence, il faut mettre à jour i.
            i = -1
    return compteur # On renvoit le nombre d'occurences de motif.
```

### Trouver une occurrence:

```python
def occurence(motif,texte,indice):
    '''indique s'il y a une occurence de la chaîne motif dans la chaîne texte à la position indice'''
    # Si l'indice n'est pas valide, on peut renvoyer faux directement.
    if indice < 0 or indice > len(texte) :
        return False
    
    # Sinon, on peut parcourir la chaine motif en parallèle de la chaine texte.
    for j in range(len(motif)):
        if texte[indice+j] != motif[j]:
            return False
    return True
```

### La recherche Naïve:

```python
def recherche_naif(motif,texte):
    '''renvoie l'indice de la première occurence de motif dans texte.'''
    
    for indice in range(len(texte)):
        if occurence(motif,texte,indice):
            return indice
    return -1
```

### Complexité temporelle

```python
debut = time.time()
recherche_naif(stendhal,"Julien trembla")
fin = time.time()
print(fin-debut)
```

## Partie Boyer Moore

### Implémentation de la table de décalage

```python
def table_position(motif):
    d = {}
    for i in range(len(motif)-1): # On ne prend pas en compte le dernier caractère.
        d[motif[i]] = i # On enregistre pour chaque caractère sa position
	# Ici on écrasera bien les positions précédentes, ce qui donne la plus à droite.
    return d

```

### Fonction decalage

```python
def decalage(table,j,c):
    if c in table:
        d = j - table[c] # Attention, différence entre j et le caractère dans la table, pas l'inverse.
        if d <= 0:
            d = 1
    else:
        d = j + 1
    return d

```

### Recherche de Boyer Moore

La correction vous est déjà donnée :slight_smile: