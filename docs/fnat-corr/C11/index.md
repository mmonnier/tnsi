# Graphes
 
## Activités 

### Le problème des ponts de Königsberg 

<!-- Mettre une vidéo de peertube-->

Voici une illustration du problème des sept points de Königsberg extrait de la vidéo précédente :
![Königsberg](https://fabricenativel.github.io/Terminale/images/C11/konigsberg.png){: width=560px .imgcentre}

On rappelle que le but du problème est de trouver un chemin qui permet de passer une seule et unique fois par chaque pont de la ville.

??? question "Faire quelques essais, que penser de ce problème ?"

#### Vers les graphes

!!! question "Sur papier" 
    On divise la ville en quatre zones notées **N** (nord de la ville), **S** (sud de la ville), **I** (île centrale) et **E** (est de la ville) comme illustré ci-dessous. 
    Faire un schéma en représentant chaque zone par un rond et un pont par un lien entre ces zones.

    ![Konigsberg2](https://fabricenativel.github.io/Terminale/images/C11/konigsberg2.png){: width=560px .imgcentre}

??? success "Une correction possible"
    Vous devriez obtenir un schéma similaire à  :
    <div class="centre">
    ```mermaid
    graph TD
    N(("N"))
    I(("I"))
    S(("S"))
    E(("E"))
    N-- 1 ---I
    N-- 2 ---I
    I-- 6 ---S
    I-- 7 ---S
    I-- 4 ---E
    N-- 3 ---E
    E-- 5 ---S
    ```
    </div>

??? question "Le schéma obtenu s'appelle un **graphe**, rechercher sur le *Web* comment se nomment les zones et les ponts dans le vocabulaire de la théorie des graphes."

??? question "Donner le **degré** de chaque sommet de ce graphe"

!!! tip "Rappel de cours"
    On pourra remarquer que les arbres sont des graphes particuliers et se rappeler de la notion de degré (ou arité) d'un noeud dans un arbre.

#### Preuve mathématique

??? question "Supposons qu'une solution au problème qui commence et finit dans la même zone existe, en déduire une propriété du degré de chaque sommet."

??? tip "Pour avancer"
    Raisonner sur le fait que si on entre dans une zone, il faut pouvoir en sortir par un pont non encore utilisé.

??? question "Même question si on suppose que la solution commence et finit dans deux zones différentes."
    
??? tip "Pour avancer
    Traiter séparément des autres zones, celle de départ et d'arrivée.

#### Pour aller plus loin ...
    
??? question "En supprimant un seul pont, est-il possible d'obtenir une solution au problème ? Si oui lequel ?"

??? question "Même question en construisant un nouveau pont."


### Vocabulaire sur les graphes

!!! question "Votre graphe"
    En vous aidant de vos connaissances sur la géographie de l'europe (et si besoin du *Web*), dessiner le graphe représentant les frontières de l'europe de l'ouest.
    On fera figurer les pays suivants comme sommet du graphe : Allemagne, Belgique, Espagne, France, Grèce, Italie, Luxembourg, Norvège, Portugal,Suisse.
    On mettra un trait entre deux pays si et seulement si ils ont une frontière commune.

??? question "Rechercher dans votre cours la signification des *voisins* d'un sommet d'un graphe. Donner la liste des voisins de la France."

??? question "On suppose qu'on rajoute le Royaume Uni comme sommet et que la Manche **n'est pas une frontière**. Quelle propriété du graphe n'est plus valide ?"

??? question "On suppose qu'on considère maintenant aussi **toutes** les frontières maritimes. Que dire de ce nouveau graphe en terme d'arêtes ?"

??? question "On suppose que certaines frontières sont désormais à sens unique (FR -> UK par exemple). Que dire de ce nouveau graphe ?"

??? question "On suppose maintenant que sur le graphe initial, on fait figurer le temps de trajet entre les capitales. Comment s'appelle ce type de graphe ?"


### Implémentation avec matrice d'adjacence

#### Principe de l'implémentation  
On prend l'exemple du graphe orienté suivant à 4 noeuds :
<div class="centre">
```mermaid
graph LR
A(("A"))
B(("B"))
C(("C"))
D(("D"))
A --> B
A --> C
B --> C
B --> D
C --> D
```
</div>
        
!!! question ""
    Recopier et compléter le tableau suivant dans lequel les lignes et les colonnes représentent les sommets et dans lequel on indique par un **1** la présence d'une arête allant du sommet de la ligne vers celui de la colonne et par **0** son absence
    ![Matrice1](https://fabricenativel.github.io/Terminale/images/C11/matrice1.png){: width=240px .imgcentre}

!!! note ""
    Si on numérote les sommets du graphe (A le numéro 1, B le numéro 2, ...), il n'est plus nécessaire d'indiquer les noms des sommets sur les lignes et les colonnes.
    
De façon générale, une **matrice** en mathématiques est un tableau de nombres, ici, on a donc représenté notre graphe par une matrice appelé **matrice d'adjacence** de ce graphe :

$$
\begin{pmatrix}
0 & 1 & 1 & 0 \\\
\dots & \dots  & \dots & \dots \\\
\dots & \dots  & \dots & \dots \\\
\dots & \dots  & \dots & \dots \\\
\dots & \dots  & \dots & \dots \\\
\end{pmatrix}
$$

En nommant les sommets $S_1, S_2, S_3$ et $S_4$, dessiner le graphe dont la matrice d'adjacence est :
    
$$
\begin{pmatrix}
0 & 0 & 1 & 1 \\\
1 & 0 & 0 & 0 \\\
1 & 0 & 0 & 0 \\\
1 & 1 & 1 & 0 \\\
\end{pmatrix}
$$
    
??? question "Que peut-on dire d'un graphe dont la matrice d'adjacence est symétrique par rapport à sa diagonale principale ?"

??? question "Proposer une méthode pour représenter un graphe pondéré par une matrice d'adjacence."

#### Implémentation en python  

On s'inspire de ce qui a été fait pour les arbres et on utilisera la **POO** pour représenter un graphe par sa matrice d'adjacence. Enfin, on suppose qu'on implémente des graphes orientés.

??? question "Quelle type de données de Python est souhaitable pour représenter les sommets ?"

??? question "Même question pour la matrice d'adjacence."

!!! question "Pour le constructeur de la classe Graphe, on propose de fournir uniquement les sommets et de créer l'objet graphe ayant sa matrice d'adjacence vide initialement. De plus on ajoute un attribut `taille` au graphe. Compléter le code ci-dessous :"
    ```python
    class Graphe:

        def __init__(self,sommets):
            self.sommets=sommets
            self.taille = len(......)
            self.matrice = .............
    ```

??? question "Poursuivre cette implémentation en ajoutant une méthode d'ajout d'une arête."

!!! tip "Astuce"
    * Cette méthode prend en paramètre l'origine et l'extrémité de l'arête à ajouter.
    * On pourra vérifier que l'origine et l'extrémité sont bien dans la liste de sommets et rechercher leur position grâce à la méthode `index` des listes de python.

??? question "Ajouter une méthode de suppression d'une arête"
??? question "Ajouter une méthode d'affichage de la matrice d'adjacence"
??? question "Écrire la méthode `voisins` qui prend en paramètre un sommet et renvoie la liste de ses voisins."

### Implémentation avec des listes d'adjacences

#### Principe de l'implémentation  

On reprend l'exemple du graphe orienté déjà utilisé à l'activité précédente 
<div class="centre">
```mermaid
graph LR
A(("A"))
B(("B"))
C(("C"))
D(("D"))
A --> B
A --> C
B --> C
B --> D
C --> D
```
</div>

!!! question "Compléter le schéma suivant où on a fait figurer à côté de chaque sommet la liste des sommets adjacents :"
    * `A : B,C`
    * `B : ...`
    * `C : ...`
    * `. : ...`

!!! question "Dessiner le graphe dont la représentation par liste d'adjacence est :"
    * `R : S`
    * `S : R,T,U,V`
    * `T : V`
    * `U :`
    * `V : R,U`

#### Implémentation en Python  

On donne ci-dessous le constructeur d'une classe `Graphe` qui implémente les graphes sous la forme de listes d'adjacence :

```python
class Graphe:

    def __init__(self,sommets):
        self.taille = len(sommets)
        self.listes = {}
        for s in sommets:
            self.listes[s]=[]
```

??? question "Quel est le type de l'attribut `listes` d'un objet de la classe `Graphe` ?"

??? question "On suppose qu'on crée un objet de la classe `Graphe` en donnant en paramètre la liste  `["A","B","C","D"]`. Quel est alors le contenu de l'attribut `listes` de cet objet ?"

??? question "Poursuivre cette implémentation en ajoutant une méthode d'ajout d'une arête."

??? question "Ajouter une méthode de suppression d'une arête."

??? question "Proposer une méthode permettant d'ajouter un sommet."

??? question "Proposer une méthode permettant de supprimer un sommet."
    
??? question "Écrire la méthode `voisins` qui prend en paramètre un sommet et renvoie la liste de ses voisins."

### Parcours d'un graphe

#### Visualisation d'un parcours *depth first search*  

Un [outil en ligne](https://workshape.github.io/visual-graph-algorithms/#dfs-visualisation){target=_blank}, permet de visualiser le résultat du parcours en profondeur d'un graphe. Un graphe est donné en exemple, mais vous pouvez le modifier ou construire le votre :

[![dfs](https://fabricenativel.github.io/Terminale/images/C11/dfs.png){: width=600px .imgcentre}](https://workshape.github.io/visual-graph-algorithms/#dfs-visualisation){target=_blank}

!!! warning "Dans les menus déroulants, bien choisir Algorithme : **DFS** et Example graph : **directedGraph**"

#### Visualisation d'un parcours *breadth first search*  

Ce même [outil en ligne](https://workshape.github.io/visual-graph-algorithms/#bfs-visualisation){target=_blank}, permet de visualiser le résultat du parcours en largeur d'un graphe. Un graphe est donné en exemple, mais vous pouvez le modifier ou construire le votre :
[![dfs](https://fabricenativel.github.io/Terminale/images/C11/bfs.png){: width=600px .imgcentre}](https://workshape.github.io/visual-graph-algorithms/#bfs-visualisation){target=_blank}

!!! warning "Dans les menus déroulants, bien choisir Alorithme : **BFS** et Example graph : **directedGraph**"

#### À vous

On considère le graphe suivant :
<div class="centre">
    ```mermaid
    graph LR
    A(("A"))
    B(("B"))
    C(("C"))
    D(("D"))
    E(("E"))
    F(("F"))
    G(("G"))
    A --> B
    A --> C
    B --> D
    C --> F
    C --> G
    E --> G
    A --> E
    D --> F
    G --> F
    B --> C
    ```
</div>

??? question "Prévoir l'ordre de parcours pour un parcours en profondeur en commençant par le sommet `A`. Vérifier en testant dans l'outil en ligne."

??? question "Même question pour un parcours en largeur."


## Exercices

### Vocabulaire sur les graphes

On considère le graphe suivant :
<div class="centre">
```mermaid
graph LR
A --- B & C
C --- F & G & H
E --- G
G --- F
B --- E & D
D --- C
```
</div>

??? question "Ce graphe est-il orienté ? simple ? complet ? pondéré ?"
    Oui, oui, non, non.
??? question "Donner la liste des voisins de `C`."
    `F,G,H`
??? question "Quel est le degré de `G` ?"
    `G` a un degré sortant de 1, un degré entrant de 2, `G` a donc un degré de 3.
??? question "Quels sont les sommets adjacents à `A` ?"
    Les sommets successeurs de `A` sont `B` et `C`, il n'a aucun prédecesseur, donc seulement `B` et `C` .
### Graphe Complet

??? question "Rappeler la définition d'un graphe *complet*"
    Un graphe complet est un graphe tel que pour tout couple de sommet il existe une arête les reliant.
    
??? question "Dessiner un graphe complet à cinq noeuds."
    <div class="centre">
    ```mermaid
    graph LR
    A --- B & C & D & E
    B --- A & C & D & E
    C --- A & B & D & E
    D --- A & B & C & E
    E --- A & B & C & D
    ```
    </div>

??? question "Combien d'arêtes possède ce graphe ?"
    Il en possède $0+1+2+3+4 = 10$ s'il est non dirigé, le double sinon.

??? question "Donner la matrice d'adjacence de ce graphe."

??? question "Quel est le nombre d'arêtes d'un graphe complet à $n$ noeuds ?"
    Le graphe complet à $n$ noeuds possède: $0+1+ \dots + (n-1) = \frac{(n-1) \times n}{2}$ arêtes.

!!! tip ""
    On pourra utiliser sans avoir à le prouver que :
    $$ 1 + 2 + \dots + n = \dfrac{n(n+1)}{2} $$

### Représentation par matrice d'adjacence

!!! question "Donner la matrice d'adjacence du graphe suivant :"
    <div class="centre">
    ```mermaid
    graph LR
    A(("A"))
    B(("B"))
    C(("C"))
    D(("D"))
    E(("E"))
    A --- B & C
    C --- E & D
    D --- E
    B --- C
    ```
    </div>

!!! question "Dessiner le graphe dont la matrice d'adjacence est :"
    $$\begin{pmatrix}
    0 & 1 & 1 & 0 & 0 \\\
    0 & 0 & 1 & 0 & 1 \\\
    1 & 1 & 0 & 0 & 0 \\\
    0 & 1 & 1 & 0 & 0 \\\
    0 & 1 & 1 & 0 & 0 \\\
    \end{pmatrix}
    $$

### Représentation par listes d'adjacence

!!! question "Donner la représentation sous forme de listes d'adjacences du graphe suivant :"
    <div class="centre">
    ```mermaid
    graph LR
    A(("A"))
    B(("B"))
    C(("C"))
    D(("D"))
    E(("E"))
    A --- B & C
    C --- E & D
    D --- E
    B --- C
    ```
    </div>

??? success "Correction"
    * `A: [B,C]`
    * `B: [C]`
    * `C: [D,E]`
    * `D: [E]`

!!! question "Dessiner le graphe dont la représentation sous forme de listes d'adjacence est :"
    * `A : [B]`
    * `B : [C,D,E]`
    * `C : [F]`
    * `D : [F]`
    * `E : [F]`
??? success "Correction"
    <div class="centre">
        ```mermaid
        graph LR
        A(("A"))
        B(("B"))
        C(("C"))
        D(("D"))
        E(("E"))
        A --- B
        B --- C & D & E
        C --- F
        D --- F
        E --- F
        ```
    </div>




### Parcours d'un graphe

On considère le graphe suivant :
<div class="centre">
    ```mermaid
    graph LR
    A(("A"))
    B(("B"))
    C(("C"))
    D(("D"))
    E(("E"))
    F(("F"))
    G(("G"))
    A --> B
    A --> C
    G --> E
    C --> D
    D --> F
    B --> E
    E --> F
    A --> G
    ```
</div>

??? question "Donner l'ordre de parcours des sommets pour un parcours en largeur en partant de A"
    `A,B,C,G,E,D,F`
??? question "Même question pour un parcours en profondeur"
    `A,B,E,F,C,D,G`

### Implémentation par matrice d'adjacence

Commencez par récupérer, et revoir l'implémentation des graphes réalisée en cours.

!!! question "Utiliser cette implémentation pour créer le graphe de sommets `A,B,C,D,E` et dont la matrice d'adjacence est :"
    $$
    \begin{pmatrix}
    0 & 1 & 1 & 0 & 0 \\\
    0 & 0 & 1 & 0 & 0 \\\
    0 & 0 & 0 & 1 & 1 \\\
    0 & 0 & 0 & 0 & 0 \\\
    0 & 0 & 0 & 0 & 0 \\\
    \end{pmatrix}
    $$

!!! question "Ajouter la méthode `parcours_largeur` ci-dessous à cette implémentation en la complétant."
    ```python
    def parcours_largeur(self,depart):
            assert depart in self.sommets
            a_traiter = [depart]
            deja_vu = [depart]
            pl  = []
            while a_traiter != []:
                sommet = a_traiter[0]
                voisins = self.get_voisin(sommet)
                # Ajout des sommets voisins non encore parcourus à ceux à traiter
                for v in .......:
                    if v not in .....:
                        a_traiter......(v)
                        deja_vu.......(v)
                pl.append(sommet)
                a_traiter.....(0)
            return pl
    ```

??? question "Reconnaître la structure de données utilisée pour la variable `a_traiter`, expliquer pourquoi le choix d'une liste n'est pas judicieux."

!!! question "Ajouter la méthode `parcours_profondeur` ci-dessous à cette implémentation en la complétant."
    ```python
    def parcours_profondeur(self,start,parcourus=None):
            if parcourus == None:
                parcourus = []
            parcourus.append(start)
            for v in self...........(start):
                if v not in parcourus:
                    self............(v,parcourus)
            return parcourus
    ```

??? question "Proposer une méthode permettant d'ajouter un sommet."

!!! tip ""
    Penser aux conséquences pour la matrice d'adjacence

??? question "Proposer une méthode permettant de supprimer un sommet."

### Implémentation par listes d'adjacence

Reprendre les questions de l'exercice précédent avec l'implémentation par liste d'adjacence.