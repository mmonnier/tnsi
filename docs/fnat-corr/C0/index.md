# Révisions

## Activités 

### Ligne de commande

Le but de cette activité est de redécouvrir les bases de la ligne de commande. On utilisera [gameshell](https://github.com/phyver/GameShell){target=_blank}, un mini-jeu d'aventure où les commandes servent à accomplir des missions.

On en trouvera une correction un jour ...

### Module `turtle` de Python

Le but de cette activité est de redécouvrir les bases de la programmation en python en utilisant le module `turtle` qui permet de dessiner à l'aide d'une "*tortue*" (équivalente à un crayon) à laquelle on donne des instructions (se déplacer, avancer, tourner, ...) de façon à former le dessin désiré.
Cette tortue se déplace sur un écran (équivalent au papier), doté d'un repère comme en mathématiques.

#### Dessiner une grille de morpion

!!! success "Expliquer le rôle des instructions suivantes"
    On peut ici utiliser intensément la commande `help` de python ou bien la documentation: <https://docs.python.org/fr/3/library/turtle.html>

    | Commande | Rôle | Exemple |
    | --- | --- | --- |
    | `pensize` | Obtenir ou modifier la taille du stylo | `crayon.pensize(10)` |
    | `color` | Obtenir ou modifier la couleur du stylo | `crayon.color('darkred')` |
    | `penup` | Permet de lever le stylo pour le déplacer sans dessiner |`crayon.penup()`|
    | `pendown` | Permet de poser le stylo pour dessiner |`crayon.pendown()`|
    | `goto` | Permet de déplacer le stylo vers une position repérée par des coordonnées |`crayon.goto(0,0)`|
    | `forward` | Permet de déplacer le stylo d'une distance en ligne droite |`crayon.forward(10)`|
    | `setheading` | Permet de changer l'orientation du stylo |`crayon.setheading(90)`|

!!! success "Compléter ce programme en traçant les deux traits horizontaux."
     ```python hl_lines="26-30"
        import turtle

        # Création du "papier" et du "crayon"
        crayon = turtle.Turtle()
        papier = turtle.Screen()

        # Taille, dimension et couleur pour le papier et le crayon
        papier.bgcolor("beige")
        papier.setup(width=500,height=500)
        crayon.color("navy")
        crayon.pensize(5)

        # Tracé d'un trait avec les coordonnées des extrémités
        crayon.penup()
        crayon.goto(-50,-150)
        crayon.pendown()
        crayon.goto(-50,150)

        # Tracé d'un trait en orientant et en faisant avancer la tortue
        crayon.penup()
        crayon.goto(50,-150)
        crayon.pendown()
        crayon.setheading(90)
        crayon.forward(300)

        # Tracé des deux traits horizontaux en utilisant les goto
        crayon.penup()
        crayon.goto(-150,-50)
        crayon.pendown()
        crayon.goto(150,-50)

        crayon.penup()
        crayon.goto(-150,50)
        crayon.pendown()
        crayon.goto(150,50)

        # Attends un clic pour fermer la fenêtre de dessin
        papier.exitonclick()
     ```

!!! success "Dessiner un cercle au centre de la grille de morpion (de rayon 40, de couleur `darkred` avec un crayon d'épaisseur 7)"
    !!! success "Aide"
        Utiliser `circle(r)` où `r` est le rayon du cercle à tracer, on fera attention que le centre du cercle se situe toujours à *gauche* de l'orientation de la tortue et à une distance `r`.
    
    ```python
        ...
        crayon.penup()
        crayon.goto(0,40)
        crayon.setheading(90)
        crayon.pendown()
        crayon.circle(40)
    ```

### De l'utilité des fonctions

!!! danger "Attention"
    Cette activité est la suite de la précédente, on doit donc déjà disposer d'un programme Python permettant de tracer la grille de morpion ainsi que le cercle central.

On propose d'écrire une fonction `ligne` permettant de tracer avec la tortue `crayon` un trait en donnant les coordonnées `x1` et `y1` de l'origine et `x2` et `y2` de l'extrémité.

!!! success "Par quel mot clé commence la définition d'une fonction en Python ?"
    `def` suivi du nom de la fonction

!!! success "Quels seront ici les arguments de la fonction ?"
    `x1,y1,x2,y2` c'est à dire les coordonnées des points de départ et d'arrivée.

!!! success "Recopier et compléter le code de cette fonction :"
    ``` python
    def ligne(x1,y1,x2,y2):
        crayon.penup()
        crayon.goto(x1,y1)
        crayon.pendown()
        crayon.goto(x2,y2)
    ```
    
!!! success "Ajouter une chaîne de documentation à cette fonction"
    ```python
    def ligne(x1,y1,x2,y2):
        '''
        Trace une ligne partant du point (x1,y1) et arrivant au point (x2,y2)
        >>> ligne(0,0,10,20)
        '''
        crayon.penup()
        crayon.goto(x1,y1)
        crayon.pendown()
        crayon.goto(x2,y2)
    ```

!!! success "Faire le tracé de la grille de morpion en vous aidant de cette fonction."
    ```python
    ligne(-150,-50,150,-50)
    ligne(-150,50,150,50)
    ligne(-50,-150,-50,150)
    ligne(50,-150,50,150)
    ```

!!! success "Que peut-on dire par rapport à la version du programme qui n'utilisait pas de fonction ?"
    Celle ci est plus courte et plus compréhensible, les bugs sont plus simples à corriger.

!!! success "Écrire une fonction `ligne2` permettant de tracer un trait en donnant les coordonnées `x` et `y` de son origine, ainsi que sa longueur `l` et sa direction `d` (sous la forme d'un angle)."
    ```python
    def ligne2(x,y,l,d):
        '''
        Trace une ligne partant de (x,y) de longueur l et de direction d.
        >>> ligne2(0,0,10,90)
        '''
        crayon.penup()
        crayon.goto(x,y)
        crayon.setheading(d)
        crayon.pendown()
        crayon.forward(l)
    ```
!!! success "Écrire une fonction `cercle` permettant de tracer un cercle dont on donne les coordonnées du centre `x` et `y` et le rayon `r`"
    ```python
    def cercle(x,y,r):
        '''
        Trace un cercle de centre (x,y) et de rayon r.
        >>> cercle(0,0,40)
        '''
        crayon.penup()
        crayon.goto(x,y+r)
        crayon.setheading(90)
        crayon.pendown()
        crayon.circle(r)
    ```
!!! success "Écrire une fonction `croix` qui permet de tracer une croix en donnant son centre et la longueur des branches."
    ```python
    def croix(x,y,l):
        '''
        Trace un croix centrée en (x,y) avec deux branches de longueur l
        '''
        ligne2(x,y,0+45,l//2)
        ligne2(x,y,90+45,l//2)
        ligne2(x,y,180+45,l//2)
        ligne2(x,y,270+45,l//2)
    ```

### Une boucle pour répéter

On souhaite dessiner la grille suivante à l'aide du module `turtle` de Python : 

![grille](./images/grille.png)

On dispose déjà d'un début de programme qui définit les propriétés du papier et du crayon ainsi que  la fonction `ligne` permettant de tracer une ligne en donnant les deux extrémités (voir activités précédentes) :

```python
    import turtle

    # Création du "papier" et du "crayon"
    crayon = turtle.Turtle()
    papier = turtle.Screen()
    # Taille, dimension et couleur pour le papier et le crayon
    papier.bgcolor("beige")
    papier.setup(width=500,height=500)
    crayon.color("navy")
    crayon.pensize(5)

    def ligne(x1,y1,x2,y2):
        crayon.penup()
        crayon.goto(x1,y1)
        crayon.pendown()
        crayon.goto(x2,y2)
```

!!! success "Écrire les instructions permettant de tracer les lignes horizontales."
    ```python
    ligne(-200,-200,200,-200)
    ligne(-200,-150,200,-150)
    ligne(-200,-100,200,-100)
    ligne(-200,-50,200,-50)
    ligne(-200,0,200,0)
    ligne(-200,50,200,50)
    ligne(-200,100,200,100)
    ligne(-200,150,200,150)
    ligne(-200,200,200,200)
    ```

#### Une (bien) meilleure solution
    
!!! success "Vérifier que les instructions suivantes permettent de tracer les lignes verticales :"
    ```python
    for abscisse in range(-200,250,50):
        ligne(abscisse,-200,abscisse,200)
    ```

!!! success "Quelles sont les valeurs prises successives prises par la variable `abscisse` dans le programme précédent ?"
    En ajoutant un `print(abscisse)` au début de la boucle for on peut voir les valeurs: $-200,-150,-100,-50,0,50,100,150,200$ qui sont les mêmes que celles écrites à la main pour les ordonnées des points des lignes horizontales.

!!! success "Rappeler le rôle des paramètres de `range`."
    Ici on utilise la forme du `range` à trois arguments:

    1. Il s'agit de la valeur de départ de la variable d'itération;
    2. il s'agit de la valeur finale **exclue**.
    3. il s'agit du **pas** c'est à dire de la valeur qui sera additionnée à la variable d'itération à chaque tour de boucle.

### Instructions conditionnelles

On souhaite dessiner la figure suivante à l'aide du module `turtle` de Python : 

![carres](./images/carres.png)

!!! success "Écrire une fonction `carre(x,y,c)` qui trace le carré de côté `c` dont le coin inférieur gauche a pour coordonnées `(x,y)`."
    ```python
    def carre(x,y,c):
        '''
        Trace un carré avec son coin inférieur gauche en $(x,y)$ et de coté $c$
        '''
        ligne(x,y,x+c,y)
        ligne(x+c,y,x+c,y+c)
        ligne(x+c,y+c,x,y+c)
        ligne(x,y+c,x,y)
    ```
!!! success "Écrire une boucle à l'aide d'une instruction `for ... in range(....):` de façon à tracer la suite de carrés bleu."
    ```python
    for coord in range(-200,200,50):
        carre(coord,coord,40)
    ```

!!! success "Ajouter une instruction conditionnelle dans la boucle de façon à ce que le septième carré soit tracé en rouge et avec un crayon plus épais comme sur la figure."
    ```python hl_lines="2-7"
    for coord in range(-200,200,50):
        if coord == 100:
            crayon.color('darkred')
            crayon.pensize(7)
        else:
            crayon.color('navy')
            crayon.pensize(5)
        carre(coord,coord,40)
    ```
    
### Le problème de Josephus

<div class="centre"><iframe width="560" height="315" src="https://www.youtube.com/embed/uCsD3ZGzMgE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

Le but de l'activité est d'écrire un programme permettant de résoudre le [problème de Joséphus](https://fr.wikipedia.org/wiki/Probl%C3%A8me_de_Jos%C3%A8phe){target=_blank} en révisant les listes de Python.


On représente un cercle de `n` soldats par la liste `[1,2,...,n]`

!!! success "Écrire une fonction `soldats(n)` qui renvoie la liste `[1,2,....,n]`"
    ```python
    def soldats(n):
        return [i for i in range(1,n+1)]
    ```

!!! success "Vérifier que `n` est bien un entier strictement positif à l'aide d'instruction `assert`"
    ```python hl_lines="2"
    def soldats(n):
        assert n > 0
        return [i for i in range(1,n+1)]
    ```
!!! success "Ajouter une chaîne documentation."
    ```python hl_lines="2-4"
    def soldats(n):
        '''
        Renvoie la liste [1,2,...,n] si et seulement si n est un entier strictement positif.
        '''
        assert n > 0
        return [i for i in range(1,n+1)]
    ```

Afin de repérer l'épée, on décide que le soldat qui la tient se situe *toujours en première position de la liste*.

!!! success "Compléter l'évolution de la liste de soldat ci-dessous."
    | État de la liste | Explications |
    |------------------|--------------|
    |[==1==,~~2~~,3,4,5,6] | 1 élimine 2` et passe l'épée à 3 qui passe donc en tête de liste |
    |[==3==,~~4~~,5,6,1]  | 3 élimine 4 et passe l'épée à 5 qui passe donc en tête de liste |
    |[==5==,~~6~~,1,3]  | 5 élimine 6 et passe l'épée à 1 qui passe donc en tête de liste |
    |[==1==,~~3~~,5] | 1 élimine 3 et passe l'épée à 5 qui passe donc en tête de liste |
    |[==5==,~~1~~] | 5 élimine 1 et n'a plus personne à qui passer l'épée |
    |[==5==] | 5 est le dernier élément non éliminé |
    
!!! success "Compléter l'algorithme suivant d'évolution de la liste et indiquer les instructions Python correspondantes (on désigne par `cercle` la liste représentant le cercle de soldats)."
    |Étapes | Opération sur la liste | Instructions Python |
    |-|------------------------|---------------------|
    |1| Récupérer le premier élément et l'enlever de la file|`tueur=cercle.pop(0)`
    |2| Ajouter cet élément en fin de liste|`cercle.append(tueur)`|
    |3| Supprimer le premier élément |`cercle.pop(0)`|

!!! success "Quel est la condition d'arrêt de l'algorithme ?"
    Cet algorithme continue tant qu'il y a un tueur et une victime, c'est à dire tant qu'il y a au moins deux éléments.

!!! success "Exprimer cette condition par un test en python sur `cercle`"
    On peut utiliser le test:
    ```python
    len(cercle) >= 2
    ```

!!! success "Programmer une fonction `josephus(n)` qui renvoie le soldat survivant pour un cercle de `n` soldats."
    ```python
    def josephus(n):
        '''
        Renvoie le soldat survivant dans un cercle de longueur n
        '''
        while len(cercle) >= 2:
            tueur = cercle.pop(0)
            cercle.append(tueur)
            cercle.pop(0)
        return cercle[0]
    ```


## Exercices


### Les bases de la ligne de commande

1. En utilisant uniquement la  ligne de commande, créer l'arborescence suivante dans votre répertoire personnel :
        ```mermaid
            graph TD
            A[Cours] --> B[C0-Révisions]
            A[Cours] --> G[C1-Récursivité]
            B --> C[Exercices]
            B --> D[Activités]
            B --> E[Notes]
            B --> F[Python]
        ```
2. Renommer le dossier `Cours` en `NSI`
3. Créer un fichier vide `exercice2.txt` dans le dossier `Exercices`

!!! success "1."
    ```bash
    mkdir Cours
    mkdir Cours/CO-Révisions Cours/C1-Récursivité
    cd Cours/CO-Révisions
    mkdir Exercices Activités Notes Python
    ```

!!! success "2."
    ```bash
    mv Cours NSI
    ```

!!! success "3."
    ```bash
    touch Cours/CO-Révisions/Exercices/exercice2.txt
    ```


### Quelques commandes

1. **Sans les tester**, écrire dans le fichier `exercice2.txt` crée à l'exercice précédent l'effet des commandes suivantes :
    * `cd ~`
    * `mkdir Partage`
    * `chmod a+rwx Partage`
    * `cd Partage`
    * `touch hello.txt`
    * `echo "Salut tout le monde" > hello.txt`
    * `cat hello.txt`

2. Taper ces commandes pour vérifier vos précisions.

!!! success "Correction"
    * `cd ~` change le dossier courant vers mon dossier personnel (`/home/<mon id>`)
    * `mkdir Partage` crée un dossier `Partage` dans le dossier courant (le dossier personnel).
    * `chmod a+rwx` donne à tout le monde les droits de lecture,écriture et exécution sur ce dossier.
    * `cd Partage` change le dossier courant vers le dossier `Partage`
    * `touch hello.txt` crée un fichier vide nommé `hello.txt` dans le dossier courant (`Partage`)
    * `echo "Salut tout le monde" > hello.txt` écrit le texte `Salut tout le monde` dans le fichier `hello.txt`
    * `cat hello.txt` affiche le contenu du fichier `hello.txt` rempli juste avant.

### Arborescence

1. Rechercher l'aide de la commande `tree`, quel est l'effet de cette commande ?
2. Afficher l'arborescence de votre répertoire personnel
3. Afficher l'arborescence de la racine *en limitant à un la profondeur*
4. Rechercher sur le *Web* le rôle des dossiers suivants :
    * `/etc`
    * `/home`
    * `/dev`
    * `/tmp`

!!! success "1."
    Cette commande affiche le contenu du dossier courant et de tout ses sous dossiers sous forme d'arborescence, c'est à dire en montrant les liens entre eux.

!!! success "2."
    On utilise la commande `tree ~` ou `tree` si on est déjà dans le dossier personnel.

!!! success "3."
    On utilise la commande `tree -L 1 /`.

!!! success "4."
    * `/etc` contient les fichiers de configuration du système et les préférences systèmes des logiciels;
    * `/home` contient les répertoires personnels des personnes utilisant le système, sauf le super-administrateur;
    * `/dev` contient les *pseudo-fichiers* des périphériques reliés à la machine;
    * `/tmp` contient les fichiers temporaires créés par les logiciels ou les personnes.

!!! Aide
    Pour les exercices avec `turtle`, on peut consulter [la page de documentation officielle du module](https://docs.python.org/fr/3/library/turtle.html){target=_blank}

### Figures géométriques avec Turtle

1. Écrire une fonction `rectangle(x,y,l1,l2)` qui trace le rectangle de dimensions `l1` $\times$ `l2` et dont le coin inférieur gauche à pour coordonnées `x` et `y`.
2. On peut remplir une surface construite avec un `crayon` du module `turtle` :
    * Spécifier une couleur de remplissage par exemple `crayon.fillcolor('red')`
    * Au début du tracé de la figure écrire l'instruction `crayon.begin_fill()`
    * A la fin du tracé de la figure écrire l'instruction `crayon.end_fill()`

    Modifier votre fonction rectangle de façon à pouvoir tracer un rectangle rempli avec une couleur passée en paramètre.

!!! success "Correction"
    ```python
    def rectangle(x,y,l1,l2):
        '''
        Trace un rectangle avec son coin inférieur gauche en (x,y) et de cotés l1 (largeur) x l2 (hauteur)
        '''
        crayon.penup()
        crayon.goto(x,y)
        crayon.pendown()
        crayon.setheading(0)
        for loop in range(2):
            crayon.forward(l1)
            crayon.left(90)
            crayon.forward(l2)
            crayon.left(90)

    def rectangle_colore(x,y,l1,l2,couleur):
        '''
        Trace un rectangle avec son coin inférieur gauche en (x,y) et de cotés l1 (largeur) x l2 (hauteur) et rempli de la couleur donnée.
        '''
        crayon.penup()
        crayon.goto(x,y)
        crayon.pendown()
        crayon.setheading(0)

        crayon.fillcolor(couleur)
        crayon.begin_fill()

        for loop in range(2):
            crayon.forward(l1)
            crayon.left(90)
            crayon.forward(l2)
            crayon.left(90)

        crayon.end_fill()
    ```

### Quelques figures avec `turtle`

Construire les figures suivantes (le repère est là pour vous aider et ne doit pas être reproduit):

1. L'escalier
![escalier](images/escalier.png)


2. Cercles concentriques (les couleurs alternent entre `blue` et `lightblue`, le crayon a une épaisseur de 10, les cercles ont pour rayon 10,20,30, ...)
![cercles](images/cercles.png){: .imgcentre}

!!! success "1."
    ```python
    crayon.setheading(0)
    for loop in range(...):
        crayon.forward(...)
        crayon.left(90)
        crayon.forward(...)
        crayon.right(90)
    ```

!!! success "2."
    ```python
    crayon.setheading(90)
    crayon.pensize(10)
    for loop in range(1,..):
        if loop % 2 == 0:
            crayon.color('blue')
        else:
            crayon.color('lightblue')
        cercle(0,0,loop*10)
    ```


### Pour réviser les listes
1. On considère le programme suivant :
    ```python
        liste1 = [0]*100
        liste2 = [0 for k in range(100)]
        liste3 = []
        for k in range(100):
            liste3.append(0)
    ```

    1. Quel est le contenu de chacune des listes ?
    2. Indiquer par quel procédé chacune de ces listes a été crée.

!!! success "1."
    Ces listes contiennent toutes 100 occurrences de l'entier 0.
!!! success "2." 
    Elles sont créées respectivement par: *répétition, compréhension et ajouts successifs*

2. Écrire un programme python permettant de créer les listes suivantes :
    1. Une liste contenant 12 fois le chiffre 7.
    2. La liste des nombres entiers de 1 à 100.
    3. Une liste contenant 1000 nombres tirés au sort entre 1 et 6. \\

        !!! aide 
            On rappelle que la fonction `randint` peut être importée depuis le module `random`, elle permet de tirer un nombre au hasard entre deux valeurs `a` et `b` donnés en paramètres.

    4. La liste des cubes des entiers de 1 à 10.

!!! success "Correction"
    ```python
    l1 = [7]*12
    l2 = [entier for entier in range(1,101)]
    
    from random import randint
    l3 = [randint(1,6) for loop in range(1000)]

    l4 = []
    for entier in range(1,11):
        l4.append(entier**3)
    ```

### Parcours de liste

1. Écrire une fonction `somme(l)` qui renvoie la somme des éléments de la liste `l`. Vérifier que tous les éléments de `l` sont biens des nombres entiers (`int`) ou flottants (`float`).
2. Écrire une fonction `indice(elt,l)` qui renvoie l'indice de la première apparition de `elt` si `elt` est dans `l` et $-1$ sinon.

    !!! Example "Exemples"
        * `indice(3,[1,2,3,5,7,11])` renvoie `2` puisque `3` est dans cette liste à l'indice 2.
        * `indice(13,[1,2,3,5,7,11])` renvoie `-1` puisque `13` n'est pas dans cette liste.

!!! success "1."
    ```python
    def somme(l):
        '''
        Renvoie la somme des éléments de l uniquement si ceux-ci sont des entiers ou des flottants
        '''
        assert all(type(x) == 'int' or type(x) == 'float' for x in l)
        somme_partielle = 0
        for element in l:
            somme_partielle = somme_partielle + element
        return somme_partielle
    ```

!!! success "2."
    ```python
    def indice(elt,l):
        '''
        Renvoie l'indice de la première occurrence de elt dans l, -1 si elt n'appartient pas à l.
        '''
        # On parcourt tout les indices possibles de l
        for indice in range(len(l)):
            if l[indice] == elt: # Si l'élément se trouve à l'indice actuel
                return indice # On renvoit l'indice actuel
        # On a pas trouvé l'élément dans la liste si on sort de la boucle
        return -1
    ```

### Polygone régulier

1. Écrire une fonction `triangle_equilateral(c)` qui trace un triangle équilatéral de côte `c` à partir de la position courante de la tortue.
2. Écrire une fonction `carre(c)` qui trace un carré de côte `c` à partir de la position courante de la tortue.
3. Écrire une fonction `polygone_regulier(n,c)` qui trace un polygone régulier de côte `c` à partir de la position courante de la tortue.
    
    !!! Rappel
 
        * Un polygone régulier est un polygone dont tous les côtés sont de la même longueur et tous les angles sont égaux.
        * Les angles d'un polygone régulier à $n$ côtés mesurent $\dfrac{360}{n}$

!!! success "1."
    ```python
    def triangle_equilateral(c):
        '''
        Trace un triangle équilatéral de coté c à la position actuelle
        '''
        for i in range(3):
            crayon.forward(c)
            crayon.left(120)
    ```

!!! success "2."
    ```python
    def carre(c):
        '''
        Trace un carré de coté c à la position actuelle
        '''
        for i in range(4):
            crayon.forward(c)
            crayon.left(90)
    ```

!!! success "3."
    ```python
    def polygone_régulier(n,c):
        '''
        Trace un polygone régulier de coté c à n cotés à la position actuelle
        '''
        angle = 360 / n
        for i in range(n):
            crayon.forward(c)
            crayon.left(angle)
    ```

