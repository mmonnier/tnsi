# Processus et Ordonnancement

## Activité 1 - Observer les processus

### Commande ps

!!! question "Consulter l'aide sur la commande ps, quel est le rôle de cette commande ?"
    La commande `ps` permet de lister les processus.

!!! question "À quoi correspond la colonne PID ?"
    PID veut dire: Process Identifier, c'est l'identifiant du processus


!!! question "ps -e / ps -f / ps -x"
    `ps -e` Affiche tout les processus du système et leur binaire associé.
    `ps -f` affiche toutes les informations, **PPID** pour Parent Process Identifier
    `ps -x` tout les processus du système


## Activité 2 - Dîner des Philosophes

## Exercices

### Exercice 1

### Exercice 2

### Exercice 3
