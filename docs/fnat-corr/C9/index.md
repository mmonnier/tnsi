# Schéma relationnel des bases de données

## Activités 

### Rappels de SQL

Commencer par télécharger et sauvegarder la base de données de livres suivante : [Livres](https://fabricenativel.github.io/Terminale/files/C9/livres.db)
Cette base provient de [pixees](https://pixees.fr/informatiquelycee/term/c2.html){target=_blank}

#### Quelques rappels sur les bases de données

Lancer SQLite et ouvrir la base de données `livres.db` (qui contient une unique table livres).

??? question "Rappeler la signification du vocabulaire suivant : *enregistrement*, *attribut*."
    Un **attribut** est une colonne d'une table. Il s'agit d'une propriété des individus.
	un **enregistrement** est une ligne. Il s'agit des invididus, c'est à dire des éléments de la relation.

??? question "Que sont les enregistrements dans le cas de cette table ?"
    Dans le cas de cette table un enregistrement est un livre.
	
??? question "De quel type est l'attribut `note` ? Proposer un domaine pour cet attribut."
	Cet attribut est un *entier*.
	Au vu des valeurs présentes, `note` est un entier entre 7 et 10, mais on peut déduire du nom qu'il est compris entre 0 et 10.

??? question "Rappeler le principe d'unicité, quel attribut sert de clé primaire ?"
	Ici la **clé primaire** est `id`.
	Dans une table, le principe d'unicité garantit qu'aucun enregistrement n'est présent plusieurs fois.
    
#### Quelques rappels sur le `sql`:
    
??? question "Écrire une requête permettant de lister tous les livres ayant obtenu une note de 10."
	`select titre from livres where note = 10;`

??? question "Écrire une requête permettant de lister les livres dans l'ordre alphabétique de leur titre."
	`select titre from livres order by titre`

??? question "Écrire une requête permettant de savoir combien d'auteurs différents apparaissent dans cette base de données."
	`select count(distinct concat(nom_auteur,prenom_auteur)) from livres`

??? question "Écrire une requête permettant de calculer la moyenne des notes attribuées aux livres écrits par René Barjavel."
	`select AVG(note) from livres where nom_auteur = "Barjavel" and prenom_auteur = "René"`

#### Du nouveau en `sql`

Nous n'avons [pour le moment](../C2/index.md) vu que l'instruction `select` qui permet de sélectionner des données, le langage `sql` permet aussi :

* d'ajouter des données dans une table avec `insert`,
* de modifier des données dans une table avec `update`,
* de supprimer des données dans une table avec `delete`.

À partir de vos propres recherches sur le web, trouvez la syntaxe des requêtes **d'insertion**, de **mise à jour** et de **suppression**, puis répondez aux trois questions suivantes.

??? tip "Mais où chercher ?"
	Je vous conseille le site <https://sql.sh> qui est une mine d'or sur la syntaxe de ce langage !
	
??? question "Ajouter dans cette table l'enregistrement correspondant au livre *"la planète des singes"* écrit par Pierre Boulle en francais et publié en 1963. On laisse pour le moment vide le champ `note`."
	```sql
	INSERT INTO livres(id,titre,nom_auteur,prenom_auteur,annee_publi,langue_auteur)
	VAlUES (17,"la planète des singes","Boulle","Pierre",1963,"français");
	```

??? tip "J'ai des erreurs !" 
	Avez vous pensé à mettre les virgules ?
	Avez vous respecté le type des attributs ? 
	Avez vous pensé à l'attribut `id` ?

??? question "Modifier l'enregistrement précédent en mettant une note de 10."
	```sql
	UPDATE livres
	SET note = 10
	WHERE id=17;
	```
??? tip "Je modifie plusieurs lignes !"
	Utiliser l'attribut `id` pour repérer l'enregistrement à modifier

??? question "Supprimez tout les livres écrits avant 1900."
	```sql
	DElETE FROM livres
	WHERE annee_publi < 1900
	```

#### Vers l'utilisation de plusieurs tables

On appelle *schéma relationnel* l'ensemble des relations (ou tables) présentes dans une base de données.
On représente généralement une table d'un schéma relationnel en indiquant chaque attribut, la clé primaire étant souligné.
Le schéma de notre base est donc :
**livres** (^^id^^, titre, nom_auteur, prenom_auteur, annee_auteur, langue_auteur, annee_publi, note)

Des représentation en tableau sont parfois plus lisibles :

![tableau](https://fabricenativel.github;io/Terminale/images/C9/sgbd1.png)

??? question "De nombreuses informations sont dupliquées dans notre base de données, lesquelles ?"
	Ce sont les informations sur les auteurs qui sont dupliquées inutilement.

??? question "Quels sont les inconvénients de cette duplication d'information ?"
	Pour modifier les informations d'un seul auteur, il faut potentiellement modifier tout les enregistrements.
	
On propose de créer une nouvelle table **auteurs** qui contiendra les informations sur les auteurs.

??? question "Proposer un schéma pour cette table."
	En se basant sur l'énoncé on peut proposer: **auteurs** (^^id_auteur^^, nom_auteur, prenom_auteur, annee_auteur, langue_auteur)
	
La table des livres s'en trouve allégée, on y enlève les informations concernant l'auteur et on ajoute un champ `id_auteur` qui référence un élément de la table des auteurs.
On dit que `id_auteur` est une *clef étrangère*, c'est à dire la clé primaire d'une autre table, dans le schéma relationnel on note ce champ en le faisant précédé d'un `#`.

??? question "Écrire le schéma relationnel de cette nouvelle base de données."
	On doit faire figurer les deux tables, les informations de clés primaires et étrangères.

	- **livres**(^^id^^,titre,#id_auteur,annee_publi,note)
	- **auteurs**(^^id_auteur^^,nom_auteur,prenom_auteur,annee_auteur,langue_auteur)
	

### Plusieurs tables

On reprend la base de données livres contenant maintenant deux tables et construite à l'activité précédente :[Livres avec table auteur](https://fabricenativel.github.io/Terminale/files/C9/livres_auteurs.db)
Le schéma relationnel de cette base de données est donné ci-dessous :

![sr](https://fabricenativel.github.io/Terminale/images/C9/sgbd2.png)

#### Intégrité référentielle

??? question "Rappeler rapidement la signification des attributs soulignés et du caractère `#`."
	Les attributs soulignés sont des **clés primaires**, ceux avec un `#` des clés étrangères (des clés primaires d'autres tables).

On considère la requête suivante :
```sql
DELETE FROM auteurs WHERE id_auteur=10
```

??? question "Quel devrait être l'effet de cette requête sur la base de données ? Tester cette requête, que se passe-t-il ?"
	Cette requête doit supprimer toutes les lignes de `auteurs` où l'attribut `id` vaut 10.
	On obtient une erreur car l'auteur 10 est référencé dans une autre table.

!!! question "Même question pour la requête  : "
    ```sql
    INSERT INTO  livres  VALUES (17,11,"Harry potter",1997,6)
    ```
??? success "Réponse"
	Cette requête insère un nouveau livre d'identifiant 17, écrit par l'auteur 11, son titre est Harry Potter, il a été publié en 1997 et a une note de 6.
	On obtient une erreur car l'auteur 11 n'existe pas.
	
Ces requêtes échouent car le **sgbd** préserve de façon automatique l'*intégrité référentielle* schématisée par la flèche dans le schéma relationnel ci-dessus. 

??? question "En faisant éventuellement des recherches sur le web, donner une définition de l'intégrité référentielle."
	L'intégrité référentielle garantit que toutes clés étrangères prend ses valeurs dans le domaine d'une clé primaire.
	C'est à dire, qu'on ne peut utiliser une valeur non existante, ni supprimer une valeur existante mais déjà référencée.

#### Requête dans plusieurs tables

On désire maintenant lister tous les titres de livres présents dans notre base de données accompagnés du nom de leur auteur.
On doit donc effectuer une requête *sur plusieurs tables* puisque les titres sont dans la table des **livres** et les noms d'auteurs dans la table **auteurs**

!!! danger "Attention !"
	Les attributs de deux tables peuvent avoir le même nom, par exemple ici `id_auteur` est un attribut de **livres** et aussi de  **auteurs**.
	On prendra donc l'habitude pour éviter toute ambiguïté de préfixer l'attribut par le nom de sa table.
	Ici, on a donc les attributs `livres.id_auteur` et `auteurs.id_auteur`. 

!!! question "Tester la requête suivante, dans laquelle on a préfixé les attributs par le nom des tables : "
	```sql
	SELECT livres.titre, auteurs.nom FROM livres,auteurs
	```
	Le résultat est-il celui attendu ?

??? success "Réponse"
	Non, on obtient tout les auteurs associés à tout les livres, mais pas seulement à ceux qu'ils ont écrits.
	
On doit effectuer ce qu'on appelle une *jointure* c'est à dire indiquer qu'on associe le livre et l'auteur lorsque le champ `id_auteur` des deux tables correspond. 
La syntaxe est la suivante : 

```sql
SELECT livres.titre, auteurs.nom 
FROM livres 
JOIN auteurs ON livres.id_auteur = auteurs.id_auteur;
```

??? question "Tester cette requête."
	Ici on obtient bien le résultat attendu.
	
??? question "Écrire une requête permettant de lister tous les titres de livres avec nom et prénom de l'auteur lorsque les auteurs sont nés en 1920."
	```sql
	SELECT livres.titre, auteurs.nom, auteurs.prenom
	FROM livres 
	JOIN auteurs ON livres.id_auteur = auteurs.id_auteur
	WHERE auteurs.annee = 1920
	```

#### A vous de jouer !   

Télécharger la base de données `bibliotheque` : [Ici](https://fabricenativel.github.io/Terminale/files/C9/bibliotheque.db).
On y trouve les deux tables des questions précédentes et deux nouvelles tables : **clients** et **emprunts**.

??? question "Donner le schéma relationnel de cette base de données en y faisant figurer les clés primaires et étrangères."
	- **livres** (^^id^^: int, titre: text, ...)
	- **auteurs** (^^id_auteur^^: int, nom: text, prenom: text, ...)
	- **clients** (^^id_client^^: int, nom : text, ...)
	- **emprunts** (^^id^^: int, `#`id_client : int, `#`id_livre)

??? tip "Comment faire ???"
	Faire le schéma comme au début de cette activité en y ajoutant les deux tables **clients** et **emprunts**.

??? question "Expliquer rapidement les relations préservées par l'intégrité référentielle de cette base de données en donnant un exemple de requête qui renverrait une erreur de type `foreign key  constraint failed`."
	L'intégrité référentielle préservent le fait qu'un emprunteur est un client et qu'il emprunte un livre.
	Une requête qui supprimerait un livre emprunté donnerait une erreur d'intégrité.

??? question "Ajouter un enregistrement de votre choix dans la table **clients**."
	```sql
	INSERT INTO clients(nom,prenom,email)
	VAlUES ("SuperNom","PrenomPasCOol","mail@monmail.fr");
	```

??? question "Ajouter un enregistrement pour le client de la question précédente dans la table des **emprunts**."
	```sql
	INSERT INTO emprunts(id_client,id_livre,date)
	VAlUES (5,4,'10/01/2025');
	```
??? question "Écrire une requête permettant de lister tous les emprunts en cours dans cette bibliothèque avec le nom de l'emprunteur et le titre du livre."
	```sql
	SElECT clients.nom, livre.titre
	FROM emprunts
	JOIN clients ON emprunts.id_client = clients.id_client
	JOIN livres ON emprunts.id_livre = livres.id
	```

## Exercices

### Exercices corrigés en ligne

Le site du [CNAM](http://deptfod.cnam.fr/bd/tp){target=_blank} propose de vous entraîner aux requêtes SQL sur quatre base de données (films, voyageurs, immeubles et messagerie). Pour chaque base, des requêtes sont suggérés et la correction est proposée.

Nicolas Reveret, professeur de NSI, a mis en place plusieurs activités en ligne pour expérimenter avec le langage SQL: <https://nreveret.forge.apps.education.fr/exercices_bdd/>.

### Sujets de baccalauréat

| Code        | Exercice | lien                                                                                     |
| ---         | ---      | ---                                                                                      |
| 24-NSIJ1AN1 | 3        | <https://mmonnier.forge.apps.education.fr/tnsi/Annales/24-NSIJ1AN1/#exercice-3-8-points> |
| 24-NSIJ2AN1 | 2        | <https://mmonnier.forge.apps.education.fr/tnsi/Annales/24-NSIJ2AN1/#exercice-2-6-points> |
| 24-NSIJ1G11 | 3        | <https://mmonnier.forge.apps.education.fr/tnsi/Annales/24-NSIJ1G11/#exercice-3-8-points> |
| 24-NSIJ2G11 | 3        | <https://mmonnier.forge.apps.education.fr/tnsi/Annales/24-NSIJ2G11/#exercice-3-8-points> |
| 24-NSIJ1JA1 | 3        | <https://mmonnier.forge.apps.education.fr/tnsi/Annales/24-NSIJ1JA1/#exercice-3-8-points> |
| 24-NSIJ2JA1 | 3        | <https://mmonnier.forge.apps.education.fr/tnsi/Annales/24-NSIJ2JA1/#exercice-3-8-points> |
| 24-NSIJ1ME1 | 2        | <https://mmonnier.forge.apps.education.fr/tnsi/Annales/24-NSIJ1ME1/#exercice-2-6-points> |

Avant 2024: <https://fabricenativel.github.io/index_annales/>