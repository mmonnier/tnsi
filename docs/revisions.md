---
author: Valérie Mousseaux, Jean-Louis Thirot et Mireille Coilhac
title: Ordonnancement
---
# Exercices de révisions

## I. L'ordonnancement

!!! abstract "Les différents algorithmes d'ordonnancement"

	* Le modèle **FIFO** : on affecte les processus dans l'ordre de leur apparition dans la file d'attente.
	* Le modèle **SJF** (Shortest Job First) : on affecte en premier le « plus court processus en premier » de la file d'attente à l'unité de calcul.
	* Le modèle **Round Robin** : (ou méthode du tourniquet) on effectue un bloc de chaque processus présent dans la file d'attente à tour de rôle, pendant un quantum de temps d'en général 20 à 30 ms. Si le processus n'est pas terminé, il repart en fin de liste d'attente.


???+ note "Élèments pour l'ordonnancement"

	* Durée du processus ou durée d'exécution sur le coeur : à la durée en quantum P nécessaire à l'execution du processus.
	* Date d'arrivée ou date de soumission : date où le processus arrive dans la file d'attente.
	* Date de terminaison: pour un processus P : durée écoulée entre le temps 0 et le temps où le processus est terminée P
	* Temps d'exécution ou temps de séjour : différence entre le temps d'arrivée de P et le temps de terminaison de P.
	* Temps d'attente d'un processus P : différence entre le temps d'execution et la durée du processus.
	* Temps moyen d'attente : moyenne des temps d'attente de tous les processus

???+ question "Exercice 1 comparaison des algorithmes"

	5 processus, P1, P2, P3, P4, P5 sont dans une file d'attente dans cet ordre (P1 est le premier, P5 est le dernier). Ils arrivent tous en même temps pour être traité. Leur exécution demande un temps total de service exprimé en unités arbitraires (quantum).

	|Processus|	P1|	P2|P3|P4|P5|	
	|:--|:--|:--|:--|:--|:--|
	|Durée en quantum|	10|	1|	2|1|5|


	* Décrire l'exécution des processus (schéma + tableau) dans le cadre des politiques d'ordonnancement FIFO, SJF, RR (avec un quantum de 1).
	* Quelle est de ces trois politiques, celle qui correspond à un temps minimal d'attente moyen par processus?

## II. SQL

*On pourra utiliser les mots clés SQL suivants :*

`AND`, `SELECT`, `FROM`, `WHERE`, `JOIN`, `INSERT INTO`, `VALUES`, `COUNT`, `ORDER BY`, `OR`, `ON`, `SET`, `UPDATE`.


On étudie une base de données permettant la gestion de l'organisation d'un festival de musique de jazz, dont voici le schéma relationnel comportant trois relations :


- la relation `groupes(PK: id_groupe, nom, style, nb_pers)`
- la relation `musiciens(PK: id_musicien, nom, prenom, instru,#id_groupe)`
- la relation `concerts(PK: id_concert, scene,heure_debut, heure_fin,#id_groupe)`

Dans ce schéma relationnel :

- les clés primaires sont préfixées par `PK` ;
- les clés étrangères sont précédées d'un #.
Ainsi `concerts.id_groupe` est une clé étrangère faisant référence à `groupes.id_groupe`.

Voici un extrait des tables **`groupes`**, **`musiciens`** et **`concerts`** :

!!! note "Extrait de **`groupes`**"
            
    | `id_groupe` |         `nom`          |     `style`     | `nb_pers` |
    | :---------: | :--------------------: | :-------------: | :-------: |
    |    `12`     |   `'Weather Report'`   | `'Jazz Fusion'` |    `5`    |
    |    `25`     |    `'The 3 Sounds'`    |  `'Soul Jazz'`  |    `4`    |
    |    `87`     | `'Return to Forever'`  | `'Jazz Fusion'` |    `8`    |
    |    `96`     | `'The Jazz Messenger'` |  `'Hard Bop'`   |    `3`    |

!!! note "Extrait de **`musiciens`**"

    | `id_musicien` |    `nom`    |  `prenom`   |      `instru`      | `id_groupe` |
    | :-----------: | :---------: | :---------: | :----------------: | :---------: |
    |     `12`      | `'Garrett'` |  `'Kenny'`  | `'saxophone alto'` |    `96`     |
    |     `13`      | `'Garrett'` |  `'Kenny'`  |     `'flute'`      |    `25`     |
    |     `58`      |  `'Corea'`  |  `'Chick'`  |     `'piano'`      |    `87`     |
    |     `97`      | `'Clarke'`  | `'Stanley'` |     `'basse'`      |    `87`     |


!!! note "Extrait de **`concerts`**"

    | `id_concert` | `scene` | `heure_debut` | `heure_fin` | `id_groupe` |
    | :----------: | :-----: | :-----------: | :---------: | :---------: |
    |     `10`     |   `1`   |  `'20 h 00'`  | `'20 h 45'` |    `12`     |
    |     `24`     |   `2`   |  `'20 h 00'`  | `'20 h 45'` |    `35`     |
    |     `36`     |   `1`   |  `'21 h 00'`  | `'22 h 00'` |    `96`     |
    |     `45`     |   `3`   |  `'18 h 00'`  | `'18 h 30'` |    `87`     |


**1.**  Citer les attributs de la table **`groupes`**.

**2.**  Justifier que l'attribut `nom` de la table **`musiciens`** ne peut pas être une clé primaire.

**3.** En s'appuyant uniquement sur l'extrait des tables fourni ci-dessus écrire ce que renvoie la requête :

```sql
SELECT nom
FROM groupes
WHERE style = 'Jazz Fusion';
```

**4.** Le concert dont l'`id_concert` est `36` finira à 22 h 30 au lieu de 22 h 00. 

Recopier sur la copie et compléter la requête SQL ci-dessous permettant de mettre à jour la relation **`concerts`** pour modifier l'horaire de fin de ce concert.

```sql
UPDATE concerts
SET ...
WHERE ... ;
```

**5.** Donner une seule requête SQL permettant de récupérer le nom de tous les groupes qui jouent sur la scène 1.

**6.** Fournir une seule requête SQL permettant d'ajouter dans la relation **`groupes`** le groupe `'Smooth Jazz Fourplay'`, de style `'Free Jazz'`, composé de 4 membres. Ce groupe aura un `id_groupe` de 15. 

Les données sont ensuite récupérées pour être analysées par la société qui produit les festivals de musique. Pour ce faire, elle utilise la programmation en Python afin d'effectuer certaines opérations plus complexes.

Elle stocke les données relatives aux musiciens sous forme d'un tableau de dictionnaires dans laquelle a été ajouté le nombre de concerts effectués par chaque musicien :

```python
>>> print(musiciens)
  [{'id_musicien': 12, 'nom': 'Garrett', 'prenom': 'Kenny',
    'instru': 'saxophone alto', 'id_groupe' : 96, 'nb_concerts': 5},
   {'id_musicien': 13, 'nom': 'Garrett', 'prenom': 'Kenny',
    'instru': 'flute', 'id_groupe' : 25, 'nb_concerts': 9},
   {'id_musicien': 58, 'nom': 'Corea', 'prenom': 'Chick',
    'instru': 'piano', 'id_groupe' : 87, 'nb_concerts': 4},
   {'id_musicien': 97, 'nom': 'Clarke', 'prenom': 'Stanley',
    'instru': 'basse', 'id_groupe' : 87, 'nb_concerts': 4},
   ...
  ]
```

**7.**  Écrire la fonction `recherche_nom` ayant pour unique paramètre un tableau de dictionnaires (comme `musiciens` présenté précédemment) renvoyant une liste contenant le nom de tous les musiciens ayant participé à au moins 4 concerts.

## III. Lecture de code

> D'après 2022, Asie, J2, Ex. 5

1. On considère la fonction `somme` qui reçoit en paramètre un entier `n` strictement positif et renvoie le résultat du calcul $1+\frac12+\frac13+\cdots+\frac1n$.

    ```python
    def somme(n) :
        total = 0
        for i in range(n):
            total = total + 1 / i
        return total
    ```

    Lors de l'exécution de `somme(10)`, le message d'erreur `#!py ZeroDivisionError: division by zero` apparaît. Identifier le problème et corriger la fonction pour qu'elle effectue le calcul demandé. 

    ??? done "Réponse"
        
        Il s'agit d'un problème d'indice mal parcouru par `#!py range(n)` qui va parcourir les entiers de $0$ inclus à $n$ exclu au lieu de $1$ inclus à $n+1$ exclu.

        ```python
        def somme(n) :
            total = 0
            for i in range(1, n + 1):
                total = total + 1 / i
            return total
        ```

2. On considère la fonction `maxi` qui prend comme paramètre une liste `L` **non vide** de nombres et renvoie le plus grand nombre de cette liste :

    ```python
    def maxi(L) :
        indice = 0
        maximum = 0
        while indice <= len(L):
            if L[indice] > maximum :
                maximum = L[indice]
            indice = indice + 1
        return maximum
    ```

    a. Lors de l'exécution de `maxi([2, 4, 9, 1])` une erreur est déclenchée. Identifier et corriger le problème.

    ??? done "Réponse"
        
        Tout d'abord il s'agit d'un problème de dépassement d'indice  dans la liste `L`, puisqu'au dernier tour de boucle `indice` est égal à `len(L)` qui est en dehors de la plage de validité des indices (entre `0` et `len(L) - 1`)

        On corrige ce premier bug :

        ```python
        def maxi(L) :
            indice = 0
            maximum = 0
            while indice < len(L) :
                if L[indice] > maximum :
                    maximum = L[indice]
                indice = indice + 1
            return maximum
        ```


    b. Le bug précédent est maintenant corrigé. Que renvoie à présent l'exécution de `maxi([-2, -7, -3])` ? Modifier la fonction pour qu'elle renvoie le bon résultat.

    ??? done "Réponse"
        Ensuite comme on a initialisé `maximum` à `0`, le parcours des éléments tous négatifs de `[-2, -7, -3]` ne peut modifier la valeur de `maximum`.
        `maxi([-2, -7, -3])` renvoie donc `0` qui n'est pas le maximum de `[-2, -7, -3]`.

        On corrige ce bug en initialisant `maximum` avec le premier élément de la liste `L` non vide.

        ```python
        def maxi(L) :
            indice = 0
            maximum = L[0]
            while indice < len(L):
                if L[indice] > maximum :
                    maximum = L[indice]
                indice = indice + 1
            return maximum
        ```

3. On souhaite réaliser une fonction qui génère une liste de `n` joueurs identifiés par leur numéro. Par exemple on souhaite que l'appel `genere(3)` renvoie la liste `['Joueur 1', 'Joueur 2', 'Joueur 3']`.

    ```python
    def genere(n) :
        L = []
        for i in range(1, n + 1):
            L.append('Joueur ' + i)
        return L
    ```

    L'appel `genere(3)` déclenche l'erreur suivante `#!py TypeError: can only concatenate str (not "int") to str.`

    Expliquer ce message d'erreur et corriger la fonction afin de régler le problème.

    ??? done "Réponse"

        Le message d'erreur signale que lors de l'exécution du code, Python a essayé de concaténer une chaine de caractères et un entier. La concaténation avec une chaine n'est  possible qu'avec une autre chaine de caractères. Pour corriger ce bug il suffit de convertir l'entier en chaine de caractère avec le constructeur `str`.

        ```python
        def genere(n) :
        L = []
        for i in range(1, n + 1):
            L.append('Joueur ' + str(i))
        return L
        ```

4. On considère la fonction `suite` qui prend un entier positif `n` en paramètre et renvoie un entier.

    ```python
    def suite(n) :
        if n == 0:
            return 0
        else :
            return 3 + 2 * suite(n - 2)
    ```

    a. Quelle valeur renvoie l'appel de `suite(6)` ?

    ??? done "Réponse"
    
        On commence par évaluer `suite(6)`.
        `suite(6) = 3 + 2 * suite(6 - 2) = 3 + 2 * suite(4)`  
        Or `suite(4) = 3 + 2 * suite(4 - 2) = 3 + 2 * suite(2)`  
        On continue la descente : `suite(2) = 3 + 2 * suite(2 - 2)= 3 + 2 * suite(0)`
        $0$ est un cas de base, la descente s'arrête : `suite(0) = 0`.  
        On peut remonter pour calculer toutes les valeurs en attente :
        `suite(2) = 3 + 2 * suite(0) = 3` puis `suite(4) = 3 + 2 * suite(2) = 9` et enfin `suite(6) = 3 + 2 * suite(4) = 21`


    b. Que se passe-t-il si on exécute `suite(7)` ?

    ??? done "Réponse"

        On essaie d'évaluer `suite(7)` de la même façon.
        `suite(7) = 3 + 2 * suite(7 - 2) = 3 +2 * suite(5)`.  Or `suite(5) = 3 + 2 * suite(5 - 2) = 3 + 2 * suite(3)`.  On continue la descente : `suite(3) = 3 + 2 * suite(3 - 2)= 3 + 2 * suite(1)`.  On continue la descente : `suite(1) = 3 + 2 * suite(1 - 2)= 3 + 2 * suite(-1)`.  Zut, on a passé le cas de base et la descente ne s'arrêtera jamais, elle sera infinie ! Sauf que ...

        Le calcul de `suite(7)` sera stoppé par l'interpréteur Python au bout de 1000 appels récursifs et une erreur sera affichée. Il est possible de modifier cette valeur ; un garde fou.

5. On considère le code Python ci-dessous :

    ```python
    x = 4
    L = []
    def modif(x, L):
        x = x + 1
        L.append(2 * x)
        return x, L

    print(modif(x, L))
    print(x, L)
    ```

    a. Qu'affiche le premier `print` ?
    b. Qu'affiche le second `print` ?

    ??? done "Réponse"

        Le premier `print` affiche les valeurs de `x` et `L` renvoyées par `modif(x, L)`. Ce sont des valeurs de `x` et `L` dans la portée locale de la fonction `modif` :  c'est-à-dire 5 pour `x` et `[10]` pour  `L` à la fin de l'évaluation de modif(x, L)`

        Le second `print` affiche les valeurs des variables  `x` et `L` après exécution de `modif(x, L)` mais dans la portée globale du script. Les valeurs de  `x` et `L` ont été transmises en paramètres à `modif` et recopiées dans des variables locales de même nom. `modif(x, L)` a donc modifié des copies des variables globales `x` et `L` de valeurs initiales respectives 4 et `[]`.   La modification de la valeur reçue par la variable locale `x` n'a aucune incidence sur la variable globale `x` car cette valeur est un entier de type simple. En revanche la valeur de la variable globale `L` de type `list` est une référence vers la séquence de valeurs. La variable locale `L` et la variable globale `L` partagent la même référence donc par effet de bord, les modifications appliquées par la variable locale sont répercutées sur la variable globale.

        Ainsi, après évaluation de `modif(x, L)`, `L` est modifiée mais pas `x`. Le second `print` affiche la valeur 4 pour `x` et la valeur `[10]` pour `L`. 
