# Programme Fibonacci

La suite de Fibonacci est une séquence célèbre en mathématiques, attribuée au mathématicien italien Fibonacci au 13eme siècle.
Elle commence avec 0 et 1, le nombre suivant est ensuite calculé comme *la somme des deux nombres précédents*.
Elle se poursuit ainsi à l'infini:

$0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, \dots$

Elle a des applications dans la composition de musique, la prédiction en bourse, le schéma des graines de tournesols et beaucoup d'autres domaines.
Ce programme vous permet de la calculer aussi loin que vous le souhaitez.
Pour plus d'informations sur cette suite: <https://en.wikipedia.org/wiki/Fibonacci_number>.

## Le programme en action

Lorsque vous l'exécutez, la sortie ressemblera à ceci :

```text
Suite de Fibonacci, par Al Sweigart al@inventwithpython.com
--exemple--
Entrez le n-ième nombre de Fibonacci que vous souhaitez calculer (comme 5, 50, 1000, 9999), ou S ou s pour Stopper:
> 50

0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269, 2178309, 3524578, 5702887, 9227465, 14930352, 24157817, 39088169, 63245986, 102334155, 165580141, 267914296, 433494437, 701408733, 1134903170, 1836311903, 2971215073, 4807526976, 7778742049
```

## Comment ça marche

Comme les nombres de Fibonacci grandissent très vite, les lignes 28 à 31 vérifient si le nombre entré est supérieur ou égal à 10.000 et affiche dans ce cas un avertissement comme quoi il faudra un peu de temps pour l'affichage final.
Alors que le programme peut effectuer des millions d'opérations mathématiques presque instantanément, l'affiche de texte est relativement lent et peut prendre plusieurs secondes.

L'avertissement dans le programme rappelle que vous pouvez toujours arrêter le programme en appuyant sur ++Ctrl+C++.

## Le programme

```python
print("""Suite de Fibonacci, par Al Sweigart al@inventwithpython.com
Elle commence avec 0 et 1, le nombre suivant est ensuite calculé comme *la somme des deux nombres précédents*.
0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, ...
""")
while True: #Boucle principale
    nieme = None
    while nieme == None:
        # On demande un entier valide, ni None, ni 0
        print("Entrez le n-ième nombre de Fibonacci que vous souhaitez calculer (comme 5, 50, 1000, 9999), ou STOP pour arrêter:")
        reponse = input('>') #(!) Que se passe t'il si on change '>' par 'Tapez ici:' ?

        if 'S' in reponse or 's' in reponse: #(!) Remplacez 'S' par 'STOP' et 's' par 'stop', comment arrêter le programme ?
            print("Merci d'avoir calculé !")
            exit()
        elif reponse.isdecimal() and int(reponse) != 0:
            nieme = int(reponse)
        else:
            print("S'il vous plait, entrez un nombre > 0, ou s ou S")

    if nieme == 1:
        print("0")
        print(f"Le nombre #{nieme} de Fibonacci est : 0.") #(!) Enlevez le f, que s'affiche t'il ?
    elif nieme == 2:
        print("0, 1")
        print(f"Le nombre #{nieme} de Fibonacci est : 2.")
    else:
        if nieme > 10_000: #(!) Enlevez le _, que se passe-t-il ? En déduire son utilité
            print("ATTENTION, le calcul va mettre longtemps à s'afficher.")
            print("Si vous voulez terminer le programme avant, appuyez sur Ctrl+C")
            input("Appuyez maintenant sur Entrée pour lancer le programme")
    
        # Calcul du n-ieme nombre de Fibonacci

        # On initialise nos variables
        avantDernier = 0
        dernier = 1
        nombresCalcules = 2

        print('0, 1, ', end='') #(!) Enlevez le end='', que se passe-t-il ?

        while nombresCalcules != nieme:
            # On calcule le nombre suivant comme la somme des deux derniers et on l'affiche.

            suivant = avantDernier + dernier
            nombresCalcules += 1 #(!) Que se passe-t-il si l'on commente cette ligne ?

            print(suivant, end='')

            # Si on est au dernier nombre, on s'arrête
            if nombresCalcules == nieme:
                print('\n')
                print(f"Le nombre #{nieme} de Fibonacci est : {suivant}.")
            # Sinon on affiche la virgule puis on met à jour les entrées.
            else: #(!) Le else est-il nécessaire ?
                print(', ', end='')
                avantDernier = dernier
                dernier = suivant

```

Après avoir saisi le code source et l'avoir exécuté plusieurs fois, essayez de le modifier.
Les commentaires marqués d'un (!) proposent de petits changements que vous pouvez apporter.

## Exploration du programme

Essayez de répondre aux questions suivantes.
Expérimentez en apportant des modifications au code et relancez le programme pour voir les effets de ces modifications.

1. Essayez des nombres différents de 0 et 1 pour démarrer.
2. Modifiez le programme pour additionner les trois nombres précédents au lieu des deux précédents.
3. Sachant que la fonction `log(x,b)` du module `math` donne la taille du nombre `x` en base `b`, affichez la taille des nombres de Fibonacci en base 10, au lieu des nombres eux mêmes.
4. Quelles autres séquences peuvent être programmées de manière similaire ? À quoi ce programme pourrait-il servir ?