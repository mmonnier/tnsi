# Mini projet : Mendeleiev

Ce *mini-projet* a pour but de travailler:

- La programmation Python
- La programmation Web avec [Flask](https://flask.palletsprojects.com/en/3.0.x/)
- Les requêtes SQL.

L'objectif est d'obtenir les informations des éléments atomiques, à partir de la base suivante [periodic.sqlite](/tnsi/ressources/periodic.sqlite).
Tout les fichiers nécessaires sont dans [l'archive suivante](/tnsi/ressources/mendeleiev.zip).

## Préambule

Vous devez disposer de **Python** ainsi que du module *Flask* (non standard).
Si vous utilisez *Thonny*, vous pouvez installer *Flask* en allant dans *Outils > Gérer les paquets*, recherchez alors *flask* et installez le.
Sinon, vous pouvez passer par *pip* ou bien la procédure de votre choix (voir le site de *Flask*).

## Fichiers de l'application

* `app.py` : fichier principal de l'application Flask
* `templates/` : dossier contenant les fichiers de vue HTML
    + `index.html` : vue pour afficher la liste des éléments
    + `element.html` : vue pour afficher les caractéristiques d'un élément donné
* `periodic.sqlite` : base de données SQLite contenant la table `periodic` avec les caractéristiques des éléments

## Structure de l'application

### Fichier app.py

```python
from flask import Flask, render_template, request
import sqlite3

app = Flask(__name__)

_db_connection = None

@app.route('/')
def index():
    global _db_connection
    if _db_connection is None:
        raise Exception("BDD non définie")
    cur = _db_connection.cursor()
    cur.execute("SELECT * FROM periodic")
    elements = cur.fetchall()
    cur.close()
    return render_template('index.html', elements=elements)

@app.route('/element/<symbole>')
def element(symbole):
    global _db_connection
    if _db_connection is None:
        raise Exception("La connexion à la base de données n'est pas initialisée.")
    cur = _db_connection.cursor()
    cur.execute("SELECT * FROM periodic WHERE symbole=?", (symbole,))
    element = cur.fetchone()
    cur.close()
    if element is None:
        return f"Aucun élément trouvé avec le symbole {symbole}."
    return render_template('element.html', element=element)

def _get_db_connection():
    conn = sqlite3.connect('periodic.sqlite', check_same_thread=False)
    conn.row_factory = sqlite3.Row
    return conn

if __name__ == '__main__':
    _db_connection = _get_db_connection()
    app.run(debug=True)
```

### Fichier `index.html`

```html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tableau périodique</title>
  </head>
  <body>
    <h1>Tableau périodique</h1>
    <table>
      <thead>
        <tr>
          <th>Nom</th>
          <th>Symbole</th>
          <th>Numéro atomique</th>
        </tr>
      </thead>
      <tbody>
        {% for element in elements %}
          <tr>
            <td>{{ element['nom'] }}</td>
            <td>{{ element['symbole'] }}</td>
            <td>{{ element['numero_atomique'] }}</td>
            <td><a href="{{ url_for('element', symbole=element['symbole']) }}">Détails</a></td>
          </tr>
        {% endfor %}
      </tbody>
    </table>
  </body>
</html>
```

### Fichier `element.html`

```html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Élément chimique {{ element['symbole'] }} </title>
  </head>
  <body>
    <h1>{{ element['nom'] }}</h1>
    <p>Symbole : {{ element['symbole'] }}</p>
  </body>
</html>
```

## À Faire

1. Exécuter le code proposé en décompressant l'archive et en exécutant le fichier `app.py`
2. Chercher l'utilité du `if __name__ == "__main__"`
3. Compléter le fichier `element.html` pour afficher toutes les informations d'un élément périodique.
4. Proposer une nouvelle page permettant de comparer deux éléments passés en arguments.
5. Améliorer cette application avec du CSS et autres joyeusetés.