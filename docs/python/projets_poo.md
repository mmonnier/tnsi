# Mini projets

## Grille de notation

Sur le code:

| Critère        | Barème | Commentaire                                                                                |
| --             | --     | --                                                                                         |
| Propreté       | /2     | Avez vous des lignes inutiles, des variables non utilisées, des fonctions ...              |
| Documentation  | /2     | Toutes les méthodes/fonctions/classe/attributs publiques sont-elles commentées utilement ? |
| Fonctionnement | /1     | Est-ce que le programme se lance ?                                                         |
| Tests          | /2     | Est-ce que les méthodes sont testées ? (asserts, lignes de tests, instanciation ...)       |
| Consigne       | /3     | Est-ce que les demandes minimales du projet sont respectées ?                              |

Sur la présentation:

| Critère                 | Barème | Commentaire                                                            |
| ---                     | ---    | ---                                                                    |
| Posture orale           | /2     | Vous adressez vous au public, regardez vous trop vos notes ?           |
| Justification des choix | /1     | Êtes vous capable de justifier vos choix de conception ?               |
| Préparation             | /1     | Avez vous préparé un déroulé ? Montrez vous simplement le code ?       |
| Dialogue                | /2     | Êtes vous capable de répondre aux questions ? De revenir sur le code ? |

Pour un ordre d'idée, la présentation ne doit pas durer plus d'une dizaine de minutes, questions comprises.

## Projet 1 : jeu de dominos

L’objectif de cet Projet est de créer un jeu de domino (les 28 pièces du jeu).

1. Définir une classe `Domino` qui permette d’instancier des objets simulant les pièces d’un jeu de dominos. Le *constructeur* de cette classe initialisera les valeurs des points présents sur les deux faces A et B du domino.
2. Ajouter une méthode `valeur` renvoyant un tuple composé des valeurs des deux faces.
3. Ajouter une méthode `compatible` prenant en paramètre un objet `other` de type `Domino` et renvoyant `True` si `other` est compatible avec l’objet courant, c’est-à-dire que la valeur d’une de ses faces est égale à la valeur de l’une des faces de l’objet courant.
4. Écrire une fonction `créer_jeu` permettant de créer la liste des 28 dominos possibles. 

!!! tip "Pour ne pas se perdre"
    On rappelle qu’une face d’un domino peut être vide (on donnera la valeur $0$) ou prendre une valeur entière de $1$ à $6$.
    De plus, il n’existe pas deux dominos identiques (les couples de valeurs `(4,3)` et `(3,4)` sont considérés comme désignant le même domino).
    Enfin, la fonction doit bien renvoyer **une liste de `Domino`**.

5. Écrire une fonction `afficher_main`, prenant en paramètres une liste de dominos `main`. La tester sur la liste `jeu` obtenue en appelant la fonction `créer_jeu`.

!!! tip "Pour que ce soit cohérent"
    On utilisera la méthode `valeur` des dominos de la liste.


### Pour aller plus loin:

On écrira un programme simulant une partie de domino entre deux joueurs et pour cela :

6. Écrire une fonction `piocher` choisissant au hasard un domino dans la pioche passée en paramètre et éliminant ce domino de la pioche.
7. Écrire une fonction `distribuer` créant, à partir de la liste des 28 dominos possibles et aléatoirement, la liste des sept dominos pour deux joueurs et la pioche (les 14 dominos restants) et renvoyant ces trois listes de dominos.
8. On considère que les deux valeurs de faces possibles à jouer peuvent être vues comme un domino virtuel. Écrire une fonction `jouer` prenant en paramètre la `main` d'un joueur, un `domino` et la `pioche` et faisant jouer le joueur : trouver un domino compatible avec le domino virtuel ou bien piocher. Dans les deux cas la main sera mise à jour.
9. Ajouter une méthode `modifier` prenant en paramètre un objet `domino` de classe `Domino` et remplaçant la valeur de la face commune aux deux dominos de l’objet appelant par la deuxième face de l’objet `domino`.

**Soyons exigeants**: Faites afficher avec turtle votre partie de domino !


??? bug "Une idée de correction"
    ```python
	from random import randint,shuffle
	class Domino:
		def __init__(self,face_A,face_B):
			self.face_A = face_A
			self.face_B = face_B

		def valeur(self):
			return self.face_A,self.face_B
		def compatible(self,other):
			self_val,other_val = self.valeur(),other.valeur()
			return self_val[0] in other_val or self_val[1] in other_val

	def creer_jeu():
		liste_dominos = []
		for face_A in range(0,7):
			for face_B in range(i,7):
				liste_dominos.append(Domino(face_A,face_B))
		return liste_dominos

	def afficher_main(main):
		print("la main contient:")
		for domino in main:
			print(f" - {domino.valeur()}")

	def piocher(pioche):
		return pioche.pop(randint(0,len(pioche)-1))

	def distribuer():
		pioche = creer_jeu()
		j1,j2 = [],[]
		for loop in range(7):
			j1.append(piocher(pioche))
			j2.append(piocher(pioche))
		return pioche,j1,j2

	def jouer(main,domino,pioche):
		pass
	def modifier():
		pass
    ```

## Projet n°2 : jeu de cartes

Définissez une classe `JeuDeCartes` permettant d’instancier des objets *jeu de cartes* dont le comportement soit similaire à celui d’un vrai jeu de cartes. La classe devra comporter au moins les quatre méthodes suivantes : provoquer l’affichage `'As de coeur'`.

- Méthode constructeur: création et remplissage d’une liste de 52 éléments, qui sont eux-mêmes des tuples de deux éléments contenant les caractéristiques de chacune des 52 cartes. Pour chacune d’elles, il faut en effet mémoriser séparément un nombre entier indiquant la valeur (2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, les quatre dernières valeurs étant celles des valet, dame, roi et as), et un autre nombre entier indiquant la couleur de la carte (c’est-à-dire 0,1,2,3 pour Pique, Trèfle, Carreau et Coeur). Dans une telle liste, l’élément (11,1) désigne donc le valet de Trèfle, et la liste terminée doit être du type : $$(2,0),(3,0),(4,0),...,(12,3),(13,3),(14,3).$$
- Méthode `nomCarte` : cette méthode renvoie sous la forme d’une chaîne de caractères l’identité d’une carte quelconque, dont on lui a fourni le tuple `t` descripteur en argument. Par exemple, l’instruction : `print(jeu.nom_carte((14, 3))` doit
- Méthode `battre` : comme chacun sait, battre les cartes consiste à les mélanger. Cette méthode sert donc à mélanger les éléments de la liste contenant les cartes, quel qu’en soit le nombre.
- Méthode `tirer` : lorsque cette méthode est invoquée, une carte est retirée du jeu. Le tuple contenant sa valeur et sa couleur est renvoyé au programme appelant. On retire toujours la première carte de la liste. Si cette méthode est invoquée alors qu’il ne reste plus aucune carte dans la liste, il faut alors renvoyer l’objet spécial `None` au programme appelant.

Exemple d’utilisation de la classe `JeuDeCartes` :

```
jeu = JeuDeCartes() # instanciation d'un objet
jeu.battre() # m´elange des cartes
for n in range(52): # tirage des 52 cartes
    c = jeu.tirer()
    if c == None: # il ne reste plus aucune carte
        print('Terminé !') # dans la liste
    else:
        print(jeu.nomCarte(c)) # valeur et couleur de la carte
```

### Pour aller plus loin

Pour jouer au jeu de la bataille on devra:

- Définir deux joueurs A et B.
- Instancier deux jeux de cartes (un pour chaque joueur) et les mélanger.

Ensuite, à l’aide d’une boucle:

- Tirer 52 fois une carte de chacun des deux jeux et comparer leurs valeurs.
- Si c’est la première des deux qui a la valeur la plus élevée, on ajoute un point au joueur A.
- Si la situation contraire se présente, on ajoute un point au joueur B.
- Si les deux valeurs sont égales, on passe au tirage suivant.
- Au terme de la boucle, comparer les comptes de A et B pour déterminer le gagnant.

**Soyons exigeants**: Rajoutez des pouvoirs à certaines cartes qui vont drastiquement changer la façon de jouer ! (Tirer une dame de coeur fait retourner votre pioche, tirer un valet vous permet de piocher deux autres cartes et de les additionner, ...)

## Projet 3 : calculer avec des fractions

1. Définir une classe `Fraction` dont les attributs sont `num` et `den` (des valeurs entières, `den` est même strictement positif).
2. Surcharger l’opérateur `+` (`__add__`) afin de pouvoir additionner deux objets de la classe `Fraction`. 
	* Donner un exemple d’utilisation.
3. Surcharger l’opérateur `*` (`__mul__`) afin de pouvoir multiplier deux objets de la classe `Fraction`.
	* Donner un exemple d’utilisation.
4. Définir une méthode `opposée` et une méthode `inverse` renvoyant respectivement l’opposée et l’inverse d’une fraction.

Pour la question suivante on utilisera les méthodes spéciales `__sub__,__truediv__`.

5. Compléter avec la surcharge des opérateurs `-` et `/` à l’aide des méthodes déjà d´définies. Donner des exemples d’utilisation.

Pour la question suivante on utilisera les méthodes spéciales `__eq__,__le__`.

6. Surcharger enfin les opérateurs `<=` et `==`. Donner des exemples d’utilisation.

Ne pas oublier de documenter les fonctions.

### Pour aller plus loin

Faites une interface en ligne de commande qui permettra de faire des calculs avec vos fractions on pourra avoir une option pour les stocker, une autre pour faire l'addition de deux fractions stockées et ainsi de suite.

Ajouter une méthode pour simplifier vos fractions et modifiez toutes les anciennes méthodes pour toujours stocker la fraction la plus simplifiée.

**Soyons exigeants**: Prenez en compte les racines carrées dans vos fractions grâce à une nouvelle classe `RacineCarree` !

??? bug "Proposition de correction"
	```python
	def Fraction:
		def __init__(self,num,den):
			assert type(num) == int
			assert type(den) == int and den > 0
			self.num = num
			self.den = den
			self.simplifie()
			
		def __add__(self,other):
			assert type(other) == Fraction
			return Fraction(self.num*other.den + other.num*self.den,self.den*other.den)
			
		def __mul__(self,other):
			assert type(other) == Fraction
			return Fraction(self.num*other.num,self.den*other.den)
			
		def opposee(self):
			return Fraction(-self.num,self.den)
			
		def inverse(self):
			if self.num == 0:
				return Fraction(0,1)
			if self.num < 0:
				return Fraction(-self.den,-self.num)
			return Fraction(self.den,self.num)
			
		def __sub__(self,other):
			assert type(other) == Fraction
			return self + other.opposee()
		def __truediv__(self,other):
			assert type(other) == Fraction
			return self * other.inverse()
		def __eq__(self,other):
			return self.num == other.num and self.den == other.den
		def __ge__(self,other):
			return ( self.den == other.den and self.num >= other.num ) or ( self.num*other.den >= other.num*self.den )
			
		def simplifie(self):
			def pgcd(a, b):
				"""Calcule le PGCD de deux nombres a et b."""
				while b != 0:
					a, b = b, a % b
				return a
			p = pgcd(self.num,self.den)
		    self.num //= p
			self.den //= p
	```
## Projet 4 : intervalles

1. Définir une classe `Intervalle` définissant des intervalles de nombres (inégalités larges) dont les attributs sont `a` et `b` (deux nombres entiers ou flottants avec `a <= b`) avec un constructeur (on considérera que l’intervalle est vide si `a > b`).
2. Ajouter une méthode `est_vide` qui renvoie `True` si un objet de la classe `Intervalle` est vide, et `False` sinon.
3. Ajouter une méthode `longueur` renvoyant la longueur de l’intervalle (`0` s'il est vide, `b-a` dans les autres cas).
4. Ajouter une une surcharge de l’opérateur booléen `in` (`__contains__`) testant l’appartenance d’un nombre `x` à l’intervalle.
5. Écrire les fonctions `intersection` et `union` réalisant respectivement l’intersection et le plus petit intervalle contenant l’union de deux intervalles passés en paramètres (sans les modifier) : elles renvoient un nouvel intervalle.

Ne pas oublier de documenter les fonctions et de tester le code.

### Pour aller plus loin

Créez une classe `Fonction` qui stocke une fonction et un intervalle de définition et créez lui des méthodes pour trouver le maximum sur l'intervalle défini, le minimum.

**Soyons exigeants**: Utilisez la bibliothèque matplotlib pour afficher vos fonctions ainsi définies !


??? bug "Proposition de correction"
	```python
	class Intervalle:
		def __init__(self,debut,fin):
			if a <= b:
				self._a = debut
				self._b = fin
			else:
				self._a = None
				self._b = None
		def est_vide(self):
			return self._a is None and self._b is None
		def longueur(self):
			return self._b - self._a if not self.est_vide() else 0
		def __contains__(self,x):
			assert type(x) in [int,float]
			return self._a <= x <= self._b or not self.est_vide()
		def intersection(self,other):
			assert type(other) == Intervalle, "Impossible avec autre chose qu'un intervalle"
			
			if self.est_vide() or other.est_vide():
				return Intervalle(10,0) # Construira l'intervalle vide
			
			# self = [a;b], other = [c;d]
			a,b,c,d = self._a,self._b,other._a,other._b
			# si on a [a;b;c;d], b != c
			if a <= b < c <= d:
				return Intervalle(10,0)
			elif a <= b <= c <= d: # [ a; b=c ; d]
				return Intervalle(b,c)
			elif a <= c <= b <= d: # [ a ; c ; b ; d]
				return Intervalle(c,b)
			elif a <= c <= d <= b: # [a ; c ; d ; b]
				return Intervalle(c;d)
			elif c <= a <= d <= b: # [c ; a ; d ; b]
				return Intervalle(a,d)
			elif c <= a <= b <= d: # [c ; a ; b ; d]
				return Intervalle(a,b)
			elif a == d:
				return Intervalle(a,a)
		def union(self,other): # même idée qu'au dessus, il faut tester tout les cas.
			pass
			
			
	```


## Projet 5 : dates

1. Définir une classe `Date` pour représenter une date avec le numéro du jour, du mois et de l’année.
2. Ajouter un attribut de classe contenant la liste des mois de l’année (liste de chaînes de caractères).
3. Ajouter une méthode permettant d’afficher une date sous la forme `'8 mai 1945'` (utiliser l’attribut de classe).
4. Ajouter une méthode permettant de comparer deux dates.
5. Ajouter une méthode permettant d'ajouter une durée à une date et d'obtenir une nouvelle date.

Ne pas oublier de documenter les fonctions et de tester le code.

### Pour aller plus loin

Créez une application en ligne de commande qui permette d'enregistrer un évènement à une date donnée, de sélectionner un évènement et de modifier ses informations.

**Soyons exigeants**: Affichez un calendrier avec vos évènements mis en exergue (avec des * autour par exemple) pour améliorer l'interface ou bien faîtes une interface web !


??? bug "Une proposition de correction"
	```python
	class Date:
		__mois_fr = "Janvier","Février","Mars,"Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"
		
		def __est_coherente(jour,mois,annee):
		    bissex = (annee % 4 == 0 and annee % 100 != 0) or (annee % 400 == 0)
			duree_mois = [31] + ([29] if bissex else [28]) + [31,30,31,30,31,31,30,31,30,31]
			
			return 1 <= jour <= duree_mois[mois-1]
			
		def __init__(self,jour,numero_mois,annee):
			assert Date.__est_coherente(jour,numero_mois,annee), "la date proposée ne peut pas exister."
			self._jour = jour
			self._mois = numero_mois
			self._annee = annee
			
			self._nom_mois = Date.__mois_fr[numero_mois-1]
			
		def __str__(self):
			return f"{self._jour} {self._nom_mois} {self._annee}"
			
		def __eq__(self,other):
			assert type(other) == Date, f"Impossible de comparer une Date et une {type(other)}" 
			return self._jour,self._mois,self._annee == other._jour,other._mois,other._annee
			
		def __lt__(self,other):
			assert type(other) == Date, f"Impossible de comparer une Date et une {type(other)}"
			return self._annee < other._annee or (
				self._annee == other._annee and self._mois < other._mois) or (
				self._annee == other._annee and self._mois == other._mois and self._jour < other._jour)
	
		def __le__(self,other):
			assert type(other) == Date, f"Impossible de comparer une Date et une {type(other)}"
			return self < other or self == other
			
		def __gt__(self,other):
			assert type(other) == Date, f"Impossible de comparer une Date et une {type(other)}"
			return not (self <= other)
			
		def __ge__(self,other):
			assert type(other) == Date, f"Impossible de comparer une Date et une {type(other)}"
			return self > other or self == other
			
		def __ne__(self,other):
			assert type(other) == Date, f"Impossible de comparer une Date et une {type(other)}"
			return not (self == other)
			
		def __add__(self,other):
			if type(other) == int: # On ajoute un nombre de jours
				bissex = (self._annee % 4 == 0 and self._annee % 100 != 0) or (self._annee % 400 == 0)
				duree_mois = [31] + ([29] if bissex else [28]) + [31,30,31,30,31,31,30,31,30,31] # On calcule la durée des mois de l'année.
				new = Date(self._jour,self._mois,self._annee) # On crée la nouvelle date
				new._jour += other
				while new._jour > duree_mois[new._mois-1]: # Si on a trop de jours par rapport au mois
				    new._jour -= duree_mois[new._mois-1] 
				    new._mois += 1 # On change de mois, en enlevant les jours passés.
					if new._mois > 12: # Si on doit changer d'année
						new._mois = 1 # On passe en janvier
					    new._annee += 1
						# Attention si la nouvelle année est bissextile ou non, au mois de février.
						duree_mois[1] = 29 if (new._annee % 4 == 0 and new._annee % 100 != 0) or (new._annee % 400 == 0) else 28
	```
## Projet 6 : QCM

Créez une classe `Question` permettant de gérer une question à choix multiples dans un QCM.

1. Définir son constructeur prenant en argument une bonne réponse et trois mauvaises, ainsi qu'un énoncé.
2. Définir une méthode d'affichage de la question, qui affiche l'énoncé puis les réponses possibles aléatoirement.
3. Définir une méthode d'affichage de la correction, qui affiche uniquement l'énoncé et la bonne réponse.
4. Définir une fonction `creer_QCM` qui demande à taper au clavier 10 questions et les réponses associées, les enregistre dans des objets `Question` et renvoie la liste des questions créée.
5. Définir une méthode `executer_QCM` qui prend en argument une liste de `Question` et qui les pose les unes après les autres en affichant la correction.

### Pour aller plus loin

Créez une classe `Qcm` qui contient une liste vide et une méthode `ajouter_question` qui permet d'ajouter une `Question` au QCM.
Ensuite reprenez les fonctions `creer_QCM` et `executer_QCM` pour utiliser la classe `Qcm`.


??? bug "Proposition de correction"
	```python
	from random import shuffle,randint
	class Question:
		def __init__(self,enonce,mauvaises,bonne):
			self.enonce = enonce
			self.mauvaises = mauvaises
			self.bonne = bonne
			
			self.reponses = shuffle(mauvaises + [bonne])
		
		def __str__(self):
			question = f"Question : {self.enonce}\n"
			for reponse in self.reponses:
				question += f"\t{i+1}) {reponse}\n"
			return question
			
		def correction(self,num_proposition):
		    correction = f"Correction de la question : {self.enonce}\n"
			proposition = self.reponses[num_proposition-1]
			for reponse in reponses:
				question += f"\t{i+1}) {reponse}"
				if reponse == self.bonne:
					question += "<-- Bonne réponse !"
				if reponse == proposition:
					question += "<-- Votre réponse !"
				question += "\n"
			return question
			
	class Qcm:
		def __init__(self, liste_questions=None):
			self.questions = liste_questions if listes_questions is not None else []
			
		def ajouter_question(self,question: Question):
			self.questions.append(question)
			
		def __call__(self):
			for question in questions:
				print(question)
				rep = int(input("Quelle est votre réponse ?"))
				print(question.correction(rep))
				print("============ Question suivante ============")
	
	def creer_qcm():
		mon_qcm = Qcm()
	    fini = False
		while not fini:
			q = input("Tapez votre question :")
			b = input("Tapez la bonne réponse :")
			print("Tapez autant de mauvaises réponses que voulue, puis STOP")
			mauvaises = []
			proposition = input("Mauvaise réponse:")
			while "stop" != proposition.lower():
				mauvaises.append(proposition)
			mon_qcm.ajouter_question(Question(q,mauvaises,b))
			
	un_qcm = creer_qcm()
	
	un_qcm()
	```
## Projets du livre:

- Exercice **lemmings** du livre, chapitre programmation orientée objet.
- Exercice **Tableau** du livre, chapitre programmation orientée objet.


Certains projets ont étés mis en place par [charles.poulmaire@ac-versailles.fr](mailto:charles.poulmaire@ac-versailles.fr) ou [pascal.remy@ac-versailles.fr](mailto:pascal.remy@ac-versailles.fr).
