# Projet PageRank

L'objectif de ce projet est d'implémenter l'algorithme du PageRank, qui permet d'obtenir un classement des pages webs en fonction de leurs liens.
Pour des informations dessus: <https://fr.wikipedia.org/wiki/PageRank>, on pourra aussi pour aller plus loin regarder <https://interstices.info/comment-google-classe-les-pages-web/>.

Voici le cahier des charges de votre application / bibliothèque:

- ajouter une liste de sites dans le graphe;
- ajouter des liens entre les sites;
- trouver tous les sites qui sont isolés;
- visualiser le réseau graphiquement;
- trouver un chemin entre deux sites;
- calculer le pagerank de chacun des sites du graphe.

Pour arriver à ce résultat, vous adopterez une démarche *incrémentale* (faire petit à petit), en implémentant les fonctionnalités dans l'ordre suivant.
À chaque étape, il vous est conseillé de tester vos fonctionnalités en vérifiant que les anciens tests fonctionnent toujours.
Pensez donc à garder vos tests et à souvent exécuter votre fichier !

## Représentation du réseau sous forme de **Graphe**

Voici un exemple de réseau de sites que nous aimerions représenter.

```mermaid
graph LR;

pageA --> pageB & pageC & pageD;
pageB --> pageA & pageB & pageD;
pageC --> pageD;
pageD --> pageD;
pageE;
```

Voici quelques questions à vous poser avant de démarrer ce projet.

!!! question "Dans ce graphe quels sont les noeuds ? Les arêtes ? Le graphe est-il dirigé ? Pondéré ?"

!!! question "Comment représenter ce réseau par une liste d'adjacence ? Est-ce que certaines arêtes doivent être différenciées ?"

!!! question "Répondez à ces mêmes questions pour un réseau similaire de votre choix"

Maintenant que vous avez réfléchi à la *mise en situation* de votre graphe, il faut en implémenter la structure.
Pour ceci, vous devez commencer par un premier choix:

- préférez-vous un ensemble de fonctions manipulant une structure de données globales (implémentation impérative ou fonctionnelle)
- ou bien une utilisation sous forme d'objet qui sera modifié et aura des fonctionnalités (POO) ?

!!! note "Exemple d'utilisation"

	=== "Fonctionnel"
		```python
		# Initialisation du graphe des pages web
		graphe_pages = {}
		
	    # Exemple d'utilisation
		ajouter_pages(graphe_pages, ["pageA", "pageB", "pageC", "pageD", "pageE"])
		
		# Ajout de liens entre les pages
		ajouter_lien(graphe_pages, "pageA", "pageB")
		ajouter_lien(graphe_pages, "pageA", "pageC")
		ajouter_lien(graphe_pages, "pageB", "pageC")
		ajouter_lien(graphe_pages, "pageC", "pageD")
		ajouter_lien(graphe_pages, "pageD", "pageA")
		ajouter_lien(graphe_pages, "pageD", "pageE")
		ajouter_lien(graphe_pages, "pageE", "pageA")
		
		# Visualiser le réseau de pages avant le calcul du PageRank
		print("Réseau de pages avant le calcul du PageRank :")
		visualiser_reseau(graphe_pages)
		
		# Trouver et afficher les pages isolées
		pages_isolees = trouver_pages_isolees(graphe_pages)
		print("Pages isolées :", pages_isolees)
		
		# Calculer le PageRank des pages
		pagerank_scores = calculer_pagerank(graphe_pages)
		print("Scores de PageRank des pages :", pagerank_scores)
		
		# Trouver le chemin le plus court entre "pageA" et "pageD"
		chemin = trouver_chemin(graphe_pages, "pageA", "pageD")
		print("Chemin le plus court de 'pageA' à 'pageD' :", chemin)
		```
		
	=== "POO"
		
		```python
		class GraphePages:
			pass
			
		# Exemple d'utilisation
		graphe = GraphePages()
		graphe.ajouter_pages(["pageA", "pageB", "pageC", "pageD", "pageE"])
		
		# Ajout de liens entre les pages
		graphe.ajouter_lien("pageA", "pageB")
		graphe.ajouter_lien("pageA", "pageC")
		graphe.ajouter_lien("pageB", "pageC")
		graphe.ajouter_lien("pageC", "pageD")
		graphe.ajouter_lien("pageD", "pageA")
		graphe.ajouter_lien("pageD", "pageE")
		graphe.ajouter_lien("pageE", "pageA")
		
		# Visualiser le réseau de pages avant le calcul du PageRank
		print("Réseau de pages avant le calcul du PageRank :")
		graphe.visualiser_reseau()
		
		# Trouver et afficher les pages isolées
		pages_isolees = graphe.trouver_pages_isolees()
		print("Pages isolées :", pages_isolees)
		
		# Calculer le PageRank des pages
		pagerank_scores = graphe.calculer_pagerank()
		print("Scores de PageRank des pages :", pagerank_scores)
		
		# Trouver le chemin le plus court entre "pageA" et "pageD"
		chemin = graphe.trouver_chemin("pageA", "pageD")
		print("Chemin le plus court de 'pageA' à 'pageD' :", chemin)
		```

Pour finaliser cette première partie, vous devez donc être capable de créer un graphe statique et l'afficher en utilisant l'implémentation de la fonction `visualiser_reseau` ci-dessous:

!!! success "Pour visualiser"

	=== "En fonctionnel"
	
		```python
		import networkx as nx
		import matplotlib.pyplot as plt
		
		...
		
		def visualiser_reseau(reseau):
			G = nx.DiGraph()  # Créer un graphe dirigé
			for mot, derives in reseau.items():
				for derive in derives:
					G.add_edge(mot, derive)  # Ajouter une arête pour chaque dérivation
	        nx.draw(G, with_labels=True, node_size=2000, node_color='lightblue', font_size=10, font_color='black')
			plt.title("Visualisation du Réseau des mots")
			plt.show()  # Afficher le graphe
		```	
	
	=== "En POO"
	
		```python
		import networkx as nx
		import matplotlib.pyplot as plt
		
		...
		class GraphePages:
			...
			def visualiser_reseau(self):
				G = nx.DiGraph()  # Créer un graphe dirigé
				for mot, derives in self.reseau.items():
					for derive in derives:
						G.add_edge(mot, derive)  # Ajouter une arête pour chaque dérivation
	            nx.draw(G, with_labels=True, node_size=2000, node_color='lightblue', font_size=10, font_color='black')
				plt.title("Visualisation du Réseau de mots")
				plt.show()  # Afficher le graphe
		```
## Implémentation de fonctionnalités d'ajouts

!!! note "Interface à implémenter"

	=== "En fonctionnel"
	
		- **ajouter_pages(reseau, liste_pages)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau de pages.
				- `liste_pages` : Une liste de pages à ajouter au réseau.
			- **Retourne** : Rien. Ajoute toutes les pages à la structure.
		- **ajouter_lien(reseau, source, destination)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau de pages.
				- `source` : Page source.
				- `destination` : Page destination.
			- **Retourne** : Rien. Modifie le réseau pour ajouter un lien entre les pages.

	=== "En POO"
	
		- **ajouter_pages(self, liste_pages)** : 
			- **Paramètres** : 
				- `liste_pages` : Une liste de pages à ajouter au réseau.
			- **Retourne** : Rien. Ajoute toutes les pages à la structure.
		- **ajouter_lien(self, source, destination)** : 
			- **Paramètres** : 
				- `source` : Page source.
				- `destination` : Page destination.
			- **Retourne** : Rien. Modifie le réseau pour ajouter un lien entre les pages.

## Implémentation de fonctionnalités de relation (voisins entrants et sortants)

!!! note "Interface à implémenter"

	=== "En fonctionnel"
		- **trouver_voisins(reseau, page)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau de pages.
				- `page` : Chaîne de caractères représentant la page dont on veut trouver les voisins.
			- **Retourne** : Liste des pages qui sont directement accessibles depuis la page donnée.
		- **trouver_pages_entrantes(reseau, page)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau de pages.
				- `page` : Chaîne de caractères représentant la page dont on veut trouver les pages qui pointent vers elle.
			- **Retourne** : Liste des pages qui pointent vers la page donnée.

	=== "En POO"
		- **trouver_voisins(self, page)** : 
			- **Paramètres** : 
				- `page` : Chaîne de caractères représentant la page dont on veut trouver les voisins.
			- **Retourne** : Liste des pages qui sont directement accessibles depuis la page donnée.
		- **trouver_pages_entrantes(self, page)** : 
			- **Paramètres** : 
				- `page` : Chaîne de caractères représentant la page dont on veut trouver les pages qui pointent vers elle.
			- **Retourne** : Liste des pages qui pointent vers la page donnée.

## Implémentation des fonctionnalités de parcours

!!! note "Interface à implémenter"

	=== "En fonctionnel"
		- **trouver_chaine_pages(reseau, depart, arrivee)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau.
				- `depart` : Chaîne de caractères représentant la page de départ.
				- `arrivee` : Chaîne de caractères représentant la page d'arrivée.
			- **Retourne** : Liste des pages formant la chaîne pour transformer `depart` en `arrivee`, ou `None` si aucune chaîne n'existe.
		- **trouver_pages_isolees(reseau)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau de pages.
			- **Retourne** : Liste des pages qui n'ont aucun lien entrant ou sortant.
	
	=== "En POO"
		- **trouver_chaine_pages(self, depart, arrivee)** : 
			- **Paramètres** :
				- `depart` : Chaîne de caractères représentant la page de départ.
				- `arrivee` : Chaîne de caractères représentant la page d'arrivée.
			- **Retourne** : Liste des pages formant la chaîne pour transformer `depart` en `arrivee`, ou `None` si aucune chaîne n'existe.
		- **trouver_pages_isolees(self)** : 
			- **Paramètres** : Aucun.
			- **Retourne** : Liste des pages qui n'ont aucun lien entrant ou sortant.
	

## Implémentation du pagerank

Voici un pseudocode pour cet agorithme:

```
Fonction calculer_pagerank(graphe, d = 0.85, max_iter = 100):
    n = nombre de pages dans graphe
    pagerank = dictionnaire avec chaque page initialisée à 1/n

    Pour i de 1 à max_iter:
        nouveau_pagerank = dictionnaire vide

        Pour chaque page P dans graphe:
            nouveau_pagerank[P] = (1 - d) / n  // Damping factor

            Pour chaque page Q dans graphe:
                Si P est dans les voisins de Q:
                    nouveau_pagerank[P] += d * (pagerank[Q] / nombre de liens sortants de Q)

        pagerank = nouveau_pagerank

    Retourner pagerank
```

!!! note "Interface à implémenter"

	=== "En fonctionnel"
		- **calculer_pagerank(reseau, d=0.85, max_iter=100)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau de pages.
				- `d` : Facteur d'amortissement (par défaut 0.85).
				- `max_iter` : Nombre maximum d'itérations pour le calcul (par défaut 100).
			- **Retourne** : Dictionnaire contenant les scores de PageRank pour chaque page.

	=== "En POO"
		- **calculer_pagerank(self, d=0.85, max_iter=100)** : 
			- **Paramètres** : 
				- `d` : Facteur d'amortissement (par défaut 0.85).
				- `max_iter` : Nombre maximum d'itérations pour le calcul (par défaut 100).
			- **Retourne** : Dictionnaire contenant les scores de PageRank pour chaque page.
