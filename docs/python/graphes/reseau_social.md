# Projet Réseau social

L'objectif de ce projet est de simuler un réseau social dirigé (avec possibilité de suivi) de type micro-blogging.
Voici le cahier des charges de votre application / bibliothèque:

- pouvoir ajouter une nouvelle personne au réseau;
- pouvoir ajouter et supprimer des liens de suivi entre deux personnes dans le réseau;
- trouver toutes les personnes suivantes une personnalité donnée;
- envoyer un message à toutes les personnes en suivant une autre;
- visualiser le réseau social graphiquement;
- trouver les **personnes influentes**;
- trouver une **chaine** de personnes pour faire passer un message jusqu'à une autre personne.

Pour arriver à ce résultat, vous adopterez une démarche *incrémentale* (faire petit à petit), en implémentant les fonctionnalités dans l'ordre suivant.
À chaque étape, il vous est conseillé de tester vos fonctionnalités en vérifiant que les anciens tests fonctionnent toujours.
Pensez donc à garder vos tests et à souvent exécuter votre fichier !

## Représentation du réseau sous forme de **Graphe**

Voici un exemple de réseau social que nous aimerions implémenter (inspiré de la série Lost):

```mermaid
graph LR;

Claire --> John;
Claire --> Aaron;
Charlie --> Claire;
Charlie --> John;
Charlie --> Jack;
Jack --> Kate;
Kate --> Jack;
Sawyer --> Kate;
Jack --> John;
John --> Jack;
```

Voici quelques questions à vous poser avant de démarrer ce projet.

!!! question "Dans ce graphe quels sont les noeuds ? Les arêtes ? Le graphe est-il dirigé ? Pondéré ?"

!!! question "Comment représenter ce réseau par une liste d'adjacence ? Est-ce que certaines arêtes doivent être différenciées ?"

!!! question "Répondez à ces mêmes questions pour un réseau similaire de votre choix"

Maintenant que vous avez réfléchi à la *mise en situation* de votre graphe, il faut en implémenter la structure.
Pour ceci, vous devez commencer par un premier choix:

- préférez-vous un ensemble de fonctions manipulant une structure de données globales (implémentation impérative ou fonctionnelle)
- ou bien une utilisation sous forme d'objet qui sera modifié et aura des fonctionnalités (POO) ?

!!! note "Exemple d'utilisation"
	
	=== "Fonctionnelle"
	
		```python
		# Initialisation du réseau
		reseau_social = {"John": ["Jack", "Jacob"]}
		
		visualiser_reseau(reseau_social)
		
		# Partie Fonctionnalités d'ajouts
		
		ajouter_personne(reseau_social, "Alice")
		ajouter_personne(reseau_social, "Bob")
		ajouter_personne(reseau_social, "Charlie")
		
		suivre(reseau_social, "Alice", "Bob")
		suivre(reseau_social, "Bob", "Charlie")
		ne_plus_suivre(reseau_social,"Bob","Charlie")
		
		# Partie fonctionnalités de relations
		
		print(trouver_suiveurs(reseau_social, "Bob"))  # ['Alice']
		envoyer_message_aux_suiveurs(reseau_social, "Bob")  # Message envoyé à Alice de la part de Bob
		
		# Partie fonctionnalités de parcours
		
		print(trouver_influents(reseau_social))  # Liste des influents
		
		print(trouver_chaine_message(reseau_social, "Alice", "Charlie"))  # Chemin de message
		```
	
	=== "POO"
	
		```python
		class RéseauSocial:
			pass
		
		# Initialisation du réseau
		reseau_social = RéseauSocial()
		reseau_social.visualiser_reseau()
		
		# Partie ajouts
		
		reseau_social.ajouter_personne("Alice")
		reseau_social.ajouter_personne("Bob")
		reseau_social.ajouter_personne("Charlie")
		
		reseau_social.suivre("Alice", "Bob")
		reseau_social.suivre("Bob", "Charlie")
		
		# Partie relation
		
		print(reseau_social.trouver_suiveurs("Bob"))  # ['Alice']
		reseau_social.envoyer_message_aux_suiveurs("Bob")  # Message envoyé à Alice de la part de Bob
		
		# Partie parcours
		
		print(reseau_social.trouver_influents())  # Liste des influents
		
		print(reseau_social.trouver_chaine_message("Alice", "Charlie"))  # Chemin de message
		```

Pour finaliser cette première partie, vous devez donc être capable de créer un graphe statique et l'afficher en utilisant l'implémentation de la fonction `visualiser_reseau` ci-dessous:

!!! success "Pour visualiser"

	=== "En fonctionnel"
	
		```python
		import networkx as nx
		import matplotlib.pyplot as plt
		
		...
		
		def visualiser_reseau(reseau):
			G = nx.DiGraph()  # Créer un graphe dirigé
			for personne, suivis in reseau.items():
				for suivi in suivis:
					G.add_edge(personne, suivi)  # Ajouter une arête pour chaque lien de suivi
	        nx.draw(G, with_labels=True, node_size=2000, node_color='lightblue', font_size=10, font_color='black')
			plt.title("Visualisation du Réseau Social")
			plt.show()  # Afficher le graphe
		```	
	
	=== "En POO"
	
		```python
		import networkx as nx
		import matplotlib.pyplot as plt
		
		...
		class RéseauSocial:
			...
			def visualiser_reseau(self):
				G = nx.DiGraph()  # Créer un graphe dirigé
				for personne, suivis in self.reseau..items():
					for suivi in suivis:
						G.add_edge(personne, suivi)  # Ajouter une arête pour chaque lien de suivi
	            nx.draw(G, with_labels=True, node_size=2000, node_color='lightblue', font_size=10, font_color='black')
				plt.title("Visualisation du Réseau Social")
				plt.show()  # Afficher le graphe
		```	

## Implémentation de fonctionnalités d'ajouts de personnes (nœuds) et de liens de suivi (arêtes)

Pour cette partie vous devez implémenter les fonctionnalités suivantes:

!!! note "Interface à implémenter"

	=== "En fonctionnel"
	
		- **ajouter_personne(reseau, pseudo)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau social.
				- `pseudo` : Chaîne de caractères représentant le pseudo de la personne à ajouter.
			- **Retourne** : Rien. Ajoute une nouvelle personne au réseau.

		- **suivre(reseau, abonne, suivi)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau social.
				- `abonne` : Chaîne de caractères représentant le pseudo de la personne qui suit.
				- `suivi` : Chaîne de caractères représentant le pseudo de la personne à suivre.
			- **Retourne** : Rien. Ajoute un lien de suivi entre deux personnes.
		- **ne_plus_suivre(reseau, abonne, suivi)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau social.
				- `abonne` : Chaîne de caractères représentant le pseudo de la personne qui ne suit plus.
				- `suivi` : Chaîne de caractères représentant le pseudo de la personne à ne plus suivre.
			- **Retourne** : Rien. Supprime le lien de suivi entre deux personnes.

	=== "En POO"
	
		- **ajouter_personne(self, pseudo)** : 
			- **Paramètres** : 
				- `pseudo` : Chaîne de caractères représentant le pseudo de la personne à ajouter.
			- **Retourne** : Rien. Ajoute une nouvelle personne au réseau.
		- **suivre(self, abonne, suivi)** : 
			- **Paramètres** : 
				- `abonne` : Chaîne de caractères représentant le pseudo de la personne qui suit.
				- `suivi` : Chaîne de caractères représentant le pseudo de la personne à suivre.
			- **Retourne** : Rien. Ajoute un lien de suivi entre deux personnes.
		- **ne_plus_suivre(self, abonne, suivi)** : 
			- **Paramètres** : 
				- `abonne` : Chaîne de caractères représentant le pseudo de la personne qui ne suit plus.
				- `suivi` : Chaîne de caractères représentant le pseudo de la personne à ne plus suivre.
			- **Retourne** : Rien. Supprime le lien de suivi entre deux personnes.

## Implémentation de fonctionnalités de relation (voisins, entrants, sortants).

!!! note "Interface à implémenter"

    === "En fonctionnel"
	
        - **trouver_suiveurs(reseau, pseudo)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau social.
				- `pseudo` : Chaîne de caractères représentant le pseudo de la personne dont on veut trouver les suiveurs.
			- **Retourne** : Liste des pseudos des personnes qui suivent la personnalité donnée.
        - **envoyer_message_aux_suiveurs(reseau, expéditeur)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau social.
				- `expéditeur` : Chaîne de caractères représentant le pseudo de la personne qui envoie le message.
			- **Retourne** : Rien. Envoie un message à toutes les personnes suivant l'expéditeur.

    === "En POO"
	
		- **trouver_suiveurs(self, pseudo)** : 
			- **Paramètres** : 
				- `pseudo` : Chaîne de caractères représentant le pseudo de la personne dont on veut trouver les suiveurs.
				- **Retourne** : Liste des pseudos des personnes qui suivent la personnalité donnée.
	    - **envoyer_message_aux_suiveurs(self, expéditeur)** : 
			- **Paramètres** : 
				- `expéditeur` : Chaîne de caractères représentant le pseudo de la personne qui envoie le message.
			- **Retourne** : Rien. Envoie un message à toutes les personnes suivant l'expéditeur.


## Implémentation des fonctionnalités de parcours


!!! note "Interface à implémenter"

    === "En fonctionnel"
	
		- **trouver_influents(reseau)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau social.
			- **Retourne** : Liste des pseudos des personnes influentes, c'est-à-dire celles qui ont le plus de suiveurs.
        - **trouver_chaine_message(reseau, depart, arrivee)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau social.
				- `depart` : Chaîne de caractères représentant le pseudo de la personne de départ.
				- `arrivee` : Chaîne de caractères représentant le pseudo de la personne d'arrivée.
			- **Retourne** : Liste des pseudos formant la chaîne de personnes pour faire passer un message de `depart` à `arrivee`, ou `None` si aucune chaîne n'existe.

    === "En POO"
	
		- **trouver_influents(self)** : 
			- **Paramètres** : 
				- Aucun.
			- **Retourne** : Liste des pseudos des personnes influentes, c'est-à-dire celles qui ont le plus de suiveurs.
        - **trouver_chaine_message(self, depart, arrivee)** : 
			- **Paramètres** : 
				- `depart` : Chaîne de caractères représentant le pseudo de la personne de départ.
				- `arrivee` : Chaîne de caractères représentant le pseudo de la personne d'arrivée.
			- **Retourne** : Liste des pseudos formant la chaîne de personnes pour faire passer un message de `depart` à `arrivee`, ou `None` si aucune chaîne n'existe.
