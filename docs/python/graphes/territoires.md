# Projet Contrôle de territoires

L'objectif de ce projet est de créer un jeu de contrôle de territoire avec un joueur automatique.
Voici le cahier des charges de votre application / bibliothèque:

- créer un graphe hexagonale de type *ruche* d'un diamètre donné;
- pouvoir ajouter et supprimer des cases et des liens;
- trouver toutes les cases voisines d'une autre;
- modifier qui contrôle une case et stocker l'état de la case;
- calculer le score (nombre de case) d'une joueuse;
- créer une joueuse automatique (aléatoire ou mieux);
- trouver un chemin entre deux cases du graphe.

Pour arriver à ce résultat, vous adopterez une démarche *incrémentale* (faire petit à petit), en implémentant les fonctionnalités dans l'ordre suivant.
À chaque étape, il vous est conseillé de tester vos fonctionnalités en vérifiant que les anciens tests fonctionnent toujours.
Pensez donc à garder vos tests et à souvent exécuter votre fichier !

## Représentation du réseau sous forme de **Graphe**

Voici un exemple de pateau que nous aimerions implémenter:

<svg id="holatex" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="174.07864pt" height="116.60802pt" viewBox="-1.72027 -115.02056 174.07864 116.60802">
	<g stroke-miterlimit="10" transform="translate(21.53955,-57.09155)scale(0.996264,-0.996264)">
		<g stroke="#000" fill="#000">
			<g stroke-width="0.4">
			<path d=" M 10.66977 18.48064 L -10.66977 18.48064 L -21.33955 0.0 L -10.66977 -18.48064 L 10.66977 -18.48064 L 21.33955 0.0 Z  " fill="none"/>
		<g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)">
			<g fill="#000">
				<g stroke="none"> </g> 
			</g>
		</g>
		<g transform="translate(-2.5,-3.22221)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)">
			  <g fill="#000">
			  <g stroke="none"> 
				  <text y="-57.09155" x="21.53955" fill="black" style="font-family: cmr10;" font-size="10">1</text>
			  </g> </g>
		  </g></g>
		  <path d=" M 42.6791 37.68608 L 21.33955 37.68608 L 10.66977 19.20544 L 21.33955 0.72481 L 42.6791 0.72481 L 53.34888 19.20544 Z  " fill="none"/>
		  <g transform="translate(32.00932,19.20544)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> </g> </g>
		  </g></g>
		  <g transform="translate(29.51321,15.98557)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> <text y="-57.09155" x="21.53955" fill="black" style="font-family: cmr10;" font-size="10">2</text>
			  </g> </g>
		  </g></g>
		  <path d=" M 42.6791 -0.72481 L 21.33955 -0.72481 L 10.66977 -19.20544 L 21.33955 -37.68608 L 42.6791 -37.68608 L 53.34888 -19.20544 Z  " fill="none"/>
		  <g transform="translate(32.00932,-19.20544)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> </g> </g>
		  </g></g>
		  <g transform="translate(29.51321,-22.43)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> <text y="-57.09155" x="21.53955" fill="black" style="font-family: cmr10;" font-size="10">3</text>
			  </g> </g>
		  </g></g>
		  <path d=" M 74.68843 18.48064 L 53.34889 18.48064 L 42.67911 0.0 L 53.34889 -18.48064 L 74.68843 -18.48064 L 85.35822 0.0 Z  " fill="none"/>
		  <g transform="translate(64.01866,0.0)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> </g> </g>
		  </g></g>
		  <g transform="translate(61.52646,-3.22221)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> <text y="-57.09155" x="21.53955" fill="black" style="font-family: cmr10;" font-size="10">5</text>
			  </g> </g>
		  </g></g>
		  <path d=" M 74.68843 56.89156 L 53.34889 56.89156 L 42.67911 38.41092 L 53.34889 19.93028 L 74.68843 19.93028 L 85.35822 38.41092 Z  " fill="none"/>
		  <g transform="translate(64.01866,38.41092)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> </g> </g>
		  </g></g>
		  <g transform="translate(61.52646,35.19337)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> <text y="-57.09155" x="21.53955" fill="black" style="font-family: cmr10;" font-size="10">4</text>
			  </g> </g>
		  </g></g>
		  <path d=" M 74.68843 -19.93028 L 53.34889 -19.93028 L 42.67911 -38.41092 L 53.34889 -56.89156 L 74.68843 -56.89156 L 85.35822 -38.41092 Z  " fill="none"/>
		  <g transform="translate(64.01866,-38.41092)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> </g> </g>
		  </g></g>
		  <g transform="translate(61.52646,-41.6378)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> <text y="-57.09155" x="21.53955" fill="black" style="font-family: cmr10;" font-size="10">6</text>
			  </g> </g>
		  </g></g>
		  <path d=" M 106.69775 -0.72481 L 85.35822 -0.72481 L 74.68843 -19.20544 L 85.35822 -37.68608 L 106.69775 -37.68608 L 117.36754 -19.20544 Z  " fill="none"/>
		  <g transform="translate(96.02798,-19.20544)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> </g> </g>
		  </g></g>
		  <g transform="translate(93.53969,-22.43)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> <text y="-57.09155" x="21.53955" fill="black" style="font-family: cmr10;" font-size="10">7</text>
			  </g> </g>
		  </g></g>
		  <path d=" M 106.69775 37.68608 L 85.35822 37.68608 L 74.68843 19.20544 L 85.35822 0.72481 L 106.69775 0.72481 L 117.36754 19.20544 Z  " fill="none"/>
		  <g transform="translate(96.02798,19.20544)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> </g> </g>
		  </g></g>
		  <g transform="translate(93.53969,15.98557)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> <text y="-57.09155" x="21.53955" fill="black" style="font-family: cmr10;" font-size="10">8</text>
			  </g> </g>
		  </g></g>
		  <path d=" M 138.7071 18.48064 L 117.36757 18.48064 L 106.69778 0.0 L 117.36757 -18.48064 L 138.7071 -18.48064 L 149.37689 0.0 Z  " fill="none"/>
		  <g transform="translate(128.03734,0.0)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> </g> </g>
		  </g></g>
		  <g transform="translate(125.55295,-3.22221)">
		  <g stroke="none" transform="scale(-1.00375,1.00375)translate(21.53955,-57.09155)scale(-1,-1)"><g fill="#000">
		  <g stroke="none"> <text y="-57.09155" x="21.53955" fill="black" style="font-family: cmr10;" font-size="10">9</text>
			  </g> </g>
		  </g></g>
		  </g>
			  </g>
			  </g>
</svg>

Sur ce jeu les régles sont les suivantes:

- au début les joueuses contrôlent chacune une case à chaque extrémité;
- à chaque tour, chaque joueuse doit choisir une nouvelle case à contrôler, en respectant les deux règles suivantes:
	- la nouvelle case est adjacente à au moins une case déjà controlée;
	- la nouvelle case **n'est pas adjacente à une case adverse**;
- si une joueuse ne peut pas jouer, elle perd le jeu;
- la derniere joueuse gagne le jeu.

Voici quelques questions à vous poser avant de démarrer ce projet.

!!! question "Dans ce graphe quels sont les noeuds ? Les arêtes ? Le graphe est-il dirigé ? Pondéré ?"

!!! question "Comment représenter ce réseau par une liste d'adjacence ? Est-ce que certaines arêtes doivent être différenciées ?"

!!! question "Comment représenter qu'une case appartient à une joueuse ? Où disposer cette information, dans le graphe ou dans une structure annexe ?"

!!! question "Ce plateau est de diametre 5, en dessinant ceux de diamètre 3 et 7, trouvez comment évolue le nombre et la distribution des noeuds."

Maintenant que vous avez réfléchi à la *mise en situation* de votre graphe, il faut en implémenter la structure.
Pour ceci, vous devez commencer par un premier choix:

- préférez-vous un ensemble de fonctions manipulant une structure de données globales (implémentation impérative ou fonctionnelle)
- ou bien une utilisation sous forme d'objet qui sera modifié et aura des fonctionnalités (POO) ?

!!! note "Exemple d'utilisation"
	
	=== "Fonctionnelle"
	
		```python
		# Exemple d'utilisation
		joueuses = ["Jane", "Sun"]
		diametre = 5
		plateau = creer_plateau(diametre, joueuses)
		visualiser_reseau(plateau)
		# Partie fonctionnalités de jeu
		controler(plateau, 1, "Jane")  # Jane prend possession de la case 2 sur le plateau, uniquement si elle est contrôlable ou qu'elle n'en a pas.
		visualiser_reseau(plateau)
		print(controlables(plateau,"Jane"))
		if peut_jouer(plateau, "Jane"):  # Renvoie True si et seulement si Jane peut contrôler au moins une nouvelle case.
			print("À vous de jouer Jane : ")
			visualiser_reseau(plateau)
			controler(plateau,int(input("Quelle case ?")),"Jane")
		else:
			print("Vous avez perdu Jane")
		```
	
	=== "POO"
	
		```python
		class Joueuse: 
			pass
		class Plateau: 
			pass
		# Exemple d'utilisation
		joueuses = [Joueuse("Jane"), Joueuse("Sun")]  # Création de deux instances de Joueuse
		plateau = Plateau(5,joueuses)  # Création d'une instance de Plateau avec le diamètre
		# Visualiser le plateau
		plateau.visualiser_reseau()
		# Partie fonctionnalités de jeu
		joueuse_actuelle = joueuses[0]  # Jane
		case_a_controler = 2
		# Tentative de contrôle d'une case
		if plateau.controler(case_a_controler, joueuse_actuelle):
			plateau.visualiser_reseau()
		else:
			print(f"{joueuse_actuelle.nom} ne peut pas contrôler la case {case_a_controler}.")
		# Afficher les cases contrôlables
		print(plateau.controlables(joueuse_actuelle))
		# Vérifier si la joueuse peut jouer
		if plateau.peut_jouer(joueuse_actuelle):
			print(f"À vous de jouer {joueuse_actuelle.nom} : ")
			plateau.visualiser_reseau()
			case_choisie = int(input("Quelle case ? "))
			plateau.controler(case_choisie, joueuse_actuelle)
		else:
			print(f"Vous avez perdu {joueuse_actuelle.nom}.")

		```

Pour finaliser cette première partie, vous devez donc être capable de créer un graphe statique et l'afficher en utilisant l'implémentation de la fonction `visualiser_reseau` ci-dessous:

!!! success "Pour visualiser"

	=== "En fonctionnel"
	
		```python
		import networkx as nx
		import matplotlib.pyplot as plt
		
		...
		
		def visualiser_reseau(reseau):
			G = nx.DiGraph()  # Créer un graphe dirigé
			for noeud, suivis in reseau.items():
				for suivi in suivis:
					G.add_edge(noeud, suivi)  # Ajouter une arête pour chaque lien de suivi
	        nx.draw(G, with_labels=True, node_size=2000, node_color='lightblue', font_size=10, font_color='black')
			plt.title("Visualisation du Réseau")
			plt.show()  # Afficher le graphe
		```	
	
	=== "En POO"
	
		```python
		import networkx as nx
		import matplotlib.pyplot as plt
		
		...
		class Plateau:
			...
			def visualiser_reseau(self):
				G = nx.DiGraph()  # Créer un graphe dirigé
				for noeud, suivis in self.reseau.items():
					for suivi in suivis:
						G.add_edge(noeud, suivi)  # Ajouter une arête pour chaque lien de suivi
	            nx.draw(G, with_labels=True, node_size=2000, node_color='lightblue', font_size=10, font_color='black')
				plt.title("Visualisation du Réseau")
				plt.show()  # Afficher le graphe
		```	

## Implémentation des fonctionnalités

Pour ce projet, vous devez implémenter les fonctionnalités suivantes.

!!! note "Interface à implémenter"

	=== "En fonctionnel"
	
		- **creer_plateau(diametre, joueuses)** : 
			- **Paramètres** : 
				- `diametre` : Entier représentant le diamètre du plateau de jeu.
				- `joueuses` : Liste de chaînes de caractères représentant les noms des joueuses à ajouter au plateau.
			- **Retourne** : Un objet représentant le plateau de jeu initialisé avec les joueuses.
		- **visualiser_reseau(plateau)** : 
			- **Paramètres** : 
				- `plateau` : Objet représentant le plateau de jeu.
			- **Retourne** : Rien. Affiche l'état actuel du plateau de jeu.
		- **controler(plateau, case, joueuse)** : 
			- **Paramètres** : 
				- `plateau` : Objet représentant le plateau de jeu.
				- `case` : Entier représentant la case que la joueuse souhaite contrôler.
				- `joueuse` : Chaîne de caractères représentant le nom de la joueuse qui tente de contrôler la case.
			- **Retourne** : Booléen. Indique si la joueuse a réussi à contrôler la case.
		- **controlables(plateau, joueuse)** : 
			- **Paramètres** : 
				- `plateau` : Objet représentant le plateau de jeu.
				- `joueuse` : Chaîne de caractères représentant le nom de la joueuse pour laquelle on souhaite connaître les cases contrôlables.
			- **Retourne** : Liste d'entiers représentant les cases que la joueuse peut contrôler.
		- **peut_jouer(plateau, joueuse)** : 
			- **Paramètres** : 
				- `plateau` : Objet représentant le plateau de jeu.
				- `joueuse` : Chaîne de caractères représentant le nom de la joueuse à vérifier.
			- **Retourne** : Booléen. Indique si la joueuse peut encore jouer.

	=== "En POO"
	
		- **creer_plateau(self, diametre, joueuses)** : 
			- **Paramètres** : 
				- `diametre` : Entier représentant le diamètre du plateau de jeu.
				- `joueuses` : Liste d'objets `Joueuse` à ajouter au plateau.
			- **Retourne** : Rien. Initialise le plateau de jeu avec les joueuses.
		- **visualiser_reseau(self)** : 
			- **Retourne** : Rien. Affiche l'état actuel du plateau de jeu.
		- **controler(self, case, joueuse)** : 
			- **Paramètres** : 
				- `case` : Entier représentant la case que la joueuse souhaite contrôler.
				- `joueuse` : Objet représentant la joueuse qui tente de contrôler la case.
			- **Retourne** : Booléen. Indique si la joueuse a réussi à contrôler la case.
		- **controlables(self, joueuse)** : 
			- **Paramètres** : 
				- `joueuse` : Objet représentant la joueuse pour laquelle on souhaite connaître les cases contrôlables.
			- **Retourne** : Liste d'entiers représentant les cases que la joueuse peut contrôler.
		- **peut_jouer(self, joueuse)** : 
			- **Paramètres** : 
				- `joueuse` : Objet représentant la joueuse à vérifier.
			- **Retourne** : Booléen. Indique si la joueuse peut encore jouer.

## Implémentation du jeu

Maintenant que vous avez toutes les fonctionnalités, vous pouvez implémenter votre jeu.

Un pseudocode habituel pour ce genre de jeu est:

```
Créer la liste des joueuses (en précisant si c'est une IA).
Créer le plateau d'une taille donnée.

Tant que au moins une joueuse peut jouer:
	Afficher le plateau
	Si la joueuse n'est pas une IA:
		lui demander quelle case elle joue
		si c'est possible jouer cette case sur le plateau
		sinon redemander tant que la case n'est pas valide
	Sinon
		choisir une case au hasard ou en suivant une stratégie
		parmi toutes les cases contrôlables.
		Jouer cette case
	Passer à la prochaine joueuse pouvant jouer.
```

Voici un extrait de code permettant d'avoir un affichage interactif qui se met à jour automatiquement:

```python
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

# Activer le mode interactif
plt.ion()

# Créer un graphe vide
G = nx.Graph()

# Ajouter des nœuds et des arêtes initiales
G.add_nodes_from(range(5))
G.add_edges_from([(0, 1), (1, 2), (2, 3), (3, 4)])

# Fonction pour dessiner le graphe
def draw_graph(G):
    plt.clf()  # Effacer la figure actuelle
    pos = nx.spring_layout(G)  # Positionnement des nœuds
    nx.draw(G, pos, with_labels=True, node_color='lightblue', node_size=700, font_size=16)
    plt.title("Graphe NetworkX")

# Dessiner le graphe initial
draw_graph(G)

# Compteur de nœuds
node_count = 5

# Boucle pour ajouter des nœuds
add_node = input("Voulez-vous ajouter un nœud ? (o/n) : ")
while add_node.lower() == 'o':
    if add_node.lower() == 'o':
        G.add_node(node_count)  # Ajouter un nouveau nœud
        # Ajouter une arête entre le nouveau nœud et un nœud existant (par exemple, le nœud 0)
        G.add_edge(node_count, 0)
        node_count += 1  # Incrémenter le compteur de nœuds
        draw_graph(G)  # Redessiner le graphe
		add_node = input("Voulez-vous ajouter un nœud ? (o/n) : ")
    elif add_node.lower() == 'n':
        print("Sortie de la boucle.")
    else:
        print("Veuillez répondre par 'o' ou 'n'.")
		add_node = input("Voulez-vous ajouter un nœud ? (o/n) : ")

# Garder la fenêtre ouverte
plt.ioff()  # Désactiver le mode interactif (facultatif, si vous voulez garder le mode interactif, vous pouvez omettre cette ligne)
plt.show()  # Afficher la figure finale
```
