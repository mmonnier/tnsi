# Projet Chemin de Mots

L'objectif de ce projet est de simuler un jeu permettant de trouver un chemin entre deux mots donnés de même longueur.

Voici le cahier des charges de votre application / bibliothèque:

- ajouter une liste de mots dans un graphe;
- créer automatiquement les liens entre ces mots si une seule lettre diffère;
- trouver tous les mots qui sont *dérivables* depuis un mot donné:
- visualiser le réseau graphiquement;
- trouver une **chaine** de mots pour transformer un mot de départ vers un mot d'arrivée;
- trouver les différentes mots accessibles en un nombre quelconque d'étapes. depuis un mot donné.
- trouver les mot les plus liés.

Pour arriver à ce résultat, vous adopterez une démarche *incrémentale* (faire petit à petit), en implémentant les fonctionnalités dans l'ordre suivant.
À chaque étape, il vous est conseillé de tester vos fonctionnalités en vérifiant que les anciens tests fonctionnent toujours.
Pensez donc à garder vos tests et à souvent exécuter votre fichier !

## Représentation du réseau sous forme de **Graphe**

Voici un exemple de réseau de mots que nous aimerions représenter.

```mermaid
graph LR;

CAGE --> RAGE & SAGE & NAGE & CAFE;
SAGE --> RAGE & CAGE & NAGE;
RAGE --> CAGE & SAGE & NAGE;
NAGE --> CAGE & SAGE & RAGE;
CAFE --> CAGE;
MAIN --> MAIL;
MAIL --> MAIN;
```

Voici quelques questions à vous poser avant de démarrer ce projet.

!!! question "Dans ce graphe quels sont les noeuds ? Les arêtes ? Le graphe est-il dirigé ? Pondéré ?"

!!! question "Comment représenter ce réseau par une liste d'adjacence ? Est-ce que certaines arêtes doivent être différenciées ?"

!!! question "Répondez à ces mêmes questions pour un réseau similaire de votre choix"

Maintenant que vous avez réfléchi à la *mise en situation* de votre graphe, il faut en implémenter la structure.
Pour ceci, vous devez commencer par un premier choix:

- préférez-vous un ensemble de fonctions manipulant une structure de données globales (implémentation impérative ou fonctionnelle)
- ou bien une utilisation sous forme d'objet qui sera modifié et aura des fonctionnalités (POO) ?

!!! note "Exemple d'utilisation"
	
	=== "Fonctionnelle"
	
		```python
		# Initialisation du graphe
		graphe_mots = {}
		
		# Ajout de mots de longueur 4 en français
		ajouter_mots(graphe_mots, ["chat", "plat", "raté", "bate", "bate", "lait", "fait", "sait"])
		
		# Création des liens
		creer_liens(graphe_mots)
		
		# Trouver les mots dérivables depuis "chat"
		derivables = trouver_derivables(graphe_mots, "chat")
		print("Mots dérivables depuis 'chat':", derivables)
		
		# Visualiser le réseau
		visualiser_reseau(graphe_mots)
		
		# Trouver une chaîne de mots de "chat" à "plat"
		chaine = trouver_chaine(graphe_mots, "chat", "plat")
		print("Chaîne de mots de 'chat' à 'plat':", chaine)
		```
	
	=== "POO"
	
		```python
		class GrapheMots:
			pass
		# Utilisation de la classe
		graphe = GrapheMots()
		# Ajout de mots de longueur 4 en français
		graphe.ajouter_mots(["chat", "plat", "raté", "bate", "lait", "fait", "sait"])
		# Création des liens
		graphe.creer_liens()
		
		# Trouver les mots dérivables depuis "chat"
		derivables = graphe.trouver_derivables("chat")
		print("Mots dérivables depuis 'chat':", derivables)
		
		# Visualiser le réseau
		graphe.visualiser_reseau()
		
		# Trouver une chaîne de mots de "chat" à "plat"
		chaine = graphe.trouver_chaine("chat", "plat")
		print("Chaîne de mots de 'chat' à 'plat':", chaine)		
		```

Pour finaliser cette première partie, vous devez donc être capable de créer un graphe statique et l'afficher en utilisant l'implémentation de la fonction `visualiser_reseau` ci-dessous:

!!! success "Pour visualiser"

	=== "En fonctionnel"
	
		```python
		import networkx as nx
		import matplotlib.pyplot as plt
		
		...
		
		def visualiser_reseau(reseau):
			G = nx.DiGraph()  # Créer un graphe dirigé
			for mot, derives in reseau.items():
				for derive in derives:
					G.add_edge(mot, derive)  # Ajouter une arête pour chaque dérivation
	        nx.draw(G, with_labels=True, node_size=2000, node_color='lightblue', font_size=10, font_color='black')
			plt.title("Visualisation du Réseau des mots")
			plt.show()  # Afficher le graphe
		```	
	
	=== "En POO"
	
		```python
		import networkx as nx
		import matplotlib.pyplot as plt
		
		...
		class GrapheMots:
			...
			def visualiser_reseau(self):
				G = nx.DiGraph()  # Créer un graphe dirigé
				for mot, derives in self.items():
					for derive in derives:
						G.add_edge(mot, derive)  # Ajouter une arête pour chaque dérivation
	            nx.draw(G, with_labels=True, node_size=2000, node_color='lightblue', font_size=10, font_color='black')
				plt.title("Visualisation du Réseau de mots")
				plt.show()  # Afficher le graphe
		```	

## Implémentation de fonctionnalités d'ajouts

Pour cette partie vous devez implémenter les fonctionnalités suivantes:

!!! note "Interface à implémenter"

	=== "En fonctionnel"
	
		- **ajouter_liste_mots(reseau, liste_mots)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau de mots.
				- `liste_mots` : Une liste de mots de même longueurs que les mots déjà présents.
			- **Retourne** : Rien. Ajoute tout les mots de même longueur.
		- **creer_liens(reseau)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau de mots.
			- **Retourne** : Rien. Modifie le réseau pour ajouter toutes les arêtes manquantes.

	=== "En POO"
	
		- **ajouter_liste_mots(self, liste_mots)** : 
			- **Paramètres** : 
				- `liste_mots` : Une liste de mots de même longueurs que les mots déjà présents.
			- **Retourne** : Rien. Ajoute tout les mots de même longueur.
		- **creer_liens(self)** : 
			- **Paramètres** : Aucun.
			- **Retourne** : Rien. Modifie le réseau pour ajouter toutes les arêtes manquantes.

## Implémentation de fonctionnalités de relation (voisins, entrants, sortants).

!!! note "Interface à implémenter"

    === "En fonctionnel"
	
        - **trouver_derivables(reseau, mot)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau de mots.
				- `mot` : Chaîne de caractères représentant le mot dont on veut trouver les suivants.
			- **Retourne** : Liste des mots qui sont dérivables depuis le mot donné.
		- **trouver_derivables_transitif(reseau, mot, etapes)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau de mots.
				- `mot` : Chaîne de caractères représentant le mot dont on veut trouver les suivants.
				- `etapes` : Entier représentant le nombre de dérivations maximales à faire
			- **Retourne** : Liste des mots qui sont dérivables depuis le mot donné en un nombre d'étapes maximum.
    === "En POO"
	
        - **trouver_derivables(self, mot)** : 
			- **Paramètres** : 
				- `mot` : Chaîne de caractères représentant le mot dont on veut trouver les suivants.
			- **Retourne** : Liste des mots qui sont dérivables depuis le mot donné.
		- **trouver_derivables_transitif(self, mot, etapes)** : 
			- **Paramètres** : 
				- `mot` : Chaîne de caractères représentant le mot dont on veut trouver les suivants.
				- `etapes` : Entier représentant le nombre de dérivations maximales à faire
			- **Retourne** : Liste des mots qui sont dérivables depuis le mot donné en un nombre d'étapes maximum.

## Implémentation des fonctionnalités de parcours


!!! note "Interface à implémenter"

    === "En fonctionnel"
	
		- **trouver_centraux(reseau)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau.
			- **Retourne** : Liste des mots centraux, c'est-à-dire ceux qui emmenent vers le plus d'autre mots.
        - **trouver_chaine_message(reseau, depart, arrivee)** : 
			- **Paramètres** : 
				- `reseau` : Dictionnaire représentant le réseau.
				- `depart` : Chaîne de caractères représentant le mot de départ.
				- `arrivee` : Chaîne de caractères représentant le mot d'arrivée.
			- **Retourne** : Liste des mots formant la chaîne pour transformer `depart` en `arrivee`, ou `None` si aucune chaîne n'existe.

    === "En POO"
	
		- **trouver_centraux(self)** : 
			- **Paramètres** : Aucun
			- **Retourne** : Liste des mots centraux, c'est-à-dire ceux qui emmenent vers le plus d'autre mots.
        - **trouver_chaine_message(self, depart, arrivee)** : 
			- **Paramètres** :
				- `depart` : Chaîne de caractères représentant le mot de départ.
				- `arrivee` : Chaîne de caractères représentant le mot d'arrivée.
			- **Retourne** : Liste des mots formant la chaîne pour transformer `depart` en `arrivee`, ou `None` si aucune chaîne n'existe.
