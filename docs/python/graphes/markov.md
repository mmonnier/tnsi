# Projet Génération de Textes avec des Chaînes de Markov

L'objectif de ce projet est d'implémenter un générateur de textes basé sur des chaînes de Markov.
Ce générateur utilisera un corpus de texte pour créer de nouveaux textes en se basant sur les probabilités de transition entre les mots.

Pour faire ceci, on pourra se baser sur les ressources suivantes:

- <https://fr.wikipedia.org/wiki/Cha%C3%AEne_de_Markov>
- <https://www.youtube.com/watch?v=YsR7r2378j0>

Pour des informations sur les chaînes de Markov, vous pouvez consulter des ressources en ligne.

Voici le cahier des charges de votre application / bibliothèque :

- ajouter un corpus de texte au modèle ;
- construire un modèle de transition entre les mots ;
- générer un texte à partir d'un mot de départ ;
- visualiser le réseau de mots graphiquement ;
- trouver un chemin entre deux mots dans le modèle.

Pour arriver à ce résultat, vous adopterez une démarche *incrémentale* (faire petit à petit), en implémentant les fonctionnalités dans l'ordre suivant.
À chaque étape, il vous est conseillé de tester vos fonctionnalités en vérifiant que les anciens tests fonctionnent toujours.

## Représentation du modèle sous forme de **Graphe**

```mermaid
graph TD;
    je -->|0.5| suis;
    je -->|0.5| mange;
    suis -->|0.1| je;
    suis -->|0.8| ici;
	suis -->|0.1| bien;
    mange -->|1.0| bien;
```

Voici quelques questions à vous poser avant de démarrer ce projet.

!!! question "Dans ce modèle, quels sont les noeuds ? Les arêtes ? Le graphe est-il dirigé ? Pondéré ?"

!!! question "Comment représenter ce modèle par une liste d'adjacence ? Est-ce que certaines arêtes doivent être différenciées ?"

!!! question "Répondez à ces mêmes questions pour un modèle similaire de votre choix"

Maintenant que vous avez réfléchi à la *mise en situation* de votre modèle, il faut en implémenter la structure. Pour ceci, vous devez commencer par un premier choix :

- préférez-vous un ensemble de fonctions manipulant une structure de données globales (implémentation impérative ou fonctionnelle)
- ou bien une utilisation sous forme d'objet qui sera modifié et aura des fonctionnalités (POO) ?

!!! note "Exemple d'utilisation"

	=== "Fonctionnel"
		```python
		# Initialisation du modèle de texte
		modele_texte = {}
		
	    # Exemple d'utilisation
		ajouter_corpus(modele_texte, "Ceci est un exemple de texte. Ceci est un autre exemple.")
		
		# Construire le modèle de transition
		construire_modele(modele_texte)
		
		# Visualiser le réseau de mots
		print("Réseau de mots :")
		visualiser_reseau(modele_texte)
		
		# Générer un texte à partir d'un mot de départ
		texte_genere = generer_texte(modele_texte, "Ceci", 10)
		print("Texte généré :", texte_genere)
		
		# Trouver et afficher un chemin.
	    chemin = trouver_chemin_mots(modele_texte,"Ceci","Texte")
		print("Un chemin entre 'Ceci' et 'Texte' est :", chemin)
		```
		
	=== "POO"
		```python
		class ModeleTexte:
			pass
			
		# Exemple d'utilisation
		modele = ModeleTexte()
		modele.ajouter_corpus("Ceci est un exemple de texte. Ceci est un autre exemple.")
		
		# Construire le modèle de transition
		modele.construire_modele()
		
		# Visualiser le réseau de mots
		print("Réseau de mots :")
		modele.visualiser_reseau()
		
		# Générer un texte à partir d'un mot de départ
		texte_genere = modele.generer_texte("Ceci", 10)
		print("Texte généré :", texte_genere)
		
		# Trouver et afficher les mots isolés
		chemin = modele.trouver_chemin_mots("Ceci","Texte")
		print("Un chemin entre 'Ceci' et 'Texte' est :", chemin)
		```

Pour finaliser cette première partie, vous devez donc être capable de créer un modèle statique et l'afficher en utilisant l'implémentation de la fonction `visualiser_reseau` ci-dessous :

!!! success "Pour visualiser"

	=== "En fonctionnel"	
		```python
		import networkx as nx
		import matplotlib.pyplot as plt
		
		...
		
		def visualiser_reseau(modele):
			G = nx.DiGraph()  # Créer un graphe dirigé
			for mot, voisins in modele.items():
				for voisin, poids in voisins.items():
					G.add_edge(mot, voisin, weight=poids)  # Ajouter une arête pour chaque transition
	        nx.draw(G, with_labels=True, node_size=2000, node_color='lightblue', font_size=10, font_color='black')
			plt.title("Visualisation du Réseau de mots")
			plt.show()  # Afficher le graphe
		```	
	=== "En POO"
	
		```python
		import networkx as nx
		import matplotlib.pyplot as plt
		
		...
		
		def visualiser_reseau(self):
			G = nx.DiGraph()  # Créer un graphe dirigé
			for mot, voisins in self.modele.items():
				for voisin, poids in voisins.items():
					G.add_edge(mot, voisin, weight=poids)  # Ajouter une arête pour chaque transition
	        nx.draw(G, with_labels=True, node_size=2000, node_color='lightblue', font_size=10, font_color='black')
			plt.title("Visualisation du Réseau de mots")
			plt.show()  # Afficher le graphe
		```	
	
## Implémentation de fonctionnalités d'ajout

!!! note "Interface à implémenter"

	=== "En fonctionnel"
	
		- **ajouter_corpus(modele, texte)** : 
			- **Paramètres** : 
				- `modele` : Dictionnaire représentant le modèle de texte.
				- `texte` : Chaîne de caractères représentant le corpus de texte à ajouter.
			- **Retourne** : Rien. Ajoute le texte au modèle.
		
	=== "En POO"
	
		- **ajouter_corpus(self, texte)** : 
			- **Paramètres** : 
				- `texte` : Chaîne de caractères représentant le corpus de texte à ajouter.
			- **Retourne** : Rien. Ajoute le texte au modèle.

## Implémentation de la construction du modèle de transition

!!! note "Interface à implémenter"

	=== "En fonctionnel"
	
		- **construire_modele(modele)** : 
			- **Paramètres** : 
				- `modele` : Dictionnaire représentant le modèle de texte.
			- **Retourne** : Rien. Modifie le modèle pour construire les transitions entre les mots.
		
	=== "En POO"
	
		- **construire_modele(self)** : 
			- **Paramètres** : Aucun.
			- **Retourne** : Rien. Modifie le modèle pour construire les transitions entre les mots.

## Implémentation de la génération de texte

!!! note "Interface à implémenter"

	=== "En fonctionnel"
	
		- **generer_texte(modele, mot_depart, longueur)** : 
			- **Paramètres** : 
				- `modele` : Dictionnaire représentant le modèle de texte.
				- `mot_depart` : Chaîne de caractères représentant le mot de départ pour la génération.
				- `longueur` : Entier représentant le nombre de mots à générer.
			- **Retourne** : Chaîne de caractères représentant le texte généré.
		
	=== "En POO"
	
		- **generer_texte(self, mot_depart, longueur)** : 
			- **Paramètres** : 
				- `mot_depart` : Chaîne de caractères représentant le mot de départ pour la génération.
				- `longueur` : Entier représentant le nombre de mots à générer.
			- **Retourne** : Chaîne de caractères représentant le texte généré.

## Implémentation des fonctionnalités de recherche

!!! note "Interface à implémenter"

	=== "En fonctionnel"
	
		- **trouver_chemin(modele,depart,arrivee)** : 
			- **Paramètres** : 
				- `modele` : Dictionnaire représentant le modèle de texte.
				- `depart` : le mot de départ sous forme de chaine de caractere.
				- `arrivee`: le mot d'arrivée sous forme de chaine de caractere.
			- **Retourne** : Liste des mots qui forme le chemin.
		
	=== "En POO"
	
		- **trouver_chemin(self,depart,arrivee)** : 
			- **Paramètres** :
				- `depart` : le mot de départ sous forme de chaine de caractere.
				- `arrivee`: le mot d'arrivée sous forme de chaine de caractere.
			- **Retourne** : Liste des mots qui forme le chemin.
