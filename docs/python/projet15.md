# Programme Grotte verticale

Ce programme est une animation d'une grotte qui s'enfonce sans fin dans la terre.
Bien que court, ce programme exploite le terminal pour produire une visualisation intéressante et sans fin, preuve que peu de code suffit à produire quelque chose d'amusant à regarder.

## Le programme en action

Lorsque vous l'exécutez, la sortie ressemblera à ceci :

```text
Grotte verticale
Appuyez sur Ctrl-C pour arrêter.
####################          ########################################
####################         #########################################
####################          ########################################
####################          ########################################
#####################          #######################################
######################          ######################################
#####################          #######################################
####################          ########################################
###################          #########################################
...
```

## Comment ça marche

Ce programme tire parti du fait qu'imprimer de nouvelles lignes finit par faire remonter les lignes précédentes à l'écran. 
En imprimant un léger décalage sur chaque ligne, le programme crée une animation de défilement qui donne l'impression que le spectateur se déplace vers le bas.

La largeur de la ligne de gauche est suivie par la variable `leftWidth`. Le nombre d'espaces au milieu est suivi par la variable `gapWidth`.
Le nombre de caractères dièse sur la droite est calculé à partir de `WIDTH - gapWidth - leftWidth`.
Cela garantit que chaque ligne a toujours la même largeur.

## Le programme

```python
import random, sys, time

# Les constantes:
WIDTH = 70  # (!) Essayez les valeurs 10 ou 30.
PAUSE_AMOUNT = 0.05  # (!) Essayez les valeurs 1.0 ou 0

print('Grotte verticale')
print('Appuyez sur Ctrl-C pour arrêter.')
time.sleep(2)

leftWidth = 20

while True:
    # Afficher le tunnel
    rightWidth = WIDTH - gapWidth - leftWidth
    print(('#' * leftWidth) + (' ' * gapWidth) + ('#' * rightWidth))
    # Verifier Ctrl-C pendant la pause
    try:
        time.sleep(PAUSE_AMOUNT)
    except KeyboardInterrupt:
        sys.exit()  # On ferme le programme
    # La taille de la partie gauche est aléatoirement modifiée
    diceRoll = random.randint(1, 6)
    if diceRoll == 1 and leftWidth > 1:
        leftWidth = leftWidth - 1  # Diminuer la partie gauche
    elif diceRoll == 2 and leftWidth + gapWidth < WIDTH - 1:
        leftWidth = leftWidth + 1  # Augmenter la partie gauche
    # La taille du tunnel est aléatoirement modifiée
    # (!) Essayez en décommentant le code suivant
    #diceRoll = random.randint(1, 6)
    #if diceRoll == 1 and gapWidth > 1:
    #    gapWidth = gapWidth - 1  # Diminuer le tunnel
    #elif diceRoll == 2 and leftWidth + gapWidth < WIDTH - 1:
    #    gapWidth = gapWidth + 1  # Augmenter le tunnel
```

Après avoir saisi le code source et l'avoir exécuté plusieurs fois, essayez de le modifier.
Les commentaires marqués d'un (!) proposent de petits changements que vous pouvez apporter.

## Exploration du programme

Essayez de répondre aux questions suivantes.
Expérimentez en apportant des modifications au code et relancez le programme pour voir les effets de ces modifications.

1. Que se passe-t-il si vous remplacez (' ' * gapWidth) à la ligne 12 par ('.' * gapWidth) ?
2. Que se passe-t-il si vous remplacez random.randint(1, 6) à la ligne 19 par random.randint(1, 1) ?
3. Que se passe-t-il si vous remplacez random.randint(1, 6) à la ligne 19 par random.randint(2, 2) ?
4. Que se passe-t-il si vous supprimez ou que vous commentez leftWidth = 20 à la ligne 11 ?
5. Que se passe-t-il si vous modifiez WIDTH = 70 à la ligne 4 en WIDTH = -70 ?
6. Que se passe-t-il si vous modifiez PAUSE_AMOUNT = 0.05 à la ligne 5 en PAUSE_AMOUNT = -0.05 ?
