import socketserver

def itob(n:int): return bytes([n])
def btoi(b:bytes): return int.from_bytes(b)

class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # self.request is the TCP socket connected to the client
        # self.data est le premier octet -> Le type de message
        self.data = self.request.recv(1)
        while self.data != b'E':
            ## DEBUG
            print(f"Received from {self.client_address[0]}")
            print(f"Received <{self.data}>")
            ## DEBUG
            
            if self.data == b'M':
                n = btoi(self.request.recv(1))
                message = self.request.recv(n).decode('ascii')
                print(f"Message reçu {message}")
                liste_messages.append(message)
            elif self.data == b'L':
                print(f"Liste des messages demandés {liste_messages}")
                self.request.sendall(itob(len(liste_messages)))
                for m in liste_messages:
                    self.request.sendall(itob(len(m))+m.encode('ascii'))
            elif self.data == b'R':
                n = btoi(self.request.recv(1))
                print(f"Suppression du message {n}")
                del liste_messages[n]

            self.data = self.request.recv(1)
            ## DEBUG
            print(f"End of request")

liste_messages = []

if __name__ == "__main__":
    HOST, PORT = "localhost", 9999

    # Create the server, binding to localhost on port 9999
    with socketserver.TCPServer((HOST, PORT), MyTCPHandler) as server:
        # Activate the server; this will keep running until you
        # interrupt the program with Ctrl-C
        server.serve_forever()
