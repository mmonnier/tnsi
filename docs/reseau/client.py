# Echo client program
import socket

HOST = 'localhost'    # The remote host
PORT = 9999           # The same port as used by the server


def itob(n:int): return bytes([n])
def btoi(b:bytes): return int.from_bytes(b)


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    print('Requete 1')
    s.sendall('M'.encode('ascii')+itob(7)+'bonjour'.encode('ascii'))
    print('Requete 2')
    s.sendall('L'.encode('ascii'))
    n = btoi(s.recv(1))
    print(n,type(n))
    for _ in range(n):
        n = btoi(s.recv(1))
        print(s.recv(n).decode('ascii'))
    s.sendall('E'.encode('ascii'))
    print("END")
