---
author: Marius Monnier
title: Exercices sur les arbres
---

# Les arbres

## Cours

Le diaporama sur les **structures de données hiérarchisées**: [ici](/tnsi/diapo_arbres.pdf).

## Implémentations

Voici trois implémentations des arbres binaires, sous formes de **listes**, de **dictionnaires** et en **POO**.

??? tip "Implémentation des arbres en listes"
    ```python
    def arbre(racine,sag=None,sad=None):
        if sag == None:
            sag = []
        if sad == None:
            sad = []
        return [racine,sag,sad]

    def est_vide(arbre: list) -> bool:
        return arbre == []
    def est_feuille(arbre: list) -> bool:
        if est_vide(arbre):
            return False
        return est_vide(arbre[1]) and est_vide(arbre[2])

    def hauteur(arbre: list) -> int:
        if est_vide(arbre):
            return 0
        return 1 + max(hauteur(arbre[1]), hauteur(arbre[2]))

    def taille(arbre: list) -> int:
        if est_vide(arbre):
            return 0
        return 1 + taille(arbre[1]) + taille(arbre[2])

    def nombre_feuilles(arbre: list) -> int:
        if est_vide(arbre):
            return 0
        elif est_feuille(arbre):
            return 1
        return nombre_feuille(arbre[1])+nombre_feuilles(arbre[2])
    ```

??? tip "Implémentation des arbres en dictionnaires"
    ```python
    def arbre(racine,sag=None,sad=None):
        return {"racine":racine,"gauche":sag,"droite":sad}

    def est_vide(arbre: dict) -> bool:
        return arbre is None
    def est_feuille(arbre: dict) -> bool:
        if est_vide(arbre):
            return False
        return est_vide(arbre["sag"]) and est_vide(arbre["sad"])

    def hauteur(arbre: dict) -> int:
        if est_vide(arbre):
            return 0
        return 1 + max(hauteur(arbre["sag"]), hauteur(arbre["sad"]))

    def taille(arbre: dict) -> int:
        if est_vide(arbre):
            return 0
        return 1 + taille(arbre["sag"]) + taille(arbre["sad"])

    def nombre_feuilles(arbre: list) -> int:
        if est_vide(arbre):
            return 0
        elif est_feuille(arbre):
            return 1
        return nombre_feuille(arbre["sag"])+nombre_feuilles(arbre["sag"])
    ```

??? tip "Implémentation des arbres en POO"
    ```python
    class ArbreB:
        """
        L'arbre vide est représenté par None.
        Un arbre non vide est constitué de trois attributs:
            - racine (qui contient l'étiquette du noeud)
            - gauche (le sous arbre gauche éventuellement vide)
            - droite (le sous arbre droit éventuellement vide)
        
        Pour tester qu'un arbre est vide on utilisera: arbre is None.
        Cette classe ne permettra pas de récupérer la taille, la hauteur ou le nombre de feuilles d'un arbre vide.
        """
        def __init__(self,etiquette,sag=None,sad=None):
            """si sag et sad ne sont pas précisés gauche et droite sont initialisés à None"""
            self.racine = etiquette
            self.gauche = sag
            self.droite = sad

        def est_feuille(self):
            """Renvoie vrai si et seulement si les deux enfants sont vides"""
            return self.gauche == None and self.droite == None

        def hauteur(self):
            """Renvoie la longueur de la plus grande branche de l'arbre"""
            if self.gauche is not None:
                h1 = self.gauche.hauteur()
            else:
                h1 = 0

            if self.droite is not None:
                h2 = self.droite.hauteur()
            else:
                h2 = 0
            return 1 + max(h1,h2)


        def taille(self):
            """Renvoie le nombre total de noeuds de l'arbre"""
            if self.gauche is not None:
                t1 = self.gauche.taille()
            else:
                t1 = 0

            if self.droite is not None:
                t2 = self.droite.taille()
            else:
                t2 = 0
            return 1 + t1 + t2

        def nombre_feuilles(self):
            """Renvoie le nombre de feuilles de l'arbre"""
            if self.est_feuille():
                return 1

            if self.gauche is not None:
                f1 = self.gauche.nombre_feuilles()
            else:
                f1 = 0
            
            if self.droite is not None:
                f2 = self.droite.nombre_feuilles()
            else:
                f2 = 0

            return f1 + f2
    ```

Ces trois implémentations sont toujours basés sur les mêmes propriétés:

- la taille d'un arbre correspond à la somme des tailles de ses sous arbres plus un pour compter la racine
- la hauteur d'un arbre correspond au maximum des hauteurs de ses sous arbres plus un pour compter la racine
- le nombre de feuilles d'un arbre correspond à la somme du nombre de feuilles de chacun de ses sous arbres.


## Activités

### Vocabulaire

#### En situation

Après exécution d'une commande dans un terminal Linux on a obtenu l'affichage ci-dessous :

```
.
├── Maths
├── NSI
│   ├── Cours
│   │   └── Systèmes
│   ├── Exercices
│   │   └── Python
│   ├── Histoire
│   └── Projet
└── SVT
```


1. De quelle commande s'agissait-il ? Quelle est sa traduction en français ?
2. Que sont `Maths` et `NSI` pour le système d'exploitation ?
3. Un point (**.**) figure en haut du schéma, quelle en est la signification ?
4. Que signifie le lien entre `NSI` et `Projet` ?

#### En terme de structure

Le schéma ci-dessus est un exemple d'**arbre** (on parle d'ailleurs de l'arborescence des dossiers dans un système d'exploitation).
Un arbre est constitué de **noeuds** et un lien entre deux noeuds s'appelle une **arête**.

1. Citer trois noeuds de l'arbre ci-dessus et donner un exemple d'arête.
2. Quel noeud est le répertoire parent de `Systèmes`  ? On dira dans le vocabulaire des arbres que le noeud `Systèmes` est un **enfant** de ce noeud.
3. Citer tous les enfants du noeud `NSI`.
4. Dans un arbre, un seul et unique noeud n'est l'enfant de personne, on l'appelle la **racine** de l'arbre. De qui s'agit-il ici ?
5. Nommer les noeuds n'ayant aucun enfant (on les appelle **feuilles** de l'arbre).
6. Une **branche** est une suite (finie) de noeuds depuis la racine vers une feuille. Donner une branche de cette arbre constituée de trois noeuds.
7. Donner la **taille** de cet arbre (c'est à dire son nombre de noeuds).
8. Donner l'**arité** de cet arbre, c'est à dire le nombre maximal d'enfant qu'un noeud peut avoir.
9. Donner la **hauteur** de cet arbre c'est à dire le nombre maximal de noeuds dans une branche.

!!! danger "Vocabulaire"
	La hauteur d'un arbre est parfois définie comme le plus grand nombre d'arêtes dans une branche. Dans les sujets de **bac** faire attention à la définition utilisée qui est normalement donnée dans l'énoncé.

	De même on parle souvent des **enfants** et des **pères** au lieu des **enfants** et des **parents**.

### Arbre Binaire

Chez les abeilles, le système de reproduction fait que :

* une abeille femelle est issue de deux abeilles, un mâle et une femelle,
* une abeille mâle est issue d'une seule abeille femelle.

!!! tip "Un point SVT"
	Cela s'explique par le fait qu'un oeuf non fécondé (donc issu uniquement d'une femelle) donne toujours naissance à une abeille mâle, alors qu'une oeuf fécondé (donc issu d'un mâle et d'une femelle) donne toujours naissance à une abeille femelle.
	
On a représenté ci-contre les quatre premiers niveaux de l'arbre généalogique d'une abeille mâle en notant avec la lettre **M** les mâles et la lettre **F** les femelles.

```mermaid
    graph TD
    F0["F"] --> F1["F"]
    F0["F"] --> M1["M"]
    M1 --> F2["F"]
    F1 --> M2["M"]
    F1 --> F3["F"]
    F2 --> F4["F"]
    F2 --> M3["M"]
    M2 --> F5["F"]
    F3 --> F6["F"]
    F3 --> M4["M"]
```


#### Définition des arbres binaires

1. Recopier et compléter cet arbre en ajoutant le 5^e^ niveau.
2. Rappeler la définition de l'*arité* d'un arbre et d'un noeud. Quelle est l'arité des feuilles d'un arbre ?
3. Déterminer le degré (arité) des noeuds qui ne sont pas des feuilles selon que le noeud représente une abeille mâle ou une abeille femelle.
4. On appelle **arbre binaire**, un arbre dans lequel les noeuds ont au maximum deux enfants. Donner une définition équivalente utilisant le mot *arité* et justifier rapidement que l'arbre généalogique d'une abeille est binaire.

#### Une définition récursive

On reprend l'exemple de l'arbre généalogique d'une abeille femelle jusqu'au cinquième niveau dessinée ci-dessus.
On appelle *sous arbre gauche* et *sous arbre droit* l'arbre généalogique de chacune des deux parents de la racine.
L'arbre est alors noté sous la forme d'un triplet constitué de la racine et des deux sous arbres : `(racine, sous arbre gauche, sous arbre droit)`. 

1. Justifier rapidement que les deux sous arbres sont des arbres binaires.
2. Que dire du sous arbre droit lorsque l'abeille est un mâle ?
3. Pour une feuille, que dire du sous arbre droit et du sous arbre gauche ?
4. En déduire une version récursive de la définition d'un arbre binaire.
5. Donner une définition récursive de la taille d'un arbre binaire.
6. Donner une définition récursive de la hauteur d'un arbre binaire.


### Relation entre la hauteur et la taille d'un arbre binaire

1. On note $h$ la hauteur d'un arbre binaire et $n$ sa taille, on suppose dans la suite que $n \geq 2$. En rappelant les définitions de la hauteur et de la taille d'un arbre, justifier que $n \geq h$.
2. On numérote les noeuds d'un arbre binaire de la suivante :  
    * la racine porte le numéro 1,
    * l'enfant gauche d'un noeud porte le numéro de son père suivi d'un 0,
    * l'enfant droit d'un noeud porte le numéro de son père suivi d'un 1.

On a entamé la numérotation de l'arbre binaire ci-dessous, compléter cette numérotation.

```mermaid
    graph TD
    A["A : 1"] --> B["B : 10"]
    A --> C["C : 11"]
    B --> D["D : ..."]
    B --> E["E : 101"]
    C --> V1[" "]
    C --> F["F: ..."]
    D --> G["G: ..."]
    D --> H["H: ..."]
    F --> I["I: ..."]
    F --> V2[" "]
    style V1 fill:#FFFFFF, stroke:#FFFFFF
    linkStyle 4 stroke:#FFFFFF,stroke-width:0px
    style V2 fill:#FFFFFF, stroke:#FFFFFF
    linkStyle 9 stroke:#FFFFFF,stroke-width:0px
```

3. Justifier que sur un niveau donné de l'arbre tous les numéros de noeud ont le même nombre de caractères.
4. En déduire en fonction de $h$, le nombre de caractères formant le numéro des feuilles
5. En utilisant vos connaissances sur la numérotation binaire d'un entier positif, prouver que $n \leq 2^h - 1$.

### Implémentation des arbres binaires en Python

[Le notebook suivant](https://capytale2.ac-paris.fr/web/c/3295-4964028) présente des implémentations *possibles* des arbres binaires en Python. 

Elles pourront être sauvegardées et utilisées en exercice.



### Parcours d'arbres

On considère l'arbre ci-dessous:

```mermaid
    graph TD
    N --> U
    N --> M
    U --> E
    M --> R
    M --> I
    R --> Q
    Q --> u2["U"]
    Q --> e2["E"]
```

- Quelle est la taille de cet arbre ? Sa hauteur ?

#### Parcours, introduction.

Parcourir un arbre, c'est **donner un ordre pour les noeuds** que l'on peut décrire **algorithmiquement** (avec un programme).

- Quel mot forme cet arbre si on lit tout les noeuds du **niveau 0**, puis du **niveau 1** et ainsi de suite ?

Ce premier parcours permet de facilement parler *des noeuds d'un niveau donné*.

On considère maintenant un parcours qui va s'intéresser **aux branches** au lieu des niveaux.

- Quel mot forme cet arbre si on effectue le pseudo-code suivant:

```
si l'arbre est une feuille, écrire l'étiquette de la feuille.
sinon:
    - parcourir tout le sous arbre gauche en effectuant ce pseudo-code.

    - écrire l'étiquette de la racine

    - parcourir tout le sous arbre droit en effectuant ce pseudo code.
```

- Comment pourriez vous modifier ce pseudo-code pour effectuer **deux autres** parcours de l'arbre qui ne donne pas le même mot.

## Des exercices un peu plus **pratiques**

### [La recherche du PPAC](https://iremi.univ-reunion.fr/spip.php?article1083#) section PPAC

On peut le remanier avec:

1. La recherche sur l'exemple donné
2. La transformation en arbre d'un fichier décrivant des relations (On donnera des squelettes de fonctions)
3. L'implémentation de l'algorithme de recherche du PPAC (voir [ens-fr](https://e-nsi.forge.aeif.fr/pratique/N3/874-ancetre_commun/sujet/))

### L'implémentation d'un arbre linéaire

Il s'agit de la représentation utilisée [ici](https://e-nsi.forge.aeif.fr/pratique/N3/874-ancetre_commun/sujet/).
Bien expliquée [ici](http://blog.ac-versailles.fr/nsilo/public/Terminale_-_Cours_complets/Tle_chap12_arbres.pdf) à partir de la page 26

### Utilisation d'un arbre pour évaluer une expression:

La structure n'est pas explicitement donnée: <https://adventofcode.com/2022/day/21>
